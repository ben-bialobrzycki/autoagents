using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using BBKModel;
using BBKAutoAgents.Controller;

namespace BBKAutoAgents
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class AgentSimulationGame : Microsoft.Xna.Framework.Game
    {
        protected GraphicsDeviceManager m_graphicsdevicemanager;
        SpriteBatch m_spritebatch;
        protected BBKModel.WholeModel m_wholemodel;
        protected WholeView m_gameview;
        protected InputHandler m_inputhandler;



        public AgentSimulationGame()
        {
            m_graphicsdevicemanager = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            this.m_graphicsdevicemanager.PreferredBackBufferWidth = 1280;
            this.m_graphicsdevicemanager.PreferredBackBufferHeight = 720;
            m_graphicsdevicemanager.ApplyChanges();
            Restart();
           
        }

        protected void Restart()
        {
            m_wholemodel = new WholeModel();
            m_gameview = new WholeView(m_wholemodel, m_graphicsdevicemanager, Content);
            m_gameview.Initialise();
            m_inputhandler = new InputHandler(m_gameview, m_wholemodel);
            this.Window.Title = "Autonomous Agents";
            m_wholemodel.SelectNext(); //default to showing debug info of first agent
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            m_spritebatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            m_inputhandler.HandleInput();
            if (m_inputhandler.Exit)
                this.Exit();
            if (m_inputhandler.RequestRestart)
                Restart();
            m_wholemodel.Update(gameTime);
            base.Update(gameTime);

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
            m_gameview.Draw(m_spritebatch);
           
        }
    }
}
