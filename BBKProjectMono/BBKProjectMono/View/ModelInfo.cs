﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace BBKAutoAgents
{

    public class ModelInfo
    {

        static ContentManager content;
        private Model mymodel;
        private ModelType mtype;
        public ModelType ModelType
        { get { return mtype; } }
        public Model MyModel
        { get { return mymodel; } }
        protected Matrix[] boneTransforms;
        public Matrix[] BoneTransforms
        { get { return boneTransforms; } }
        private float scale;
        public float Scale { get { return scale; } set { scale = value; } }
        private Vector3 initialrotation;
        public Vector3 InitialRotation { get { return initialrotation; } }

        public const float DEFAULTSCALE = 0.20f;
        public ModelInfo(ModelType mtype)
        {
            if (content == null)
                throw new Exception("Need contentmanager");

           
            this.mtype = mtype;
 
            if (mtype == ModelType.RedAgent)
            {
                //mymodel = content.Load<Model>("3dmodels/Robomousered");
                mymodel = content.Load<Model>("Robomousered");
                scale = DEFAULTSCALE;
                initialrotation = Vector3.Zero;
                initialrotation.X = (float)Math.PI / 2.0f;
                initialrotation.Y = (float)Math.PI / 2.0f;
                initialrotation.Z = (float)Math.PI + -(float)Math.PI / 2.0f;
               
                
            }
            else if (mtype == ModelType.BlueAgent)
            {
                //mymodel = content.Load<Model>("3dmodels/Robomouseblue");
                mymodel = content.Load<Model>("Robomouseblue");
                scale = DEFAULTSCALE;
                
                initialrotation = Vector3.Zero;
                initialrotation.X = (float)Math.PI / 2.0f; 
                initialrotation.Y = (float)Math.PI / 2.0f;
                initialrotation.Z = (float)Math.PI + -(float)Math.PI / 2.0f;
    
            }
            else if (mtype == ModelType.Wall)
            {
                //mymodel = content.Load<Model>("3dmodels/pinebox256x");
                mymodel = content.Load<Model>("pinebox256x");
                scale = 0.5f;
                initialrotation = Vector3.Zero;
      
            }
            else if (mtype == ModelType.Floor)
            {
                //mymodel = content.Load<Model>("3dmodels/GreenFloor");
                mymodel = content.Load<Model>("GreenFloor");
                scale = 1f;
                initialrotation = Vector3.Zero;
                initialrotation.Y = (float)Math.PI ; 
   
            }
            else if (mtype == ModelType.Cheese)
            {
                mymodel = content.Load<Model>("Cheese");
                //mymodel = content.Load<Model>("3dmodels/Cheese");
                scale = DEFAULTSCALE;
                initialrotation = Vector3.Zero;
  
            }
            else //Default
            {
                throw new Exception("Type not known " + mtype.ToString()); 
       
            }
            SetBones();
           

        }

        //Neccesary because some models created with different axis alignment i.e not with z as up
        public Vector3 AdjustAngleForModel(Vector3 angle)
        {
            if (mtype == ModelType.BlueAgent || mtype == ModelType.RedAgent)
            {
                Vector3 newangle = new Vector3(initialrotation.X, initialrotation.Y + angle.Z, initialrotation.Z);
                return newangle;
            }
            return angle + initialrotation;
        }
        public static void SetContentManager(ContentManager con)
        {
            content = con;
         
        }
        protected void SetBones()
        {
            boneTransforms = new Matrix[mymodel.Bones.Count];
            mymodel.CopyAbsoluteBoneTransformsTo(boneTransforms);
        }
        
        
    }
}
