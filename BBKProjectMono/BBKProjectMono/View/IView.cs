﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BBKModel;

namespace BBKAutoAgents.View
{
    public interface IView
    {
        Matrix ViewMatrix
        { get ; }
        Matrix ProjectionMatrix
        { get ; }

        Helper2d Helper2D
        { get ; }
        int ScreenWidth
        { get; }
        int ScreenHeight
        { get; }
        int WorldWidth
        { get ;}
        int WorldHeight
        { get ;}

        bool ShowPaths
        { get; }
        ModelEntity SelectedModel
        { get; }
        void SetLighting(BasicEffect effect);
    }
}
