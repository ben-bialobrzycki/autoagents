﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BBKModel;
using BBKAutoAgents.View;

namespace BBKAutoAgents
{
    public class MouseAgentView : ModelObjectView
    {
        Color m_defaultcolor = new Color(new Vector4(1.0f, 1.0f, 1.0f, ViewConstants.DEFAULTALPHA));
        public MouseAgentView(IView view, ModelType mtype):base(view, mtype)
        {

        }

       
        public override void Draw(ModelObject robj)
        {
            MouseAgent rmouse = (MouseAgent)robj;
            foreach (ModelMesh mesh in m_modelinfo.MyModel.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    if (rmouse.Immune)
                        effect.EmissiveColor = new Vector3(1, 0, 0);
                    else if (rmouse == m_view.SelectedModel)
                        effect.EmissiveColor = new Vector3(0.3f, 0.3f, 0.3f);
                    else
                        effect.EmissiveColor = new Vector3(0, 0, 0);

                }

            }
            base.Draw(rmouse);
        }

        /*Show Various bit of information about internal state*/
        public override void Draw2D(SpriteBatch batch, ModelObject mobj)
        {
            MouseAgent rmouse = (MouseAgent)mobj;

            string Team = "";
            if (rmouse.GetModelType() == ModelType.RedAgent)
                Team = "Red";
            else if (rmouse.GetModelType() == ModelType.BlueAgent)
                Team = "Blue";
            string info = "Type " + Team;
            string mode = "Mode " + rmouse.Mode.ToString();
            m_view.Helper2D.Draw2DString(batch, info, ViewConstants.LOCALDEFX, ViewConstants.LOCALINFOY);
            m_view.Helper2D.Draw2DString(batch, mode, ViewConstants.LOCALDEFX, ViewConstants.LOCALMODEY);
            m_view.Helper2D.Draw2DString(batch,"Position " + Helper2d.FormatVector3(rmouse.Position) ,ViewConstants.LOCALDEFX, ViewConstants.LOCALPOSY);
            float maprecwidth = (rmouse.LocalMap.Width * ViewConstants.RECSIZE)/((float)m_view.ScreenWidth);
            float mapstart = 1 - (maprecwidth) - ViewConstants.MARGIN;
            
    
            DrawLocalMap(batch, rmouse);
            string curdest = "Destination " + Helper2d.FormatVector3(rmouse.CurrentDestination);
            string Ultimatedest = "UltimateDest " + Helper2d.FormatVector3(rmouse.UltimateDestination);
            string Rotation = "Rotation " + Helper2d.FormatVector3(rmouse.GetRotation());

            m_view.Helper2D.Draw2DString(batch, curdest, ViewConstants.LOCALDEFX, ViewConstants.LOCALDESTY);
            m_view.Helper2D.Draw2DString(batch, Ultimatedest, ViewConstants.LOCALDEFX, ViewConstants.LOCALULTDESTY);
            m_view.Helper2D.Draw2DString(batch, Rotation, ViewConstants.LOCALDEFX, ViewConstants.LOCALROTY);

            string Health = rmouse.Health.ToString();
            m_view.Helper2D.Draw2DString(batch, "Health = " + Health, ViewConstants.LOCALDEFX, ViewConstants.LOCALHEALTHY);
            string UseWay = rmouse.UsingWayPoints.ToString();
            m_view.Helper2D.Draw2DString(batch, "UseWayPoints = " + UseWay, ViewConstants.LOCALDEFX, ViewConstants.LOCALWAYY);


        }

        private void DrawLocalMap(SpriteBatch batch, MouseAgent magent)
        {
            float maprecwidth = (magent.LocalMap.Width * ViewConstants.RECSIZE) / ((float)m_view.ScreenWidth);
            float mapstart = 1 - (maprecwidth) - ViewConstants.MARGIN;
          
            Vector2 position = new Vector2(mapstart * m_view.ScreenWidth, ViewConstants.LOCALMAPY * m_view.ScreenHeight);
            List<Location> predlocs = magent.PredatorFinder.TargetLocations;
            List<Location> preylocs = magent.PreyFinder.TargetLocations;
            List<Location> waypoints = CopyWayPoints(magent);
            for (int i = 0; i < magent.LocalMap.Width; i++)
            {
                for (int j = 0; j < magent.LocalMap.Height ; j++)
                {
                    Rectangle destrec = new Rectangle((int)position.X + ( i * ViewConstants.RECSIZE)
                        ,(int)position.Y + (j * ViewConstants.RECSIZE),ViewConstants.RECSIZE,ViewConstants.RECSIZE);


                    List<LocalModelObjectData> lsobjs = magent.LocalMap.GetLocationSlotObjs(new Location(i, j));
                    bool drawn = false;

                    Location currentloc = new Location(i, j);
                    foreach (LocalModelObjectData lsobj in lsobjs)
                    {
                        if (!ShouldShow(lsobj,currentloc,magent))
                            continue;
                        Rectangle sourcerec = m_view.Helper2D.RecForModelType(lsobj.ModelType);
                        Color drawcolor = new Color(1.0f, 1.0f, 1.0f, ViewConstants.DEFAULTALPHA);
                        batch.Draw(m_view.Helper2D.SpriteMap, destrec, sourcerec, drawcolor);
                        drawn = true;
                    }                   

                    if (predlocs.Contains(currentloc))
                    {
                        Rectangle sourcerec = m_view.Helper2D.RecForModelType(magent.PredatorFinder.ModelType);
                        batch.Draw(m_view.Helper2D.SpriteMap, destrec, sourcerec, m_defaultcolor);
                        drawn = true;
                    }

                    if (preylocs.Contains(currentloc))
                    {
                        Rectangle sourcerec = m_view.Helper2D.RecForModelType(magent.PreyFinder.ModelType);
                        batch.Draw(m_view.Helper2D.SpriteMap, destrec, sourcerec, m_defaultcolor);
                        drawn = true;
                    }

                    if (m_view.ShowPaths && waypoints.Contains(currentloc))
                    {
                        batch.Draw(m_view.Helper2D.SpriteMap, destrec, ViewConstants.CURRENTDESTREC, m_defaultcolor);    
                    }
                    if (!drawn)
                    {
                        Rectangle sourcerec = m_view.Helper2D.RecForModelType(ModelType.None);               
                        batch.Draw(m_view.Helper2D.SpriteMap, destrec, sourcerec, m_defaultcolor);
                    }


                    double msupdate = magent.LocalMap.TimeSinceLastUpdated(new Location(i, j)).TotalMilliseconds;
                    if (msupdate < ViewConstants.THRESHVISIBLE)
                    {
                        Rectangle sourcerec = ViewConstants.MARKERREC;
                        float alph = 1.0f - (float)(msupdate / ViewConstants.THRESHVISIBLE);
                        Color drawcolor = new Color(1.0f, 1.0f, 1.0f, ViewConstants.DEFAULTALPHAVISIBLE * alph);
                        batch.Draw(m_view.Helper2D.SpriteMap, destrec, sourcerec, drawcolor );
                    }
                    
                    if (magent.Location == currentloc)
                        batch.Draw(m_view.Helper2D.SpriteMap, destrec, m_view.Helper2D.RecForModelType(magent.GetModelType()), m_defaultcolor);                     

                    if (magent.CurrentDestinationAsLocation == currentloc)                      
                        batch.Draw(m_view.Helper2D.SpriteMap, destrec, ViewConstants.CURRENTDESTREC, m_defaultcolor);                     
      
                    if (magent.UltimateDestinationAsLocation == currentloc)
                        batch.Draw(m_view.Helper2D.SpriteMap, destrec, ViewConstants.ULTIMATEDESTREC, m_defaultcolor);   
                   
                }
            }
        }

        private List<Location> CopyWayPoints(MouseAgent magent)
        {
            List<Location> locs = new List<Location>();
            if (magent.UsingWayPoints && magent.WayPoints != null)
            {
                Vector3[] vecarray = new Vector3[magent.WayPoints.Count];
                magent.WayPoints.CopyTo(vecarray,0);
                foreach (Vector3 vec in vecarray)
                {
                    locs.Add(magent.LocalMap.VectortoLocation(vec));
                }
            }
            return locs;
            
        }

        private bool ShouldShow(LocalModelObjectData lmod, Location loc, MouseAgent magent)
        {
            if (lmod.ModelType == ModelType.Wall)
                return true;
            if (lmod.ModelID == magent.UniqueID)
                return true;
            return false;
        }

   
    }
}
