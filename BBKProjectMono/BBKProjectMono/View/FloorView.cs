﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BBKAutoAgents.View;

namespace BBKAutoAgents
{
    public class FloorView : ModelObjectView
    {
          public FloorView(IView view, ModelType mtype) : base(view, mtype) { }

          public override void Draw(BBKModel.ModelObject mobj)
          {             
              m_modelinfo.Scale = m_view.WorldHeight / 2.0f;
              base.Draw(mobj);
          }
    }
}
