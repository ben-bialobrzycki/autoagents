﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using BBKModel;

namespace BBKAutoAgents
{
    public class FrameCounterView
    {
        int m_drawcalls;
        FrameCounter m_framecounter;
        float m_currentfps = 0;
        Helper2d m_helper2d;

        public FrameCounterView(FrameCounter fcounter, Helper2d h2d)
        {
            m_framecounter = fcounter;
            m_helper2d = h2d;
        }
        public void Draw2d(SpriteBatch batch)
        {
            m_drawcalls++;
            if (m_framecounter.TimePassed > m_framecounter.TimeLimit)
            {
                m_currentfps = (float)(1000.0f * (m_drawcalls / m_framecounter.TimePassed.TotalMilliseconds));
                m_framecounter.Reset();
                m_drawcalls = 0;
            }
            string fpsstring = Helper2d.FormatFloat(m_currentfps, "N3");
            m_helper2d.Draw2DString(batch, "FPS " + fpsstring, ViewConstants.FPSX, ViewConstants.FPSY);

        }
    }
}
