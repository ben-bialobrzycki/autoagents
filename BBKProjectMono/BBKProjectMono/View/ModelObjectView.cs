﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BBKModel;
using BBKAutoAgents.View;


namespace BBKAutoAgents
{
    public abstract class ModelObjectView
    {
        protected ModelInfo m_modelinfo;
        protected IView m_view;

        protected static SpriteFont m_font;

        public ModelObjectView(IView view, ModelType mtype) 
        {
            m_view = view;
            m_modelinfo = new ModelInfo(mtype);
        }

        public ModelType EntitiyModelType
        { get { return m_modelinfo.ModelType; } }

        public virtual void Draw(ModelObject mobj)
        {
            Quaternion qrotation;
         
            Vector3 rotation = m_modelinfo.AdjustAngleForModel(mobj.GetRotation());
            
            qrotation = Quaternion.CreateFromAxisAngle(Vector3.UnitX, rotation.X) *
                    Quaternion.CreateFromAxisAngle(Vector3.UnitY, rotation.Y) *
                    Quaternion.CreateFromAxisAngle(Vector3.UnitZ, rotation.Z);
            foreach (ModelMesh mesh in m_modelinfo.MyModel.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {        
                    effect.View = m_view.ViewMatrix;
                    effect.Projection = m_view.ProjectionMatrix;
                    m_view.SetLighting(effect);
                    effect.World = m_modelinfo.BoneTransforms[mesh.ParentBone.Index] *
                    Matrix.CreateScale(m_modelinfo.Scale) *
                    Matrix.CreateFromQuaternion(qrotation) *
                    Matrix.CreateTranslation(mobj.GetPosition());
                }
                mesh.Draw();
            }
        }

        public virtual void Draw2D(SpriteBatch batch, ModelObject mobj)
        {

        }

    }
}
