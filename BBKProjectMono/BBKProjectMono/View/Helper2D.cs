﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using BBKModel;

namespace BBKAutoAgents
{

    public class Helper2d
    {

        Texture2D m_spritemap;
        ContentManager m_content;
        WholeView m_view;
       
        Color m_defaultcolor = new Color(new Vector4(1.0f, 1.0f, 1.0f, ViewConstants.DEFAULTALPHA));
        Color m_textcolor = new Color(new Vector4(0.0f, 0.0f, 0.5f, 1.0f));
        public Helper2d(WholeView view,ContentManager content)
        {
            m_content = content;
            m_view = view;
            //m_spritemap = m_content.Load<Texture2D>("Textures/2dMap");
            //m_font = m_content.Load<SpriteFont>("Other/Courier");

            m_spritemap = m_content.Load<Texture2D>("2dMap");
            m_font = m_content.Load<SpriteFont>("Courier");
        }

        private SpriteFont m_font;

        public Texture2D SpriteMap
        { get { return m_spritemap;}}

        public static string FormatVector3(Vector3 vec)
        {
            return FormatVector3(vec, "N2");
        }

        public static string FormatVector3(Vector3 vec, string format)
        {
            string x = FormatFloat(vec.X, format);
            string y = FormatFloat(vec.Y, format);
            string z = FormatFloat(vec.Z, format);
            return "(" + x + " ," + y + " ," + z + ")";
        }

        public static string FormatFloat(float x, string format)
        {
            return x.ToString(format);
        }
   
        public void Draw2DString(SpriteBatch batch, string text, float xperc, float yperc)
        {
            Draw2DString(batch, text, xperc, yperc, m_defaultcolor);
        }
        public void Draw2DString(SpriteBatch batch, string text,float xperc, float yperc,Color color)
        {
            Vector2 textpos = new Vector2(m_view.ScreenWidth * xperc,m_view.ScreenHeight * yperc);
            batch.DrawString(m_font, text, textpos, color);
        }

        public void Draw2DMapFromWorldMap(SpriteBatch batch, BaseMap map, float xperc, float yperc, bool ShowLastUpdate)
        {
            Draw2DMapFromWorldMap(batch, map,xperc, yperc, ShowLastUpdate, Location.Null,Location.Null,null);
        }  

        public void Draw2DMapFromWorldMap(SpriteBatch batch, BaseMap map, float xperc, float yperc, bool ShowLastUpdate,Location currentdest, Location ultimatedest ,Dictionary<ModelType,TimeSpan> IgnoreTimeDictionary)
        {
            Vector2 position = new Vector2(xperc * m_view.ScreenWidth, yperc * m_view.ScreenHeight); 
            for (int i = 0; i < map.Width; i++)
            {
                for (int j = 0; j < map.Height; j++)
                {
                    Rectangle destrec = new Rectangle((int)position.X + ( i * ViewConstants.RECSIZE)
                        ,(int)position.Y + (j * ViewConstants.RECSIZE),ViewConstants.RECSIZE,ViewConstants.RECSIZE);


                    List<LocalModelObjectData> lsobjs = map.GetLocationSlotObjs(new Location(i, j));
                    bool drawn = false;
                    LocalMap wmap = map as LocalMap;
                   
                    foreach (LocalModelObjectData lsobj in lsobjs)
                    {

                        Rectangle sourcerec = RecForModelType(lsobj.ModelType);
                        float intensity = (1.0f / lsobjs.Count);
                        if (wmap != null && IgnoreTimeDictionary != null && IgnoreTimeDictionary.ContainsKey(lsobj.ModelType))
                        {
                            
                            TimeSpan timeout = IgnoreTimeDictionary[lsobj.ModelType];
                            intensity *= GetIntensity(timeout,wmap.TimeSinceLastUpdated(new Location(i, j)));
                            if (intensity == 0)
                                continue;
                        }
                       
                        Color drawcolor = new Color(1.0f, 1.0f, 1.0f, ViewConstants.DEFAULTALPHA * intensity);
                        batch.Draw(m_spritemap, destrec, sourcerec, drawcolor);
                        drawn = true;
                        

                    }
                    
                  
                    if (!drawn)
                    {
                        Rectangle sourcerec = RecForModelType(ModelType.None);
                        Color drawcolor = new Color(1.0f, 1.0f, 1.0f, ViewConstants.DEFAULTALPHA);
                        batch.Draw(m_spritemap, destrec, sourcerec, m_defaultcolor);
                    }
                    
                    if (ShowLastUpdate && wmap != null)
                    {
                        double msupdate = wmap.TimeSinceLastUpdated(new Location(i, j)).TotalMilliseconds;
                        if (msupdate < ViewConstants.THRESHVISIBLE)
                        {
                            Rectangle sourcerec = ViewConstants.MARKERREC;
                            float alph = 1.0f - (float)(msupdate / ViewConstants.THRESHVISIBLE);
                            Color drawcolor = new Color(1.0f, 1.0f, 1.0f, ViewConstants.DEFAULTALPHAVISIBLE * alph);
                            batch.Draw(m_spritemap, destrec, sourcerec, m_defaultcolor);
                        }
                    }

                    if (currentdest != Location.Null)
                    {
                        if (currentdest.X == i && currentdest.Y == j)
                        {
                            Rectangle currdestrec = ViewConstants.CURRENTDESTREC;
                            batch.Draw(m_spritemap, destrec, currdestrec, m_defaultcolor);
                        }
                    }
                    if (ultimatedest != Location.Null)
                    {
                        if (ultimatedest.X == i && ultimatedest.Y == j)
                        {
                            Rectangle ultimatedestrec = ViewConstants.ULTIMATEDESTREC;
                            batch.Draw(m_spritemap, destrec, ultimatedestrec, m_defaultcolor);
                        }
                    }
                }

            }

           
        }

        public Rectangle RecForModelType(ModelType mtype)
        {
           
            switch (mtype)
            {
                case ModelType.BlueAgent: return ViewConstants.BLUEMOUSEREC; 
                case ModelType.RedAgent: return ViewConstants.REDMOUSEREC;
                case ModelType.Wall: return ViewConstants.WALLREC;
                case ModelType.Cheese: return ViewConstants.CHEESEREC;
                case ModelType.None: return ViewConstants.EMPTYREC; 
                default: return ViewConstants.EMPTYREC;

            }
          
        }
        public static void reset(GraphicsDevice graphics)
        {
            //TODO FIgure out what the hell to set here.
            graphics.BlendState = BlendState.AlphaBlend;
            graphics.DepthStencilState = DepthStencilState.None;
            graphics.RasterizerState = RasterizerState.CullCounterClockwise;
            graphics.SamplerStates[0] = SamplerState.LinearClamp;
//SpriteBatch also modifies the vertex buffer, index buffer, and applies its own effect onto the GraphicsDevice.

//Before you draw anything in 3D you will probably want to reset these states:

            graphics.BlendState = BlendState.Opaque;
            graphics.DepthStencilState = DepthStencilState.Default;
//Depending on your 3D content, you may also want to set:

            graphics.SamplerStates[0] = SamplerState.LinearWrap;
            
            
            //graphics.RenderState.DepthBufferEnable = true;
            //graphics.RenderState.AlphaBlendEnable = false;

            //graphics.SamplerStates[0].AddressU = TextureAddressMode.Wrap;
            //graphics.SamplerStates[0].AddressV = TextureAddressMode.Wrap;

            ////Other experimental settings

            //graphics.RenderState.SourceBlend = Blend.Zero;
            //graphics.RenderState.DestinationBlend = Blend.InverseSourceAlpha;
            //graphics.RenderState.SeparateAlphaBlendEnabled = true;

            //graphics.RenderState.AlphaTestEnable = false;
            //graphics.RenderState.AlphaFunction = CompareFunction.Greater;
            //graphics.RenderState.ReferenceAlpha = 0;
            //graphics.SamplerStates[0].MagFilter = TextureFilter.Linear;
            //graphics.SamplerStates[0].MinFilter = TextureFilter.Linear;
            //graphics.SamplerStates[0].MipFilter = TextureFilter.Linear;
            //graphics.SamplerStates[0].MipMapLevelOfDetailBias = 0.0f;
            //graphics.SamplerStates[0].MaxMipLevel = 0;
        }

        static float GetIntensity(TimeSpan timeout, TimeSpan TimeSinceUpdate)
        {
           float intensity =(float)( (timeout.TotalMilliseconds - TimeSinceUpdate.TotalMilliseconds) / timeout.TotalMilliseconds);
           if (intensity < 0)
               return 0;
           else
               return intensity;
        }
    }
}
