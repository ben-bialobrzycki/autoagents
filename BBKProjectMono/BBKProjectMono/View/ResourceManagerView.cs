﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BBKModel;
using Microsoft.Xna.Framework.Graphics;

namespace BBKAutoAgents.View
{
    public class ResourceManagerView
    {
       
        ResourceManager  m_manager;
    
        Helper2d m_helper2d;

        public ResourceManagerView(ResourceManager manager, Helper2d h2d)
        {
            m_manager = manager;
         
            m_helper2d = h2d;
        }
        public void Draw2d(SpriteBatch batch)
        {
           
            m_helper2d.Draw2DString(batch, m_manager.GetMessages(), ViewConstants.CENTREX, ViewConstants.CENTREY);

        }

      
    }
}
