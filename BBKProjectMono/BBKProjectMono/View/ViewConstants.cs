﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKAutoAgents
{
    public class ViewConstants
    {
        /*GUI DEBUG INFO POSITIONS IN PERCENTAGE OF SCREEN WIDTH AND HEIGHT*/
        public const float WORLDMAPX = 0.02f;
        public const float WORLDMAPY = 0.02f ;

        public const float LOCALINFOY = 0.02f;
        public const float LOCALMODEY = 0.04f;
        public const float LOCALPOSY = 0.06f;
        public const float LOCALDESTY = 0.08f;
        public const float LOCALULTDESTY = 0.10f;
        public const float LOCALROTY = 0.12f;
        public const float LOCALLOOKATY = 0.14f;
        public const float LOCALPREDY = 0.16f;
        public const float LOCALHEALTHY = 0.18f;
        public const float LOCALDEFX = 0.75f;
        public const float LOCALWAYY = 0.20f;

        public const float LOCALMAPX = 0.65f;
        public const float LOCALMAPY = 0.3f;

        public const float FPSX = 0.45f;
        public const float FPSY = 0.01f;
        public const float CENTREX = 0.45f;
        public const float CENTREY = 0.03f;

        public const float THRESHVISIBLE = 200.0f;

        /*Positions of 2d sprites on source Bitmap*/
        public static Rectangle BLUEMOUSEREC = new Rectangle(17, 13, 25, 25);
        public static Rectangle REDMOUSEREC = new Rectangle(55, 13, 25, 25);
        public static Rectangle WALLREC = new Rectangle(92, 13, 25, 25);
        public static Rectangle CHEESEREC = new Rectangle(198, 13, 25, 25);
        public static Rectangle EMPTYREC = new Rectangle(128, 13, 25, 25);
        public static Rectangle MARKERREC = new Rectangle(164, 13, 25, 25);
        public static Rectangle CURRENTDESTREC = new Rectangle(233, 13, 25, 25);
        public static Rectangle ULTIMATEDESTREC = new Rectangle(267, 13, 25, 25);

        public const int RECSIZE = 12;
        public const float MARGIN = 0.05f; // Margin as Screen %

        //ALPHA VALUES FOR 2D OVERLAY
        public const float DEFAULTALPHA = 0.5f;
        public const float DEFAULTALPHAVISIBLE = 0.3f;



        /*Camera Constants */
        public const float DefaultCameraOffset = 0.5f;
        public const float NEARPLANEDISTANCE = 0.1f;
        public const float FARPLANEDISTANCE = 10000.0f;
    }
}
