﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using BBKModel;

namespace BBKAutoAgents
{
    public enum ViewMode { Normal, Debug };
    public enum CameraPosition { CentreHigh, CentreGround };
    public class View
    {
        List<ModelInfo> m_allmodels;
      
        WholeModel m_worldmodel;
        GraphicsDeviceManager m_graphics;
        BasicEffect m_worldeffect;
        ContentManager m_content;
        Quaternion m_qrotation;
        List<IBBKDrawable> m_drawableobjects;
        Dictionary<ModelType, Model> m_models;
        Camera m_maincamera;
        Texture2D floortexture;
        Texture2D walltexture;
        
        /*EntityViews*/
        List<EntityView> m_entityviews;
       
        Helper2d m_helper2d;
        ViewMode currentviewmode;
        private float lightstrength = 0.5f;
       // SceneRenderer m_scenerender;
        float HighCameraZ = 20;

        public Helper2d Helper2D
        { get { return m_helper2d; } }

        public List<ModelInfo> AllModels
        { get { return m_allmodels; } }
        public BasicEffect WorldEffect { get { return m_worldeffect; } }

        public int ScreenWidth
        { get { return m_graphics.PreferredBackBufferWidth; } }
        public int ScreenHeight
        { get { return m_graphics.PreferredBackBufferHeight; } }



        public View(WholeModel model, GraphicsDeviceManager graph, ContentManager cont)
        {
            m_worldmodel = model;
            m_content = cont;
            m_graphics = graph;
            m_allmodels = new List<ModelInfo>();
            m_maincamera = new Camera((float)m_graphics.GraphicsDevice.Viewport.Width / m_graphics.GraphicsDevice.Viewport.Height);
            Vector2 centre = new Vector2(model.WorldWidth  / 2, model.WorldHeight / 2);
            m_maincamera.Lookat = new Vector3(centre, 0);
            m_maincamera.position = new Vector3(model.WorldWidth / 2, model.WorldHeight, -HighCameraZ);

            currentviewmode = ViewMode.Normal;
            m_content.RootDirectory = "Content";
            //m_scenerender = new SceneRenderer(model, graph.GraphicsDevice, cont);
            graph.GraphicsDevice.RenderState.CullMode = CullMode.None; /*Todo Fix Wall Rendering so this is not neccesarry*/
            m_helper2d = new Helper2d(this,m_content);

           
        }

        public WholeModel WorldModel
        { get { return m_worldmodel; } }

        private void InitialiseEntityViews()
        {
            m_entityviews = new List<EntityView>();
            RoboMouseBlueView m_mouseview = new RoboMouseBlueView(this, GetModelInfo(ModelType.RoboMouseBlue));
            m_entityviews.Add(m_mouseview);
            RoboMouseRedView m_mouseredview = new RoboMouseRedView(this, GetModelInfo(ModelType.RoboMouseRed));
            m_entityviews.Add(m_mouseredview);
            WallView wallview = new WallView(this, GetModelInfo(ModelType.Wall));
            m_entityviews.Add(wallview);
            FloorView floorview = new FloorView(this, GetModelInfo(ModelType.Floor));
            m_entityviews.Add(floorview);
        }

        public void MoveCurrentCamera(Movement mov)
        {
            m_maincamera.Move(mov);
        }
       
        private void SetEffect()
        {
            m_worldeffect = new BasicEffect(m_graphics.GraphicsDevice, null);
            m_worldeffect.TextureEnabled = true;
            //  effect.EnableDefaultLighting();
            m_worldeffect.View = Matrix.CreateLookAt(new Vector3(1000, 1000, 6.5f), new Vector3(0, 0, 0.5f), new Vector3(0, 0, 1));
            m_worldeffect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, (float)m_graphics.GraphicsDevice.Viewport.Width / m_graphics.GraphicsDevice.Viewport.Height, 0.1f, 10000.0f);
        }
        public void Initialise()
        {
            ModelInfo.SetContentManager(m_content);
          
          //  content.Load<Model>("dalek");
            SetEffect();
            SetLighting(m_worldeffect);
            LoadModels();
            LoadTextures();
            
           // m_helper2d = Helper2d.initialise(m_content.Load<SpriteFont>("Other/Courier"));
            EntityView.Font = m_content.Load<SpriteFont>("Other/Courier");
            InitialiseEntityViews();
        }

        public void SwitchMode()
        {
            int current = (int)currentviewmode;
            current++;
            int count = Enum.GetValues(typeof(ViewMode)).Length;
            current = current % count;
            currentviewmode = (ViewMode)current;
        }
        private void LoadTextures()
        {
            //content.RootDirectory = "Content/Textures";
            floortexture = m_content.Load<Texture2D>("Textures/ConcreteFloor");
            walltexture = m_content.Load<Texture2D>("Textures/Pine");
        }

        private void LoadModels()
        {
            ModelInfo Tinfo = new ModelInfo(ModelType.Termite);
            m_allmodels.Add(Tinfo);
            ModelInfo Dinfo = new ModelInfo(ModelType.Dalek);
            m_allmodels.Add(Dinfo);
            ModelInfo Minfo = new ModelInfo(ModelType.RoboMouseRed);
            m_allmodels.Add(Minfo);
            ModelInfo MBinfo = new ModelInfo(ModelType.RoboMouseBlue);
            m_allmodels.Add(MBinfo);
            ModelInfo Wallinfo = new ModelInfo(ModelType.Wall);
            m_allmodels.Add(Wallinfo);
            ModelInfo Floorinfo = new ModelInfo(ModelType.Floor);
            m_allmodels.Add(Floorinfo);
        }

        public void Draw(SpriteBatch batch)
        {
            m_maincamera.SetView(m_worldeffect);
         
            DrawGameObjects();
       
            if (currentviewmode == ViewMode.Debug)
            {
     
                Draw2DInfo(batch);

            }
        }

        private void Draw2DInfo(SpriteBatch batch)
        {
            batch.Begin();
            foreach (IBBKDrawable drawable in m_drawableobjects)
            {
                if (drawable.GetModelType() != ModelType.Wall)
                {
                    EntityView entview = GetEntityView(drawable.GetModelType());
                    entview.Draw2D(batch, (ModelObject)drawable);
                }
            }

            Draw2DWorldInfo(batch);
            batch.End();
            Helper2d.reset(m_graphics.GraphicsDevice);
        }

        private void Draw2DWorldInfo(SpriteBatch batch)
        {
          
            m_helper2d.Draw2DMapFromWorldMap(batch, m_worldmodel.WorldMap,ViewConstants.WORLDMAPX,ViewConstants.WORLDMAPY);
          
         
        }

        private void DrawGameObjects()
        {
            m_drawableobjects = m_worldmodel.DrawableObjects;
    
            foreach (IBBKDrawable drawable in m_drawableobjects)
            {
                ModelType mtype = drawable.GetModelType();      
                EntityView entview = GetEntityView(mtype);
                entview.Draw((ModelObject)drawable);
        
            }
        }

        private EntityView GetEntityView(ModelType mtype)
        {
            foreach (EntityView eview in m_entityviews)
            {
                if (eview.EntitiyModelType == mtype)
                    return eview;
            }
            throw new Exception("No EntityModel in list for " + mtype.ToString());
        }



        private void DrawGameModel(IBBKDrawable drawable)
        {
            ModelInfo minfo = GetModelInfo(drawable.GetModelType());
            Model mymodel = minfo.MyModel;
            Vector3 rotation = drawable.GetRotation();
            rotation += minfo.InitialRotation;
            m_qrotation = Quaternion.CreateFromAxisAngle(Vector3.Right, rotation.X) *
                    Quaternion.CreateFromAxisAngle(Vector3.Up, rotation.Y) *
                    Quaternion.CreateFromAxisAngle(Vector3.Backward, rotation.Z);
            foreach (ModelMesh mesh in mymodel.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {

                    effect.View = m_worldeffect.View;
                    effect.Projection = m_worldeffect.Projection;
                    SetLighting(effect);

                    effect.World = minfo.BoneTransforms[mesh.ParentBone.Index] *
                    Matrix.CreateScale(minfo.Scale) *
                    Matrix.CreateFromQuaternion(m_qrotation) *
                    Matrix.CreateTranslation(drawable.GetPosition());
                }
                mesh.Draw();
            }
        }

        private ModelInfo GetModelInfo(ModelType modelType)
        {
            foreach (ModelInfo minfo in m_allmodels)
            {
                if (minfo.ModelType == modelType)
                    return minfo;
            }
            string msg = "The Model " + modelType.ToString() + " Was mot loaded";
            throw new Exception(msg);
        }

        private void DrawPrimitiveObject(IBBKDrawable drawable)
        {
            IBBKScenery scenery = (IBBKScenery)drawable;
            Vector3 size = scenery.GetSize();

        }


  
        public void SetLighting(BasicEffect effect)
        {
            effect.AmbientLightColor = new Vector3(0.5f, 0.5f, 0.5f);
            effect.LightingEnabled = true;
            effect.DirectionalLight0.Enabled = true;
            effect.DirectionalLight0.Direction = Vector3.Normalize(new Vector3(0, 0, -1));
           // effect.DirectionalLight0.DiffuseColor = new Vector3(lightstrength, 0.5f, 0.5f);
            effect.DirectionalLight0.DiffuseColor = new Vector3(0.5f, 0.5f, 0.5f);
           // effect.DirectionalLight1.Enabled = true;
          //  effect.DirectionalLight1.Direction = Vector3.Normalize(new Vector3(1f, -1.0f, 0f));
           // effect.DirectionalLight1.DiffuseColor = new Vector3(0, 0.7f, lightstrength);
            effect.FogEnabled = false;
            effect.FogStart = 4.0f;
            effect.FogColor = new Vector3(0.2f, 0.2f, 0.2f);
            effect.FogEnd = 10.0f;

        }
      

        internal void SetCameraPosition(CameraPosition cameraPosition)
        {
            float wcentre = m_worldmodel.WorldWidth / 2;
            float hcentre = m_worldmodel.WorldHeight / 2;
            switch (cameraPosition)
            {
                case CameraPosition.CentreGround: m_maincamera.position = new Vector3(wcentre, m_worldmodel.WorldHeight, 0); break;
                case CameraPosition.CentreHigh: m_maincamera.position = new Vector3(m_worldmodel.WorldWidth / 2, m_worldmodel.WorldHeight, HighCameraZ); break;


            }
        }
    }
}
