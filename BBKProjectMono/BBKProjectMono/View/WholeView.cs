﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using BBKModel;
using BBKAutoAgents.View;

namespace BBKAutoAgents
{
  
    public class WholeView : IView
    {
  
      
        WholeModel m_worldmodel;
        GraphicsDeviceManager m_graphics;
 
        ContentManager m_content;
   
  
        Camera m_maincamera;
        Camera m_overheadcamera;
        Camera m_topleftcorner;
        Camera m_toprightcorner;
        Camera m_bottomleftcorner;
        Camera m_bottomrightcorner;

        Camera m_agentcamera;
        Dictionary<ModelType, ModelObjectView> m_entityviews;
       
        Helper2d m_helper2d;
        FrameCounterView m_frameview;
        ResourceManagerView m_managerview;
        ViewMode currentviewmode;
        private float m_directionallightstrength = 0.5f;
        private float m_ambientlightstrength = 0.5f;
      
       

        public Helper2d Helper2D
        { get { return m_helper2d; } }



        public int ScreenWidth
        { get { return m_graphics.PreferredBackBufferWidth; } }
        public int ScreenHeight
        { get { return m_graphics.PreferredBackBufferHeight; } }
        public int WorldWidth
        { get { return m_worldmodel.WorldWidth; } }
        public int WorldHeight
        { get { return m_worldmodel.WorldHeight; } }

        public Matrix ViewMatrix
        { get { return m_maincamera.GetView(); } }
        public Matrix ProjectionMatrix
        { get { return m_maincamera.GetProjection(); } }

        public Helper2d Helper2d
        { get { return m_helper2d; } }

        public ModelEntity SelectedModel
        { get { return m_worldmodel.SelectedObject; } }
        public WholeView(WholeModel model, GraphicsDeviceManager graph, ContentManager cont)
        {
            m_worldmodel = model;
          
            m_content = cont;
            m_graphics = graph;
 
            InitialiseCameras();



            currentviewmode = ViewMode.Debug; //ViewMode.Normal;
        
            m_helper2d = new Helper2d(this,m_content);
            m_frameview = new FrameCounterView(m_worldmodel.FrameCounter, m_helper2d);
            m_managerview = new ResourceManagerView(m_worldmodel.ResourceManager, m_helper2d);

           
        }

        public float AspectRation
        {
            get { return (float)m_graphics.GraphicsDevice.Viewport.Width / m_graphics.GraphicsDevice.Viewport.Height; }
        }
        private void InitialiseCameras()
        {
            m_topleftcorner = new Camera(this,AspectRation);
            m_topleftcorner.SetCameraPosition(CameraPosition.TopLeft);
            
            m_toprightcorner = new Camera(this, AspectRation);
            m_toprightcorner.SetCameraPosition(CameraPosition.TopRight);

            m_bottomleftcorner = new Camera(this, AspectRation);
            m_bottomleftcorner.SetCameraPosition(CameraPosition.BottomLeft);

            m_bottomrightcorner = new Camera(this, AspectRation);
            m_bottomrightcorner.SetCameraPosition(CameraPosition.BottomRight);

            m_overheadcamera = new Camera(this,AspectRation );
            m_overheadcamera.SetCameraPosition(CameraPosition.CentreHigh);
        
            m_agentcamera = new Camera(this, (float)m_graphics.GraphicsDevice.Viewport.Width / m_graphics.GraphicsDevice.Viewport.Height);
            m_agentcamera.SetCameraPosition(CameraPosition.CentreGround);

            m_maincamera = m_overheadcamera;
        }

        public WholeModel WorldModel
        { get { return m_worldmodel; } }

        private void InitialiseEntityViews()
        {
            m_entityviews = new Dictionary<ModelType, ModelObjectView>();
            BlueAgentView m_mouseview = new BlueAgentView(this, ModelType.BlueAgent);
            m_entityviews.Add(ModelType.BlueAgent,m_mouseview);
            RedAgentView m_mouseredview = new RedAgentView(this, ModelType.RedAgent);
            m_entityviews.Add(ModelType.RedAgent,m_mouseredview);
            WallView wallview = new WallView(this, ModelType.Wall);
            m_entityviews.Add(ModelType.Wall,wallview);
            FloorView floorview = new FloorView(this, ModelType.Floor);
            m_entityviews.Add(ModelType.Floor,floorview);
            CheeseView cheeseview = new CheeseView(this, ModelType.Cheese);
            m_entityviews.Add(ModelType.Cheese,cheeseview);
        }

        public void MoveCurrentCamera(Movement mov)
        {
            m_maincamera.Move(mov);
        }
      
        public void Initialise()
        {
            ModelInfo.SetContentManager(m_content);
          
       
            InitialiseEntityViews();
        }

        public void SwitchMode()
        {
            int current = (int)currentviewmode;
            current++;
            int count = Enum.GetValues(typeof(ViewMode)).Length;
            current = current % count;
            currentviewmode = (ViewMode)current;
        }

        private bool m_showpaths;
        public bool ShowPaths
        {
            get { return m_showpaths; }
            set { m_showpaths = value; }
        }

        public void Draw(SpriteBatch batch)
        {
            if (m_maincamera == m_agentcamera)
                AdjustAgentCamera();    
    
            DrawGameObjects();

            if (currentviewmode == ViewMode.Debug) 
                Draw2DInfo(batch);
        }

        private void DrawGameObjects()
        {
            foreach (ModelObject drawable in m_worldmodel.ModelObjects)
            {
                ModelType mtype = drawable.GetModelType();
                ModelObjectView entview = m_entityviews[mtype];
                entview.Draw(drawable);
            }
        }
        private void AdjustAgentCamera()
        {
            ModelEntity selectedobj = m_worldmodel.SelectedObject;
            if (selectedobj != null)
            {
                Vector3 CamOffset = ViewConstants.DefaultCameraOffset * Vector3.Normalize(selectedobj.Lookat - selectedobj.Position);
                m_agentcamera.Position = selectedobj.Position + CamOffset;
                m_agentcamera.Lookat = selectedobj.Lookat;
            }    
        }

        private void Draw2DInfo(SpriteBatch batch)
        {
            batch.Begin();

            ModelEntity ment = m_worldmodel.SelectedObject;
            if (ment != null)
            {
                ModelObjectView entview = m_entityviews[ment.GetModelType()];
                entview.Draw2D(batch, ment);
            }

            Draw2DWorldInfo(batch);
            m_frameview.Draw2d(batch);
            m_managerview.Draw2d(batch);
            batch.End();
            Helper2d.reset(m_graphics.GraphicsDevice);
        }

        private void Draw2DWorldInfo(SpriteBatch batch)
        {
          
            m_helper2d.Draw2DMapFromWorldMap(batch, m_worldmodel.WorldMap,ViewConstants.WORLDMAPX,ViewConstants.WORLDMAPY,false);
          
         
        }
  
        public void SetLighting(BasicEffect effect)
        {
            effect.AmbientLightColor = new Vector3(m_ambientlightstrength, m_ambientlightstrength, m_ambientlightstrength);
            effect.LightingEnabled = true;
            effect.DirectionalLight0.Enabled = true;
            effect.DirectionalLight0.Direction = Vector3.Normalize(new Vector3(0, 0, 1));      
            effect.DirectionalLight0.DiffuseColor = new Vector3(m_directionallightstrength, m_directionallightstrength, m_directionallightstrength); 
        }
      

        internal void SetCameraPosition(CameraPosition cameraPosition)
        {
            switch (cameraPosition)
            {
                case CameraPosition.CentreHigh: m_maincamera = m_overheadcamera; break;
                case CameraPosition.MouseCamera: if (m_worldmodel.SelectedObject != null) m_maincamera = m_agentcamera; break;
                case CameraPosition.TopLeft: m_maincamera = m_topleftcorner; break;
                case CameraPosition.TopRight: m_maincamera = m_toprightcorner;break;
                case CameraPosition.BottomLeft :m_maincamera = m_bottomleftcorner; break;
                case CameraPosition.BottomRight:m_maincamera = m_bottomrightcorner;break;
            }        
        }
    }
}
