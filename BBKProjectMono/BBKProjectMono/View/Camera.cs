﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace BBKAutoAgents
{
    public class Camera
    {

        Matrix m_projection;
        private float m_aspectratio;
        Vector3 m_worldcentre;
        private Vector3 m_position;
        private Vector3 m_lookat;
        private Vector3 m_up;
        private float m_speed;
        private float m_rotspeed;
        private WholeView m_view;

        public Camera(WholeView view, float aspectrat)
        {
            m_up = new Vector3(0, -1, 1);
            m_position = new Vector3(0, 0, 50f);
     
            m_lookat = new Vector3(0, 0, 0);
            m_speed = 0.03f;
            m_rotspeed = 0.01f;
            m_aspectratio = aspectrat;
            m_projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, m_aspectratio, 0.1f, 10000.0f);
            m_view = view;
        }
      

        public Vector3 Position
        {
            get { return m_position; }
            set { m_position = value; }
        }

      

        public Vector3 Lookat
        {
            get { return m_lookat; }
            set { m_lookat = value; }
        }

       

        public Vector3 Up
        {
            get { return m_up; }
            set { m_up = value; }
        }

      

        public float Speed
        {
            get { return m_speed; }
            set { m_speed = value; }
        }
     

        public float Rotspeed
        {
            get { return m_rotspeed; }
            set { m_rotspeed = value; }
        }

        public Vector3 WorldCentre
        {
            get { return m_worldcentre; }
            set { m_worldcentre = value; }
        }
        public void SetView(BasicEffect effect)
        {
            effect.View = Matrix.CreateLookAt(m_position, m_lookat, m_up);
        }
      

        public Matrix GetView()
        {
            return Matrix.CreateLookAt(m_position, m_lookat, m_up);
        }
        
        public Matrix GetProjection()
        {
            return m_projection;
        }
       
        public void Move(Movement movement)
        {
            switch (movement)
            {
                case Movement.Forward:
                    Vector3 vec = Lookat - Position;
                    Position += m_speed * Vector3.Normalize(vec);
                    Lookat += m_speed * Vector3.Normalize(vec);
                    break;

                case Movement.Backward:
                    Vector3 vec2 = Lookat - Position;
                    Position -= m_speed * Vector3.Normalize(vec2);
                    Lookat -= m_speed * Vector3.Normalize(vec2);
                    break;

                /*This works for Camera on xy plane i.e rotate about Z axis*/
                case Movement.RotateLeft:
                    Matrix camerarot = Matrix.CreateRotationZ(-m_rotspeed); 
                    Vector3 nlookat = Vector3.Transform((m_lookat - Position), camerarot);
                    m_lookat = Position + nlookat;
                    break;
                case Movement.RotateRight:
            
                    Matrix camerarot2 = Matrix.CreateRotationZ(+m_rotspeed );
                    Vector3 nlookat2 = Vector3.Transform((m_lookat - Position), camerarot2);
                    m_lookat = Position + nlookat2;
                    break;
            }


        }

        public void SetCameraPosition(CameraPosition newposition)
        {
            float wcentre = m_view.WorldModel.WorldWidth / 2.0f;
            float hcentre = m_view.WorldModel.WorldHeight / 2.0f;
            float HighCameraZ = - 1.5f * m_view.WorldModel.WorldHeight;
            float LowCameraZ = -0.5f * m_view.WorldModel.WorldHeight;
            switch (newposition)
            {
                case CameraPosition.CentreGround: m_position = new Vector3(wcentre, m_view.WorldModel.WorldHeight, 0);
                    m_lookat = m_position + Vector3.UnitY;
                    break;                
                case CameraPosition.CentreHigh: m_position = new Vector3(wcentre, m_view.WorldModel.WorldHeight, HighCameraZ);
                    m_lookat = new Vector3(wcentre, hcentre, 0);
                    break;
                case CameraPosition.TopLeft: m_position = new Vector3(0, 0, LowCameraZ); break;
                case CameraPosition.TopRight: m_position = new Vector3(m_view.WorldModel.WorldWidth, 0, LowCameraZ); break;
                case CameraPosition.BottomLeft: m_position = new Vector3(0, m_view.WorldModel.WorldHeight, LowCameraZ); break;
                case CameraPosition.BottomRight: m_position = new Vector3(m_view.WorldModel.WorldHeight, m_view.WorldModel.WorldWidth, LowCameraZ); break;

            }
            m_up = new Vector3(0, 0, -1);
            m_lookat = new Vector3(wcentre, hcentre, 0);

        }

     

    }
}
