﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public abstract class BaseMap
    {
        protected int m_height;
        public int Height
        { get { return m_height; } }
        protected int m_width;
        public int Width
        { get { return m_width; } }

        Location m_currentlocation = Location.Null;
        public Location CurrentLocation
        { get { return m_currentlocation; } set { m_currentlocation = value; } }

        Location m_targetlocation = Location.Null;
        public Location TargetLocation

        { get { return m_targetlocation; } set { m_targetlocation = value; } }
        public static ModelType[] m_noteligibletypes = { ModelType.Floor };
        public BaseMap(int width, int height)
        {
            Rebuild(width, height);           
        }

        public virtual void Rebuild(int width, int height)
        {
            this.m_height = height;
            this.m_width = width;
            createallslots();
        
        }
        public bool ValidLocation(Location loc)
        {
            //Not Allowed off map or on edge
            if (loc.X > m_width - 1 || loc.X < 0 || loc.Y < 0 || loc.Y > m_height - 1)
                return false;
            return true;
        }


        public bool OnEdge(Location loc)
        {
            if (loc.X == m_width - 1 || loc.X == 0 || loc.Y == 0 || loc.Y == m_height - 1)
                return true;
            return false;

        }
        protected string GetErrorMessage(Location loc)
        {
            string msg = "Location (" + loc.X + "," + loc.Y + ") Not valid for map (" + m_width + ", " + m_height + ")";
            return msg;
        }

        private void createallslots()
        {
            for (int i = 0; i < m_width; i++)
            {
                for (int j = 0; j < m_height; j++)
                {
                    CreateASlot(new Location(i, j));
                   
                }
            }
        }

        protected abstract void CreateASlot(Location loc);
 

        public bool EligibleType(ModelType testmtype)
        {
            if (m_noteligibletypes.Contains<ModelType>(testmtype))
                return false;
            return true;

        }

        protected int RoundWidth(float x)
        {
            if (x < 0)
                return 0;
            if (x > m_width - 1)
                return m_width - 1;
            return RoundPosition(x);

        }

        protected int RoundHeight(float y)
        {
            if (y < 0)
                return 0;
            if (y > m_height - 1)
                return m_height - 1;
            return RoundPosition(y);
        }
        protected int RoundPosition(float x)
        {
            int ipos = (int)x;
            x = ipos - x;
            if (x > 0.5f)
                ipos++;
            return ipos;
        }


        public Vector3 LocationToVector(Location loc)
        {
            //Add 05f to put position in middle of vector space
            return new Vector3(loc.X + 0.5f, loc.Y + 0.5f, 0);
        }
        public Location VectortoLocation(Vector3 vec3)
        {
            Location loc = new Location(RoundWidth(vec3.X), RoundHeight(vec3.Y));
            return loc;
        }

        public List<Location> GetSurroundingLocations(Location loc)
        {
            return GetSurroundingLocations(loc, true);
        }


        public List<Location> GetSurroundingLocations(Location loc, bool includeblocked)
        {
            List<Location> surround = new List<Location>();

            Location[] locs = new Location[8];
            locs[0] = new Location(loc.X - 1, loc.Y);
            locs[1] = new Location(loc.X - 1, loc.Y - 1);
            locs[2] = new Location(loc.X, loc.Y - 1);
            locs[3] = new Location(loc.X + 1, loc.Y - 1);
            locs[4] = new Location(loc.X + 1, loc.Y);
            locs[5] = new Location(loc.X + 1, loc.Y + 1);
            locs[6] = new Location(loc.X, loc.Y + 1);
            locs[7] = new Location(loc.X - 1, loc.Y + 1);
            foreach (Location posloc in locs)
            {
                if (ValidLocation(posloc))
                {
                    if (!includeblocked && IsBlocked(posloc))
                        continue;
                    surround.Add(posloc);
                }
            }
            return surround;
        }

        public abstract bool Contains(Location loc, ModelType type);
   

        public bool IsBlocked(int x, int y)
        {
            return IsBlocked(new Location(x, y));
        }
        public bool IsBlocked(Location loc)
        {
            if (!ValidLocation(loc))
                return true;
            if (OnEdge(loc))
                return true;
            if (Contains(loc,ModelType.Wall))
                return true;
            if (m_targetlocation == loc)
                return false;
            if (!m_currentlocation.Equals(Location.Null) && m_currentlocation.Adjacent(loc))
            {
                foreach (ModelType mtype in ModelObjectFactory.BlockingTypes)
                {
                    if (Contains(loc,mtype))
                        return true;
                }
            }
            return false;
        }
        public void clearmap()
        {
            clearmap(MapUpdateMode.ClearAll);
        }

        public void clearmap(MapUpdateMode updatemode)
        {
            if (updatemode == MapUpdateMode.KeepAll)
                return;
            for (int i = 0; i < m_width; i++)
            {
                for (int j = 0; j < m_height; j++)
                {
                   ClearSlot(new Location(i,j),updatemode);
                }

            }
        }

        public virtual bool Empty(int x, int y)
        {
            throw new Exception("Must be overriden");
        }
        public bool IsFree(Location loc)
        {
            return IsFree(loc.X, loc.Y);
        }

        public bool IsFree(Vector3 locvec)
        {
            return IsFree(VectortoLocation(locvec));
        }
        public bool IsFree(int x, int y)
        {
            //if (ValidLocation(new Location(x, y)) && !m_map[x, y].Contains(ModelType.Wall) )
            if (ValidLocation(new Location(x, y)) && Empty(x,y) && !OnEdge(new Location(x, y)))
                return true;
            return false;
        }

        public List<Location> GetAllLocations()
        {
            List<Location> loclist = new List<Location>();
            for (int x = 0; x < m_width; x++)
            {
                for (int y = 0; y < m_height; y++)
                {
                    loclist.Add(new Location(x, y));
                }
            }
            return loclist;
        }

        public abstract List<LocalModelObjectData> GetLocationSlotObjs(Location loc);
   
        public abstract void ClearSlot(Location loc, MapUpdateMode mode);
     

        public List<Location> GetDistanceSortedList()
        {
            return GetDistanceSortedList(CurrentLocation);
        }

       
        public List<Location> GetDistanceSortedList(Location centrelocation)
        {
            List<Location> locations = GetAllLocations();
            locations.Sort(delegate(Location l1, Location l2)
            {

                int l1dist = ((centrelocation.X - l1.X) * (centrelocation.X - l1.X)) + ((centrelocation.Y - l1.Y) * (centrelocation.Y - l1.Y));
                int l2dist = ((centrelocation.X - l2.X) * (centrelocation.X - l2.X)) + ((centrelocation.Y - l2.Y) * (centrelocation.Y - l2.Y));
                return l1dist.CompareTo(l2dist);
            }
            );
            return locations;
                
        }

        public bool SameLocation(Vector3 cheese, Vector3 m_ultimatedestination)
        {
            return VectortoLocation(cheese) == VectortoLocation(m_ultimatedestination);
        }
  
    }
}
