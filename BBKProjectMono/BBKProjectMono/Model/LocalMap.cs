﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace BBKModel
{
    public class LocalMap : BaseMap
    {

        private const int DEFAULTUPDATEDISTANCE = 15;
      

        static Location NULLLOCATION = new Location(-1, -1);

        protected LocalLocationSlot[,] m_slotgrid;



        public LocalMap(int width, int height) : base(width, height) { }



      


        public override List<LocalModelObjectData> GetLocationSlotObjs(Location loc)
        {
         
            return m_slotgrid[loc.X, loc.Y].SlotObjects;
         
        }

     
        public override bool Contains(Location loc, ModelType type)
        {
            return m_slotgrid[loc.X, loc.Y].Contains(type);
        }
        public void SetLocalMapForUpdate(LocalMap localmap)
        {
            if (localmap.m_height == 0 && localmap.m_width == 0)
                localmap.Rebuild(m_width, m_height);
            if (localmap.m_width != m_width || localmap.m_height != m_height)
                throw new Exception("LocalMap Not compatible with worldmap");

        }

        public void MarkPath(BBKModel.PathFinder.Node node)
        {
            while (node != null)
            {
                m_slotgrid[node.Location.X, node.Location.Y].Marked = true;
                node = node.parent;
            }
        }

        public void MarkPath(Stack<Vector3> waypoints)
        {
            Vector3 vec;
            while (waypoints.Count > 0)
            {
                vec = waypoints.Pop();
                Location loc = VectortoLocation(vec);
                m_slotgrid[loc.X, loc.Y].Marked = true;
            }
        }


        public TimeSpan TimeSinceLastUpdated(Location loc)
        {
            return m_slotgrid[loc.X, loc.Y].TimeSinceLastUpdate;
        }

        public TimeSpan TimeStamp(Location loc)
        {
            return m_slotgrid[loc.X, loc.Y].TimeStamp;
        }





        public override void Rebuild(int width, int height)
        {
            m_slotgrid = new LocalLocationSlot[width, height];
            base.Rebuild(width, height);

            clearmap();
        }

        protected override void CreateASlot(Location loc)
        {
            m_slotgrid[loc.X, loc.Y] = new LocalLocationSlot();
        }



        public int GetTerrainPenalty(Location location)
        {
            return m_slotgrid[location.X, location.Y].Penalty;
        }
        public int SetTerrainPenalty(Location location, int value)
        {
            return m_slotgrid[location.X, location.Y].Penalty = value;
        }



        /*Used for Testing only*/
        public List<Location> GetAllLocation(ModelType tmodel)
        {
            List<Location> matchlocs = new List<Location>();
            IsType IsType = new IsType(tmodel);
            for (int y = 0; y < m_height; y++)
            {
                for (int x = 0; x < m_width; x++)
                {
                    Location loc = new Location(x, y);
                    if (Contains(loc, tmodel))
                        matchlocs.Add(loc);
                }
            }
            return matchlocs;

        }


        internal void ClearPenalties()
        {
            for (int x = 0; x < m_width; x++)
            {
                for (int y = 0; y < m_height; y++)
                {
                    m_slotgrid[x, y].Penalty = 0;
                }
            }
        }

        public void SetPenalties(List<Location> badlocs, int range)
        {
            int initpen = ModelConstants.MAXTERRAINPENALTY;
            foreach (Location loc in badlocs)
            {
                if (ValidLocation(loc))            
                    SetSurroundingPenalties(loc, range, initpen);
                
            }
        }

        public void SetSurroundingPenalties(Location centreloc, int range, int initpen)
        {
            m_slotgrid[centreloc.X, centreloc.Y].Penalty = initpen;

            int startx = centreloc.X - range;
            int starty = centreloc.Y - range;
            for (int x = startx; x < m_width && x <= centreloc.X + range; x++)
            {
                for (int y = starty; y < m_height && y <= centreloc.Y + range; y++)
                {
                    Location curloc = new Location(x, y);
                    if (ValidLocation(curloc))
                        m_slotgrid[curloc.X, curloc.Y].Penalty += CalculatePenalty(curloc, centreloc, initpen, range);
                }
            }
        }

        public bool IsDiagonal(Location current, Location dest)
        {
            return (current.X != dest.X && current.Y != dest.Y);
        }

        public bool BlockedDiagonal(Location current, Location dest)
        {
            if (current.X == dest.X || current.Y == dest.Y)
                return false;
            //block above

            //top left
            Location above = new Location(current.X, current.Y - 1);
            Location below = new Location(current.X, current.Y + 1);
            Location left = new Location(current.X - 1, current.Y);
            Location right = new Location(current.X + 1, current.Y);
            if (!IsFree(above))
            {
                if (dest.X == current.X - 1 && dest.Y == current.Y - 1)
                    return true;
                if (dest.X == current.X + 1 && dest.Y == current.Y - 1)
                    return true;
            }
            if (!IsFree(below))
            {
                if (dest.X == current.X + 1 && dest.Y == current.Y + 1)
                    return true;
                if (dest.X == current.X - 1 && dest.Y == current.Y + 1)
                    return true;
            }
            if (!IsFree(left))
            {
                if (dest.X == current.X - 1 && dest.Y == current.Y + 1)
                    return true;
                if (dest.X == current.X - 1 && dest.Y == current.Y - 1)
                    return true;
            }
            if (!IsFree(right))
            {
                if (dest.X == current.X + 1 && dest.Y == current.Y + 1)
                    return true;
                if (dest.X == current.X + 1 && dest.Y == current.Y - 1)
                    return true;
            }
            return false;
        }

        public int CalculatePenalty(Location curloc, Location centreloc, int initpen, int range)
        {
            float xdist = Math.Abs(centreloc.X - curloc.X);
            float ydist = Math.Abs(centreloc.Y - curloc.Y);
            return (int)(initpen - (((xdist + ydist) / (range + range)) * initpen));
        }

        internal Target GetTarget(Location tloc, ModelType tmodel)
        {
            if (!ValidLocation(tloc))
                return Target.NullTarget;
      
            LocalModelObjectData targetlsobj = m_slotgrid[tloc.X, tloc.Y].Find(tmodel);
            if (targetlsobj.IsNull())
                return Target.NullTarget;
       
           Target target = new Target(targetlsobj.Position, targetlsobj.ModelID, m_slotgrid[tloc.X, tloc.Y].TimeStamp);
           
           
            return target;
        
        }

        public override bool Empty(int x, int y)
        {
            return m_slotgrid[x, y].Empty;
        }

        internal Location GetBestGhostLocation(Target target)
        {
            int closest = int.MaxValue;
            Location bestloc = Location.Null;
            Location targetloc = VectortoLocation(target.Position);
            for (int x = 0; x < m_width; x++)
            {
                for (int y = 0; y < m_height; y++)
                {
                    if (IsBlocked(x, y))
                        continue;
                    if (m_slotgrid[x, y].TimeStamp < target.TimeAtLastSeen)
                    {
                        int xdist = targetloc.X - x;
                        int ydist = targetloc.Y - y;
                        int distance = (xdist * xdist) + (ydist * ydist);
                        if (distance < closest)
                        {
                            closest = distance;
                            bestloc = new Location(x, y);
                        }
                    }
                }

            }
            return bestloc;

        }


        internal Vector3 GetBestGhostPosition(Target target)
        {
            Location loc = GetBestGhostLocation(target);
            return LocationToVector(loc);
        }

        public void MarkPath(Location targloc)
        {
            if (ValidLocation(targloc))
                m_slotgrid[targloc.X, targloc.Y].Marked = true; ;
        }

        internal Location GetOldestLocation()
        {
            TimeSpan oldest = TimeSpan.MaxValue;
            Location loc = Location.Null;
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < m_height; y++)
                {
                    if (IsBlocked(x, y))
                        continue;
                    if (m_slotgrid[x, y].TimeStamp < oldest)
                    {
                        oldest = m_slotgrid[x, y].TimeStamp;
                        loc = new Location(x, y);
                    }
                }

            }
            return loc;
        }

        internal bool Marked(Location curloc)
        {
            return m_slotgrid[curloc.X, curloc.Y].Marked;
        }

        public override void ClearSlot(Location loc, MapUpdateMode updatemode)
        {
            m_slotgrid[loc.X, loc.Y].ClearSlot(updatemode);
        }

        public void ResetTimeOnSlot(Location loc)
        {
            m_slotgrid[loc.X, loc.Y].ResetTime();
        }

        public void SetContent(Location loc, LocalModelObjectData locationSlotObject)
        {
            m_slotgrid[loc.X, loc.Y].SetContent(locationSlotObject);
        }


        internal List<Location> GetBlock(Location Location, int range)
        {
            List<Location> blocklist = new List<Location>();
            for (int i = Location.X - range; i < Location.X + range; i++)
            {
                for (int j = Location.Y - range; j < Location.Y + range; j++)
                {
                    Location loc = new Location(i, j);
                    if (ValidLocation(loc))
                         blocklist.Add(loc);
                }
            }
            return blocklist;
        }
    }
}
