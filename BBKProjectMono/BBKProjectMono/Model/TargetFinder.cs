﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class TargetFinder
    {
      
        ModelType m_targettype;
        TimeSpan m_timeout;
        IMapProvider m_provider;
        Dictionary<Guid, Vector3> m_previoustargets;
        bool m_useghost = false;

        public bool UseGhost
        { get { return m_useghost; } set { m_useghost = value; } }

        public bool Contains(Guid id)
        {
            return m_targets.Contains(new Target(Vector3.Zero, id));
        }

        public ModelType ModelType
        { get { return m_targettype; } }

        private bool m_targetschanged;
        public TargetFinder(IMapProvider provider, ModelType ttype, TimeSpan targettimeout):this(provider,ttype,targettimeout,true){}
       
        public TargetFinder(IMapProvider provider, ModelType ttype, TimeSpan targettimeout, bool useghost)
        {
            m_provider = provider;
            m_targets = new List<Target>();
            m_targettype = ttype;
            m_timeout = targettimeout;
            m_previoustargets = new Dictionary<Guid, Vector3>();
            m_useghost = useghost;

        }
        List<Target> m_targets;
        Target m_currenttarget;
        public Target CurrentTarget
        { get { return m_currenttarget; } }
   
        protected Target MostRecentTarget()
        {
            if (m_targets.Count == 0)
                return Target.NullTarget;
            Target target = m_targets[0];
            foreach (Target mtarg in m_targets)
            {
                if (mtarg.TimeAtLastSeen > target.TimeAtLastSeen)
                    target = mtarg;
            }
            return target;
        }
        private void AddTargets(List<Target> newtargs)
        {
            foreach (Target targ in newtargs)
            {
                UpdateTargets(targ);
            }        
        }

       
public void Update(List<Location> visiblelocs)
{
    if (m_targettype == ModelType.None)
        return;
    CopyOldTargetList();
    ClearBadTargets();
    AddTargets(RetrieveVisibleTargetList(visiblelocs));
    Target target = ClosestCurrentlyVisibleTarget();
    if (target.IsNull() )
    {
        target = MostRecentOrGhost();
    }     
    m_currenttarget = target;
    CheckIfTargetsChanged();
}

        public Target MostRecentOrGhost()
        {
            Target target = MostRecentTarget();

            if (!target.IsNull() && IsNoLongerThere(target))
            {
                if (m_useghost)
                    target = FindGhostTarget(target);
                else
                    target = Target.NullTarget;
            }
            return target;
        }

        public List<Target> RetrieveVisibleTargetList(List<Location> visiblelocs)
        {
           List<Target> targetlist = new List<Target>();
           foreach (Location loc in visiblelocs)
            {
                Target target = GetMapTarget(loc);
                if (target.IsNull())
                    continue;
                targetlist.Add(target);
            }
            return targetlist;
        }

        public Target GetMapTarget(Location tloc)
        {
            if (m_provider.LocalMap.TimeSinceLastUpdated(tloc) > m_timeout)
                return Target.NullTarget;
            if (!m_provider.LocalMap.Contains(tloc, m_targettype))
                return Target.NullTarget;
            return m_provider.LocalMap.GetTarget(tloc, m_targettype);
        }

        private bool IsNoLongerThere(Target target)
        {
            Location loc = m_provider.LocalMap.VectortoLocation(target.Position);
            if (m_provider.LocalMap.TimeStamp(loc) > target.TimeAtLastSeen)
                return true;
            return false;
        }

        public Target ClosestCurrentlyVisibleTarget()
        {
            Target best = Target.NullTarget;
            float distance = float.MaxValue;
            foreach (Target target in m_targets)
            {
                if (target.TimeAtLastSeen > WallClock.TotalTime - ModelConstants.SHORTTIME)
                {
                    float thisdist = Vector3.Distance(m_provider.Position, target.Position);
                    if (thisdist < distance)
                    {
                        distance = thisdist;
                        best = target;
                    }
                }
            }
            return best;
        }

        private Target FindGhostTarget(Target target)
        {          
           Vector3 pos = m_provider.LocalMap.GetBestGhostPosition(target);
           Target ghosttarg = new Target(pos, target.ID, target.TimeAtLastSeen);
           return ghosttarg;
        }

        private void UpdateTargets(Target target)
        {
            
            HaveBetter better = new HaveBetter(target);
            m_targets.RemoveAll(better.HaveFoundBetter);
            if (!m_targets.Contains(target))
            m_targets.Add(target);
     
        }

        private void ClearBadTargets()
        {
            m_targets.RemoveAll(BadTarget);
        }

        private bool BadTarget(Target target)
        {
            if (target.TimeAtLastSeen < WallClock.TotalTime - m_timeout)
                return true;
            if (!m_useghost && IsNoLongerThere(target))
                return true;
            return false;
        }

        public List<Location> TargetLocations
        {
            get
            {
                List<Location> locs = new List<Location>();
                foreach (Target targ in m_targets)
                {
                    locs.Add(m_provider.LocalMap.VectortoLocation(targ.Position));
                }
                return locs;
            }
        }
        class HaveBetter
        {
            Target m_bettertarget;
            public HaveBetter(Target target)
            {
                m_bettertarget = target;
            }
            public bool HaveFoundBetter(Target oldtarget)
            {
                return oldtarget.ID == m_bettertarget.ID && m_bettertarget.TimeAtLastSeen >= oldtarget.TimeAtLastSeen;
               
            }
        }

        public void RemoveTarget(Guid guid)
        {
            Target target = new Target(Vector3.Zero, guid);
            m_targets.Remove(target);
        }

        public List<Target> Targets
        {
            get { return m_targets; }
        }

        public List<Target> ClosestTargets
        {
            get
            {
                m_targets.Sort(delegate(Target t1, Target t2)
            {
                float dist1 = Vector3.DistanceSquared(m_provider.Position, t1.Position);
                float dist2 = Vector3.DistanceSquared(m_provider.Position, t2.Position);

                return dist1.CompareTo(dist2);
            });
                return m_targets;
            }
        }
        private void CopyOldTargetList()
        {
            m_previoustargets.Clear();
            foreach (Target targ in m_targets)
            {
                m_previoustargets.Add(targ.ID, targ.Position);
            }
        }

        private void CheckIfTargetsChanged()
        {
            m_targetschanged = false;
            foreach (Target targ in m_targets)
            {
                if (m_previoustargets.ContainsKey(targ.ID)
                    && !m_provider.LocalMap.SameLocation(m_previoustargets[targ.ID], targ.Position))
                    m_targetschanged = true;
            }
        }

        
        public bool TargetsLocationsChanged
        { get { return m_targetschanged; } }


        internal float AverageDistance(Vector3 curlocpos)
        {
            if (m_targets.Count == 0)
                return -1;
            float dist = 0;
            foreach (Target targ in m_targets)
            {
                dist += Vector3.Distance(targ.Position, curlocpos);
            }
            return dist / m_targets.Count;
        }

        internal float Closest(Vector3 curlocpos)
        {
            if (m_targets.Count == 0)
                return -1;
            float closest = float.MaxValue;
            foreach (Target targ in m_targets)
            {
                float dist = Vector3.Distance(targ.Position, curlocpos);
                if (dist < closest)
                    closest = dist;
            }
            return closest ;

        }
    }
}
