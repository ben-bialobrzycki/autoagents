﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBKModel
{
    public struct Location
    {
        private int x;
        public int X
        { get { return x; } set { x = value; } }
        private int y;
        public int Y
        { get { return y; } set { y = value; } }
        public Location(int setx, int sety)
        {
            x = setx;
            y = sety;
        }
        public override string ToString()
        {
            string s = "{X: " + x.ToString() + " Y: " + y.ToString();
            return s;
        }


        public override bool Equals(object obj)
        {
            Location loc = (Location)obj;
            return this == loc;
        }
        public static bool operator ==(Location loc1, Location  loc2)
        {
            return loc1.x == loc2.x && loc1.y == loc2.y; 
        }
        public static bool operator !=(Location loc1, Location loc2)
        {
            return loc1.x != loc2.x || loc1.y != loc2.y;
        }
        public bool Adjacent(Location loc)
        {
            if (Math.Abs(loc.x - this.x) <= 1 && Math.Abs(loc.y - this.y) <= 1)
                return true;
            return false;
        }

        public static Location Null
        {
            get { return new Location(-1, -1); }
        }

    }
}
