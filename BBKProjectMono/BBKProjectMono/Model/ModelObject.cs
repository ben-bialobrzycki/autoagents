﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace BBKModel
{
    public abstract class ModelObject : IBBKDrawable
    {
        protected Vector3 m_position;
        protected Vector3 m_rotation;
        protected ModelType m_type;
        protected IWholeModel m_worldmodel;
        protected Vector3 m_size;
        protected bool m_dead;

        protected ShapeType m_shape;
        public ShapeType Shape
        { get { return m_shape; } }

        protected Guid m_UniqueID;
        public Guid UniqueID
        { get { return m_UniqueID; } }

        public Vector3 Position
        { get { return m_position; } set { m_position = value; } }
        public ModelObject(IWholeModel wmod, Vector3 pos, ShapeType shape)
        {
            m_worldmodel = wmod;
            m_position = pos;
            m_dead = false;
            m_shape = shape;
            m_UniqueID = Guid.NewGuid();
            Initialise();
        }

        public virtual BoundingBox GetBoundingBox()
        {
            return GetBoundingBox(m_position);
        }

        public virtual bool Dead
        { get { return m_dead; } }

        public virtual void HandleCollision(ModelObject CollidingObject, bool initiator)
        {
            //Depends On Both Types
        }

        public virtual void HandleCollisionResult(ModelObject CollidingObject, bool initiator)
        {
            //Depends On Both Types
        }

        protected virtual void Initialise()
        {
            //For Inherited types
        }

        protected void SetSize(float val)
        {
            m_size = new Vector3(val,val,val);
        }
        protected void SetSize(Vector3 size)
        {
            m_size = size;
        }
        public virtual BoundingBox GetBoundingBox(Vector3 vec)
        {    
            Vector3 bottom = new Vector3(vec.X, vec.Y, vec.Z);
            Vector3 top = new Vector3(vec.X, vec.Y, vec.Z);
            bottom.X -= m_size.X / 2.0f;
            bottom.Y -= m_size.Y / 2.0f;
            bottom.Z -= m_size.Z / 2.0f;
            top.X += m_size.X / 2.0f;
            top.Y += m_size.Y / 2.0f;
            top.Z += m_size.Z / 2.0f;
            BoundingBox box = new BoundingBox(bottom, top);
            return box;
        
        }

        public BoundingSphere GetBoundingSphere()
        {
            return GetBoundingSphere(m_position);
        }

        public BoundingSphere GetBoundingSphere(Vector3 m_position)
        {
            return new BoundingSphere(m_position, Radius);
        }

        public float Radius
        { get { return m_size.X; } } 
      
        public LocalModelObjectData GetLocationSlotObject()
        {
            return new LocalModelObjectData(m_type, UniqueID,m_position);
        }
       

        
        #region IDrawable Members
        public Microsoft.Xna.Framework.Vector3 GetPosition()
        {
            return m_position; ;
        }

        public ModelType GetModelType()
        {
            return m_type;
        }

        public Vector3 GetRotation()
        {
            return m_rotation;
        }

        public virtual void Update(GameTime gt)
        {

        }


        #endregion
    }
}
