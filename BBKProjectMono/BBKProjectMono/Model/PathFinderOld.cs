﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class PathFinderOld
    {
   
        LocalMap m_map;
        public Location goal;
        public Location start;
       
        List<Node> m_openlist;
        List<Node> m_closedlist;
 

        public PathFinderOld(LocalMap map)
        {
            m_map = map;
            m_openlist = new List<Node>();
            m_closedlist = new List<Node>();
        }
 
 
        public Stack<Vector3> findshortpath(Vector3 startvec, Vector3 endvec)
        {       
            start = m_map.VectortoLocation(startvec);
            goal = m_map.VectortoLocation(endvec);
            DebugHelper.AppendToTempFile(m_map, start.ToString(), goal.ToString(),false);
            if (!m_map.IsFree(goal.X, goal.Y))
                return null;
               
            m_openlist.Clear();
            m_closedlist.Clear();
            Node startnode = new Node(start.X, start.Y);
            m_openlist.Add(startnode);
            bool looking = true;
            Node currentnode = null;
            while (looking)
            {
                int max = m_map.Width * m_map.Height;
                if (m_openlist.Count > max || m_closedlist.Count > max)
                    throw new Exception("Pathfinder Exceeded Max list size");// Console.WriteLine("max {0} openlist {1} closed {2}");
                currentnode = lowestf();
    
                if (currentnode == null)
                    break;
                move_to_closed(currentnode);
                if (currentnode.Location.X == goal.X && currentnode.Location.Y == goal.Y)
                    looking = false;
                else
                    add_surrounding(currentnode);
            }
    
            Stack<Vector3> vecstack = build_vec_stack(currentnode);
            return vecstack;

        }
        private Stack<Location> build_stack(Node node)
        {
            Stack<Location> stack = new Stack<Location>();
            while (node != null)
            {
                Location co = new Location(node.Location.X, node.Location.Y);
                stack.Push(co);
                node = node.parent;
            }
            return stack;
        }

        private Stack<Vector3> build_vec_stack(Node node)
        {
            Stack<Vector3> stack = new Stack<Vector3>();
            while (node != null)
            {
                int x = stack.Count;
                Location co = new Location(node.Location.X, node.Location.Y);
                stack.Push(m_map.LocationToVector(co));
                node = node.parent;
            }
            return stack;
        }
        private void move_to_closed(Node currentnode)
        {
            m_closedlist.Add(currentnode);
            m_openlist.Remove(currentnode);
        }
        private Node lowestf()
        {
            // Node lowest = null;
            m_openlist.Sort(delegate(Node n1, Node n2) { return n1.f.CompareTo(n2.f); });
            if (m_openlist.Count > 0)
                return m_openlist[0];
            else
                return null;
    
        }
        private void add_surrounding(Node current)
        {
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (!(i == 0 && j == 0))
                    {
                        int nx = current.Location.X + i;
                        int ny = current.Location.Y + j;
                        if (m_map.IsFree(nx, ny) && !in_closed(nx, ny))
                        {
                            if (!m_map.BlockedDiagonal(current.Location, new Location(nx, ny)))
                            {
                                Node node = new Node(nx, ny);
                                node.parent = current;
                                set_values(node);
                                Node check = in_open(nx, ny);
                                if (check == null)
                                {
                                    m_openlist.Add(node);
                                }
                              //  else if (check.g < node.g)
                                else if (node.g < check.g)
                                {
                                    check.parent = current;
                                    set_values(check);
                                }

                                //if (check == null || check.g < node.g)
                                //    m_openlist.Add(node);
                                //else
                                //{
                                //    check.parent = node;
                                //    set_values(check);
                                //}
                            }

                        }
                    }

                }

            }
        }

        //private bool cut_corner(Location current, Location dest)
        //{
        //    if (current.X == dest.X || current.Y == dest.Y)
        //        return false;
        //    //block above

        //    //top left
        //    Location above = new Location(current.X, current.Y - 1);
        //    Location below = new Location(current.X, current.Y + 1);
        //    Location left = new Location(current.X - 1, current.Y);
        //    Location right = new Location(current.X + 1, current.Y);
        //    if (!m_map.IsFree(above))
        //    {
        //        if (dest.X == current.X - 1 && dest.Y == current.Y - 1)
        //            return true;
        //        if (dest.X == current.X + 1 && dest.Y == current.Y - 1)
        //            return true;
        //    }
        //    if (!m_map.IsFree(below))
        //    {
        //        if (dest.X == current.X + 1 && dest.Y == current.Y + 1)
        //            return true;
        //        if (dest.X == current.X - 1 && dest.Y == current.Y + 1)
        //            return true;
        //    }
        //    if (!m_map.IsFree(left))
        //    {
        //        if (dest.X == current.X - 1 && dest.Y == current.Y + 1)
        //            return true;
        //        if (dest.X == current.X - 1 && dest.Y == current.Y - 1)
        //            return true;
        //    }
        //    if (!m_map.IsFree(right))
        //    {
        //        if (dest.X == current.X + 1 && dest.Y == current.Y + 1)
        //            return true;
        //        if (dest.X == current.X + 1 && dest.Y == current.Y - 1)
        //            return true;
        //    }
        //    return false;
        //}

 
        private bool in_closed(int x, int y)
        {
            foreach (Node node in m_closedlist)
            {
                if (node.Location.X == x && node.Location.Y == y)
                    return true;
            }
            return false;
        }
        private Node in_open(int x, int y)
        {
            foreach (Node node in m_openlist)
            {
                if (node.Location.X == x && node.Location.Y == y)
                    return node;
            }
            return null;
        }
        private void set_values(Node node)
        {
            int xdist = node.Location.X - node.parent.Location.X;
            int ydist = node.Location.Y - node.parent.Location.Y;

            if (((xdist * xdist) + (ydist * ydist)) == 1)
                node.g = node.parent.g + 10;
            else
                node.g = node.parent.g + 14;
            node.h = 10 * (positive(node.Location.X - goal.X) + positive(node.Location.Y - goal.Y));
            node.f = node.g + node.h;
        }

        private int positive(int x)
        {
            if (x > 0)
                return x;
            else
                return -x;
        }

  
      

        
       private void display_open()
       {
           Console.WriteLine("Open List");
           show_list(m_openlist);

       }

       private void display_closed()
       {
           Console.WriteLine("Closed List");
           show_list(m_closedlist);

       }
       private void show_list(List<Node> list)
       {
           foreach (Node nod in list)
           {
               Console.WriteLine("Location " + "(" + nod.Location.X + "," + nod.Location.Y + ")"
                 + " f g h = " + nod.f + " " + nod.g + " " + nod.h);
           }
       }

  
    }
    public class Node
    {
        public Node parent = null;
        public int f = 0; // total value
        public int g = 0;  //distance to start
        public int h = 0;  // distance to destination
        public Location Location;
        public Node(int x, int y)
        {
            Location.X = x;
            Location.Y = y;
        }
    }
   
}
