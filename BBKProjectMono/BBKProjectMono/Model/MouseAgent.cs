﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace BBKModel
{
    public abstract class MouseAgent : ModelEntity
    {
      
     
        protected float m_rotspeed = ModelConstants.PLAYERROTATIONSPEED; 
        protected const float ROTATIONPERIOD = ModelConstants.ROTATIONPERIOD;
        protected EntityHealth m_health;
      

        protected float PredatorDistanceThreshold
        { get { return m_map.Height * ModelConstants.PREDATORTHRESHOLDMAPPERCENTAGE; } }
       
        protected Timer m_timer;

       
        protected Timer m_statelimiter;
       
        protected AgentMode m_mode;
        protected AgentMode m_previousmode;


        protected float m_previousrotation;
        protected Vector3 m_currentdestination;
        protected Vector3 m_ultimatedestination;

        protected int m_initialwaypointcount;

        protected bool m_preyfound = false;
        protected Stack<Vector3> m_waypoints;

        public Stack<Vector3> WayPoints
        { get { return m_waypoints; } }
        protected bool m_usingwaypoints = false;

        protected bool m_checkdone = false;

        protected List<Location> m_badlocations;
     

        #region Properties

        public bool PredatorOrPreyFound
        { get { return m_preyfound; } }

        public AgentMode Mode
        { get { return m_mode; } }

        public Vector3 CurrentDestination
        { get { return m_currentdestination; } }

        public Location CurrentDestinationAsLocation
        {
            get { return m_map.VectortoLocation(m_currentdestination);}
        }

        public Vector3 UltimateDestination
        { get { return m_ultimatedestination;}}

        public Location UltimateDestinationAsLocation
        { get { return m_map.VectortoLocation(m_ultimatedestination);}}
        public bool PlayerControlled
        { get { return m_mode == AgentMode.PlayerControlled; } 
            set 
            { 
                if (value == true)
                {
                    SetMode(AgentMode.PlayerControlled);
                    m_lookat = m_position + Vector3.UnitY;
                }
                else
                    SetMode(AgentMode.Wandering);                
            } 
        }

        public bool Immune
        { get { return m_health.Immune; } }

        public override bool Dead
        { get {   return m_health.Amount <= 0;}  }

        public bool UsingWayPoints
        { get { return m_usingwaypoints; } }
      
        #endregion

        public MouseAgent(IWholeModel wmod, Vector3 position)
            : base(wmod, position)
        {        
            m_timer = new Timer();
            m_statelimiter = new Timer();
          
            m_health = new EntityHealth(this);
            SetSize(ModelConstants.MOUSERADIUS);
            SetMode(AgentMode.Wandering);           
        }

        public override BoundingBox GetBoundingBox()
        {
            throw new Exception("This is not a Box call Get Sphere");
        }

        public override BoundingBox GetBoundingBox(Vector3 vec)
        {
            throw new Exception("This is not a Box call Get Sphere");
        }

        public void SetMode(AgentMode newmode)
        {
            SetMode(newmode,false);
        }
        public void SetMode(AgentMode newmode, bool allowreset)
        {
            if (!allowreset && newmode == m_mode)
                return;
 
            bool succesful = true;

            switch (newmode)
            {
                case AgentMode.HeadingToTargetByWayPoint:
                    {
                        succesful = SetWayPoints(); break;
                    }
                case AgentMode.Avoiding: StartAvoid(); break;
                case AgentMode.Thinking: m_timer.Reset(ModelHelper.GetRandomFloat(ModelConstants.MAXTHINKTIME)); break;
                case AgentMode.RunningAway:
                    {
                        Location safeloc = GetSafeLocation();
                        m_ultimatedestination = m_map.LocationToVector(safeloc);
                        succesful = SetWayPoints();  break;
                    }
                case AgentMode.QuickCheck:
                    {
                        m_checkdone = false;
                        if (m_predatorfinder.CurrentTarget.IsNull()) 
                            m_rotation.Z = m_rotation.Z + MathHelper.Pi;
                        else
                            SetDirection(m_predatorfinder.CurrentTarget.Position - m_position);
                        m_timer.Reset(ModelConstants.QUICKCHECKTIME);
                        break;
                    }
            }
            if (succesful)
            {
                if (m_mode != AgentMode.Surveying && m_mode != AgentMode.Thinking && m_mode != AgentMode.QuickCheck && m_mode != AgentMode.Avoiding)
                     m_previousmode = m_mode;
                m_mode = newmode;
                SetMovementMode();
            }
                      
        }

        private bool BadLocaion(Location safeloc)
        {
            Vector3 safepos = m_map.LocationToVector(safeloc);
            foreach (Target targ in m_predatorfinder.Targets)
            {
                if (Vector3.Distance(targ.Position, safepos) < ModelConstants.ABSOLOUTEMINIMUMPREDADOTORDISTANCE)
                    return true;
            }
            return false;
        }

        private void SetMovementMode()
        {
            if (m_mode == AgentMode.Wandering)
                m_movementmode = MovementMode.Walking;
            else if (m_mode == AgentMode.Surveying || m_mode == AgentMode.Thinking || m_mode == AgentMode.QuickCheck)
                m_movementmode = MovementMode.Stationery;
            else
                m_movementmode = MovementMode.Running;
           
        }

        private void StartAvoid()
        {
            m_badlocations = new List<Location>();
            SetDestination(GetAvoidDestination());
            m_blockdetected = false;
        }

      
        private Vector3 GetAvoidDestination()
        {
            List<Location> surrounding = m_map.GetSurroundingLocations(m_map.VectortoLocation(m_position), false);
            float furthestdist = 0;
            Vector3 chosen = m_position;
          
            foreach (Location loc in surrounding)
            {
                if (m_map.BlockedDiagonal(m_map.VectortoLocation(m_position), loc))
                    continue;
                if (m_badlocations != null && m_badlocations.Contains(loc))
                    continue;
                Vector3 locpos = m_map.LocationToVector(loc);
                float dist = Vector3.Distance(locpos, m_avoidposition);
                if (dist > furthestdist)
                {
                    chosen = locpos;
                    furthestdist = dist;             
                }
            }
            return chosen;
        }

        public override void HandleCollisionResult(ModelObject CollidingObject, bool initiator)
        {
            if (CollidingObject.Dead)
                m_preyfinder.RemoveTarget(CollidingObject.UniqueID); 
        }
        public override void HandleCollision(ModelObject CollidingObject,bool initiator)
        {
      
            if (initiator && ModelHelper.IsBlockingTypeFor(CollidingObject.GetModelType(),m_type) && CollidingObject.GetModelType() != Target )
            {
                m_blockdetected = true;
               
            }
            if (initiator && CollidingObject.GetModelType() == m_type)
            {
                MouseAgent collidingmouse = CollidingObject as MouseAgent;
                if (collidingmouse != null && collidingmouse.Mode != AgentMode.Avoiding)
                {
                    SetMode(AgentMode.Avoiding);
                    m_avoidposition = CollidingObject.Position;
                }
            }
            m_sensorysystem.RegisterCollision(CollidingObject);
     
        }

        public void DoBaseUpdate(GameTime gt) /*For Test Purposes*/
        {
            base.Update(gt);
        }


        public void UpdateTargetFinders(List<Location> visiblelocs)
        {
            m_preyfinder.Update(visiblelocs);
            m_predatorfinder.Update(visiblelocs);
        }
        public void UpdateSystems()
        {
            m_sensorysystem.Update();
            UpdateTargetFinders(m_sensorysystem.LastUpdated);
            m_map.CurrentLocation = m_map.VectortoLocation(m_position);
            m_health.Update();
        }
        public override void Update(GameTime gt)
        {
            UpdateSystems();
   
            if (m_mode != AgentMode.PlayerControlled)
                DoAutonomousBehaviour(gt);
             
         m_lookat = ModelHelper.CalculateLookAt(m_position,m_rotation.Z);
         m_blockdetected = false;   
         base.Update(gt);
             
        }

        public int Health
        { get { return m_health.Amount; } }
        private void SetLookat()
        {
            Quaternion q = Quaternion.CreateFromAxisAngle(Vector3.UnitZ,m_rotation.Z);

            m_lookat = m_position + Vector3.Transform(Vector3.UnitZ,q);
        }

        private void Rotate(GameTime gt)
        {
            m_previousrotation = 0;
            if (m_timer.Timeup)
                m_timer.Reset(ROTATIONPERIOD);
            float t = (float)(m_timer.TimeLeft.TotalMilliseconds / ROTATIONPERIOD);
            m_rotation.Z = m_previousrotation + (t * MathHelper.TwoPi);
        }
        public void PlayerMove(Movement movement)
        {

            switch (movement)
            {
                case Movement.Forward:
                    Vector3 vec = m_lookat - m_position;
                    m_velocity = CurrentSpeed  * Vector3.Normalize(vec);

                    break;

                case Movement.Backward:
                    Vector3 vec2 = m_lookat - m_position;
                    m_velocity = -CurrentSpeed * Vector3.Normalize(vec2);

                    break;
                case Movement.RotateLeft:
                    Matrix camerarot = Matrix.CreateRotationZ(-m_rotspeed * 1);
                    Vector3 nlookat = Vector3.Transform(Vector3.Normalize((m_lookat - m_position)), camerarot);
                    m_lookat = m_position + nlookat;
                    Vector3 lineofsight = (m_lookat - m_position);
                    m_rotation.Z -= (float)(m_rotspeed);
                    break;
                case Movement.RotateRight:
                    Matrix camerarot2 = Matrix.CreateRotationZ(+m_rotspeed * 1);
                    Vector3 nlookat2 = Vector3.Transform(Vector3.Normalize((m_lookat - m_position)), camerarot2);
                    m_lookat = m_position + nlookat2;

                    m_rotation.Z += (float)(m_rotspeed);

                    break;
                case Movement.Stop:
                    m_velocity = Vector3.Zero;
                    break;
            }
         
            //Keep Rotation between -Pi and +Pi
            if (m_rotation.Z < -MathHelper.Pi)
                m_rotation.Z = MathHelper.TwoPi + m_rotation.Z;
            else if (m_rotation.Z > MathHelper.Pi)
                m_rotation.Z = -MathHelper.TwoPi + m_rotation.Z ;



        }

        private void DoAutonomousBehaviour(GameTime gt)
        {
            switch (m_mode)
            {
                case AgentMode.Wandering: Wander(); break;
                case AgentMode.RunningAway: RunAway(); break;
                case AgentMode.HeadingToTargetDirect: 
                    HeadToTargetDirectly(); break;
                case AgentMode.HeadingToTargetByWayPoint: 
                    HeadToTargetByWayPoint(); break;
                case AgentMode.Surveying: Survey(); break;
                case AgentMode.QuickCheck: QuickCheck(); break;
                case AgentMode.Avoiding: Avoid(); break;
                case AgentMode.Thinking: Think(); break;
                default: Wander(); break;
            } 
            if (m_mode != AgentMode.Surveying  
                && m_mode != AgentMode.Thinking)
                SetDestination(m_currentdestination);                
        }

        private void QuickCheck()
        {              
            if (m_timer.Timeup)
            {
                SetMode(m_previousmode);
                m_checkdone = true;
                return;
            }
            m_velocity = Vector3.Zero;
       
        }

        private void Think()
        {
            if (m_timer.Timeup)
            {
                if (m_previousmode == AgentMode.Thinking)
                    m_previousmode = AgentMode.Wandering;
                SetMode(m_previousmode);

            }
            m_velocity = Vector3.Zero;
       
        }

        private void Avoid()
        {
            if (Arrived())
            {
                SetMode(m_previousmode);
            }
            if (m_blockdetected)
            {
                Location loc = m_map.VectortoLocation(m_currentdestination);
                if (!m_badlocations.Contains(loc))
                    m_badlocations.Add(loc);
                SetDestination(GetAvoidDestination());
            }
                 
        }   

        protected virtual void HeadToTargetByWayPoint()
        {
            if (CheckForPredators())
                return;
            NavigateByWayPoints();
        }

        protected virtual void HeadToTargetDirectly()
        {
            m_preyfound = true;
            if (CheckForPredators())
                return;

            if (Arrived())
            {
                SetMode(AgentMode.Wandering);
                return;
            }
            if (!CheckForPrey())
                SetMode(AgentMode.Wandering);
            if (!m_sensorysystem.CanMoveDirectly(m_ultimatedestination) || m_blockdetected)
            {
                SetMode(AgentMode.HeadingToTargetByWayPoint);
                return;
            }
            else
            {
                m_currentdestination = m_ultimatedestination;
                m_usingwaypoints = false;

            }
        }

        private void Survey()
        {      
            if (m_timer.Timeup)
            {
                SetMode(m_previousmode);
                m_timer.Reset(ModelConstants.LOOKTIME);
                return;
            }
            float t = (float)(m_timer.TimeLeft.TotalMilliseconds / ROTATIONPERIOD);
            m_velocity = Vector3.Zero;
            m_rotation.Z = m_previousrotation + (t * MathHelper.TwoPi);
        }

       

        private void StartSurvey()
        {
            SetMode(AgentMode.Surveying);
            m_timer.Reset(ROTATIONPERIOD);
            m_previousrotation = m_rotation.Z;
             
        }

        protected bool NeedToRestartNavigation()
        {
            if (m_waypoints == null && !SetWayPoints())
                return true;
            if (Arrived() && m_waypoints.Count == 0)
                return true;
            if (m_blockdetected && !SetWayPoints() )            
                return true;
   
            return false;
        }

     
        protected void NavigateByWayPoints()
        {
            if (NeedToRestartNavigation())
            {
                SetMode(AgentMode.Wandering);
                m_usingwaypoints = false;
                return;
            }

            if (Arrived() && m_waypoints.Count > 0 )
                m_currentdestination = m_waypoints.Pop();        
        
        }

      
        private void RunAway()
        {
            if (Safe())
            {
                if (!m_checkdone)
                    SetMode(AgentMode.QuickCheck);
                else
                    SetMode(AgentMode.Wandering);
            }
            else
                m_checkdone = false;

            Vector3 predator = m_predatorfinder.CurrentTarget.Position;
            if (m_predatorfinder.TargetsLocationsChanged)
                SetMode(AgentMode.RunningAway, true);

            NavigateByWayPoints();  
        }

        public float SafeDistance
        {
            get
            {
                float maxdist = Vector3.Distance(m_map.LocationToVector(new Location(1, 1)), m_map.LocationToVector(new Location(m_map.Width - 1, m_map.Height - 1)));
                float dist = ModelConstants.MINIMUMSAFEDISTANCE + ((1 - m_health.AmountPercentage) * ModelConstants.SAFEDISTANCEMODIFIER);
                if (m_mode == AgentMode.RunningAway)
                    dist += ModelConstants.EXTRASAFEDISTANCE;
                return dist * maxdist;
            }
        }
        private bool Safe()
        {         
            Vector3 predator = m_predatorfinder.CurrentTarget.Position;
            if (predator == Vector3.Zero)
                return true;
            float preddist = Vector3.Distance(predator, Position);
            if (preddist < SafeDistance)
                return false;
            else
                return true;
        }

       

        protected bool Arrived()
        {
            Vector2 pos2 = new Vector2(m_position.X, m_position.Y);
            Vector2 cur2 = new Vector2(m_currentdestination.X, m_currentdestination.Y);
            if (Vector2.Distance(pos2, cur2) < ModelConstants.DISTANCEFOREQUAL)
                return true;
            return false;
        }

        protected bool CheckForPredators()
        {
            if (Predator == ModelType.None)
                return false; ;
            if (!Safe())
            {
                SetMode(AgentMode.RunningAway); 
                return true;
            }
            return false;
        }

        protected virtual Vector3 GetTarget()
        {
            m_preyfinder.UseGhost = false;
            return m_preyfinder.CurrentTarget.Position;
        }
        protected bool CheckForPrey()
        {
            Vector3 cheese = GetTarget();
          
            if (cheese != Vector3.Zero && IsSafePosition(cheese))
            {
                SetTarget(cheese);
                return true;
            }
            return false;
        }

        public bool IsSafePosition(Vector3 cheese)
        {
            foreach (Target predator in m_predatorfinder.Targets)
            {
                if (Vector3.Distance(m_position, cheese) < SafeDistance)
                {                   
                    return false;
                }
            }
            return true;
        }
        protected void SetTarget(Vector3 cheese)
        {
            if (!m_usingwaypoints && Vector3.Distance(cheese,m_ultimatedestination) > ModelConstants.DISTANCEFOREQUAL 
                || !m_map.SameLocation(cheese,m_ultimatedestination) || m_mode == AgentMode.Wandering) 
            {
                 m_ultimatedestination = cheese;

                 if (m_sensorysystem.CanMoveDirectly(cheese))
                 {
                     SetMode(AgentMode.HeadingToTargetDirect);
                     m_currentdestination = cheese;
                 }
                 else
                 {
                     SetMode(AgentMode.HeadingToTargetByWayPoint, true);
                 }
            }

        }

       
        protected void Wander()
        {
            if (CheckForPredators())
                return;

            if (CheckForPrey())
                return;

            if (!m_usingwaypoints)        
                SetRandomDestination();
            
            NavigateByWayPoints();
            if (m_timer.Timeup && !m_preyfound)           
                StartSurvey();      
        }

        private bool NextDestFree()
        {
            Location loc = m_map.VectortoLocation(m_currentdestination);
            return m_map.IsFree(loc);
        }
 

        private void MarkPenalties()
        {
            if (m_predatorfinder.Targets.Count == 0)
                return;
            List<Location> badlocs = m_predatorfinder.TargetLocations;
            m_map.ClearPenalties();
            m_map.SetPenalties(badlocs, ModelConstants.PREDATORAVOIDRANGE);
        }

        protected virtual bool SetWayPoints()
        {
            if (!m_findermanager.Available)
            {
                SetMode(AgentMode.Thinking);
                return false;
            }
            
            MarkPenalties();

            m_waypoints  = m_findermanager.FindPath(Position, m_ultimatedestination);
            if (m_waypoints != null && m_waypoints.Count > 0)
            {
                m_initialwaypointcount = m_waypoints.Count;
                Vector3 start = m_waypoints.Pop();
                if (m_waypoints.Count > 0 && !m_blockdetected && m_sensorysystem.CanMoveDirectly(m_waypoints.Peek()) )
                    start = m_waypoints.Pop();
                SetDestination(start);
                m_usingwaypoints = true;
                return true;
            }
            m_usingwaypoints = false;
            return false;
        }


        public Location GetSafeLocation()
        {
            if (m_predatorfinder.Targets.Count == 0 || m_predatorfinder.CurrentTarget.IsNull())
                return GetRandomLocation();
           
     
            List<Location> locations = m_map.GetBlock(Location,ModelConstants.SAFECHECKRANGE);
            Location bestloc = Location.Null;
            float bestscore = 0;
            float MAXSEARCHDISTANCE =  SafeDistance;
         
            foreach (Location loc in locations)
            {
                if (m_map.IsBlocked(loc))
                    continue;
                float locdist = Vector3.Distance(m_map.LocationToVector(loc), m_position);
                float curscore = CalculateSafeScore(loc);
                if (curscore > bestscore)
                {
                    bestloc = loc;
                    bestscore = curscore;
                }
            }
            return bestloc;
        }

     
        public float CalculateSafeScore(Location loc)
        {
            if (m_map.IsBlocked(loc))
                return -1;
     
            Vector3 closestpred = m_predatorfinder.CurrentTarget.Position;
            Vector3 curlocpos = m_map.LocationToVector(loc);
            float curscore = (m_predatorfinder.AverageDistance(curlocpos) * ModelConstants.SCOREAVERAGEDISTMULTIPLIER);
            curscore += m_predatorfinder.Closest(curlocpos) * ModelConstants.SCORECLOSESTDISTMULTIPLIER ;
            if (!m_sensorysystem.CanMoveDirectly(curlocpos, closestpred))
                curscore += ModelConstants.SCOREHIDDENBONUS;
            return curscore;
        }
    

        public void SetDestination(Vector3 Dest)
        {
            m_currentdestination.X = Dest.X;
            m_currentdestination.Y = Dest.Y;
            m_currentdestination.Z = m_position.Z; /*They only move in 2d Never Up*/
            SetDirection();
           
        }      

        public void SetRotation(float angle)
        {
            m_rotation.Z = angle;
        }

        public void SetDirection()
        {
            Vector3 dir = m_currentdestination - m_position;
            SetDirection(dir);
        }

        public void SetDirection(Vector3 vel3)
        {         
            if (vel3 != Vector3.Zero)
            {
                m_velocity = CurrentSpeed * Vector3.Normalize(vel3);
                SetRotationForForward();
            }
            else
                m_velocity = Vector3.Zero;
        }

        protected bool SetRandomDestination()
        {
            m_preyfound = false;
            Location loc = GetRandomLocation();
            m_ultimatedestination = m_map.LocationToVector(loc);
            return SetWayPoints();
        }

        protected virtual Location GetRandomLocation()
        {
            return ModelHelper.GetRandomLocation(m_map);
        }   
    }
  
}
