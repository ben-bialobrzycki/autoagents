﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
   

    public class LocalLocationSlot
    {
      
        List<LocalModelObjectData> m_slotobjects;
        TimeSpan m_timeatlastupdate;

        bool m_marked; /*For Debugging*/
        public bool Marked
        { get {return m_marked; } set {m_marked = value;}}
        int m_penalty = 0;


        public LocalLocationSlot()
        {

            m_slotobjects = new List<LocalModelObjectData>();
        }

        public int Penalty
        { get { return m_penalty; } set { m_penalty = value; } }
        public TimeSpan TimeSinceLastUpdate
        { get { return WallClock.TotalTime - m_timeatlastupdate; } }

        public TimeSpan TimeStamp
        {
            get { return m_timeatlastupdate; }
        }
        public bool Empty
        {  get {  return m_slotobjects.Count == 0; } }


        public List<LocalModelObjectData> SlotObjects
        { get { return m_slotobjects; } }

        public void ClearSlot(MapUpdateMode updatemode)
        {   
           
            if (updatemode == MapUpdateMode.ClearEntitiesOnly)
                m_slotobjects.RemoveAll(ContainEntity);
            else if (updatemode == MapUpdateMode.ClearAll)
                m_slotobjects.Clear();
        }

        private bool ContainEntity(LocalModelObjectData lsobj)
        {
            return ModelHelper.IsEntityModel(lsobj.ModelType);
        }
      
        public bool Contains(ModelType modelType)
        {
            foreach (LocalModelObjectData sobj in m_slotobjects)
            {
                if (sobj.ModelType == modelType )
                    return true;
            }
            return false;
        }

        internal void ResetTime()
        {
            m_timeatlastupdate = WallClock.TotalTime;
        }

        internal LocalModelObjectData Find(ModelType tmodel)
        {
            foreach (LocalModelObjectData lsobj in m_slotobjects)
            {
                if (lsobj.ModelType == tmodel)
                    return lsobj;
            }
            return LocalModelObjectData.NullObject;
        }

        public void SetContent(LocalModelObjectData locationSlotObject)
        {
            if (!m_slotobjects.Contains(locationSlotObject))
                m_slotobjects.Add(locationSlotObject);
        }
    }
  
}
