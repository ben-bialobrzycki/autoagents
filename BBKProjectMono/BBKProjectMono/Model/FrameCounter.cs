﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class FrameCounter
    {
        TimeSpan m_timepassed;
        public void Update(GameTime gt)
        {
            m_timepassed = m_timepassed.Add(gt.ElapsedGameTime);
           
        }
        public TimeSpan TimePassed
        { get { return m_timepassed; } }
    
        public void Reset()
        {
            m_timepassed -= TimeLimit ;
        }
        public TimeSpan TimeLimit
        { get { return TimeSpan.FromSeconds(2); } }
    }
}
