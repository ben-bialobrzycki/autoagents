﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class WorldMap : BaseMap
    {

        RealLocationSlot[,] m_realslotgrid;
        public WorldMap(int width, int height) : base(width, height) { }
        public List<ModelObject> GetAll()
        {
            return GetAll(ModelType.None);
        }

        public override void Rebuild(int width, int height)
        {
            m_realslotgrid = new RealLocationSlot[width, height];
            base.Rebuild(width, height);

        }

        protected override void CreateASlot(Location loc)
        {
            m_realslotgrid[loc.X, loc.Y] = new RealLocationSlot();
        }

        public List<ModelObject> GetAll(ModelType tmodel)
        {
            List<ModelObject> matchobjs = new List<ModelObject>();
            IsType IsType = new IsType(tmodel);
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    if (m_realslotgrid[x, y].ModelObjects != null)
                        matchobjs.AddRange(m_realslotgrid[x, y].ModelObjects.FindAll(IsType.Match));

                }
            }
            return matchobjs;

        }
        public RealLocationSlot GetContent(Location loc)
        {
            if (!ValidLocation(loc))
                throw new Exception(GetErrorMessage(loc));

            return m_realslotgrid[loc.X, loc.Y];
        }
        public List<ModelObject> GetRelevantObjects(Location loc)
        {
            List<ModelObject> relobjs = new List<ModelObject>();
            List<Location> rellocs = GetSurroundingLocations(loc);
            rellocs.Add(loc);
            foreach (Location rloc in rellocs)
            {
                List<ModelObject> locobjs = GetModelObjects(rloc);
                foreach (ModelObject mobj in locobjs)
                {
                    relobjs.Add(mobj);
                }
            }
            return relobjs;
        }

        public void UpdateWorldMap(List<ModelObject> objlist)
        {
            UpdateWorldMap(objlist, MapUpdateMode.ClearAll);
        }
        public void UpdateWorldMap(List<ModelObject> objlist, MapUpdateMode updatemode)
        {
            clearmap(updatemode);
            foreach (ModelObject mobj in objlist)
            {

                Vector3 pos = mobj.GetPosition();
                Location loc = new Location(RoundWidth(pos.X), RoundHeight(pos.Y));
                SetContent(loc, mobj);
            }
        }

        public override bool Contains(Location loc, ModelType mtype)
        {
            foreach (ModelObject mobj in m_realslotgrid[loc.X,loc.Y].ModelObjects)
            if (mobj.GetModelType() == mtype)
                return true;
            return false;
        }

        public bool SetContent(Location loc, ModelObject mobj)
        {
            if (!ValidLocation(loc))
                return false;
            if (!EligibleType(mobj.GetModelType()))
                return false;
            return m_realslotgrid[loc.X, loc.Y].SetContent(mobj);

        }
        public override bool Empty(int x, int y)
        {
            return m_realslotgrid[x,y].ModelObjects.Count == 0;
        }
        public List<ModelObject> GetModelObjects(Location loc)
        {
            return m_realslotgrid[loc.X, loc.Y].ModelObjects;
        }
        private bool ShouldInclude(SelectionMode entitymode, ModelType mtype)
        {
            if (entitymode == SelectionMode.All)
                return true;
            else if (entitymode == SelectionMode.EntitiesOnly && ModelHelper.IsEntityModel(mtype))
                return true;
            else if (entitymode == SelectionMode.NonEntitiesOnly && !ModelHelper.IsEntityModel(mtype))
                return true;
            return false;
        }

        public void UpdateLocalMap(LocalMap localmap, List<Location> visiblelocs)
        {
         
            if (localmap.Width == 0 && localmap.Height == 0)
                localmap.Rebuild(m_width, m_height);
         

            foreach (Location loc in visiblelocs)
            {
                List<ModelObject> mobjs = GetModelObjects(loc);
                localmap.ClearSlot(loc,MapUpdateMode.ClearAll);
                foreach (ModelObject mobj in mobjs)
                {
                    localmap.SetContent(loc, mobj.GetLocationSlotObject());
                }
                localmap.ResetTimeOnSlot(loc);
           
            }
        }

        public LocalMap CreateLocalMap()
        {
            LocalMap localmap = new LocalMap(m_width, m_height);
            UpdateLocalMap(localmap, GetAllLocations());
            return localmap;
            
        }

        public override List<LocalModelObjectData> GetLocationSlotObjs(Location loc)
        {
            return ConvertFromModelObject(m_realslotgrid[loc.X,loc.Y].ModelObjects);
        }
        public static List<LocalModelObjectData> ConvertFromModelObject(List<ModelObject> mobjs)
        {
            List<LocalModelObjectData> lsobjs = new List<LocalModelObjectData>();
            foreach (ModelObject mobj in mobjs)
            {
                lsobjs.Add(mobj.GetLocationSlotObject());
            }
            return lsobjs;
        }

        public override void ClearSlot(Location loc, MapUpdateMode mode)
        {
            m_realslotgrid[loc.X, loc.Y].Clear();
        }
       
    }
}
