﻿/*ModelEnums*/

public enum ModelType { Wall,RedAgent ,BlueAgent,None,Marker,Floor, Cheese };
public enum AgentMode { HeadingToTargetByWayPoint, Wandering, RunningAway, Surveying, PlayerControlled, HeadingToTargetDirect, Avoiding, Thinking, QuickCheck };

public enum MapUpdateMode { ClearAll, ClearEntitiesOnly,KeepAll,ClearOldEntities}

public enum SelectionMode {EntitiesOnly,NonEntitiesOnly,All};

/*General Enums*/
public enum Movement { RotateUp, RotateDown, RotateLeft, RotateRight, Forward, Backward, Stop};
public enum ShapeType { Sphere, Box ,Plane};
public enum MovementMode { Walking, Running, Stationery }


