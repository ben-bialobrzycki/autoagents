﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace BBKModel
{
    public class Timer
    {

        public bool Started
        { get { return m_timestarted != TimeSpan.Zero; } }
        private TimeSpan m_timelimit;
        TimeSpan m_timestarted;

        public bool Timeup
        { get { return m_timestarted + m_timelimit < WallClock.TotalTime; } }
      
        public Timer()
        {
           
            m_timelimit = TimeSpan.Zero;
            m_timestarted = TimeSpan.Zero;
        }

        public TimeSpan TimeLeft
        { get { return  (m_timestarted + m_timelimit) - WallClock.TotalTime ; } }
        public void Reset(float timeperiod)
        {
           
            Reset(TimeSpan.FromMilliseconds(timeperiod));
        }

        public void Reset(TimeSpan timespan)
        {          
            m_timestarted = WallClock.TotalTime;
            m_timelimit = timespan;
        }
     
       
    }
}
