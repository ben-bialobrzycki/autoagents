﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public struct LocalModelObjectData
    {
        ModelType m_modeltype;
        Guid m_id;
        Vector3 m_position;

        public static LocalModelObjectData NullObject
        {
            get { return new LocalModelObjectData(ModelType.None, Guid.Empty,Vector3.Zero); }
        }
        public LocalModelObjectData(ModelType mtype, Guid id, Vector3 position)
        {
            m_id = id;
            m_modeltype = mtype;
            m_position = position;
        }

        public LocalModelObjectData(ModelType mtype)
        {
            m_modeltype = mtype;
            m_id = new Guid();
            m_position = Vector3.Zero;
        }

        public Vector3 Position
        { get { return m_position; } }
        public ModelType ModelType
        {   get { return m_modeltype; }}

      
        public Guid ModelID
        { get { return m_id;}}

        public override bool Equals(object obj)
        {
            LocalModelObjectData lsobj = (LocalModelObjectData)obj;
            return lsobj.ModelID == this.ModelID;

        }
   

        internal bool IsNull()
        {
           return m_modeltype == ModelType.None;
        }
    }

    public class SlotModelTypeComparer : IEqualityComparer<LocalModelObjectData>
    {
        #region IEqualityComparer<LocationSlotObject> Members

        public bool Equals(LocalModelObjectData x, LocalModelObjectData y)
        {
            return x.ModelType == y.ModelType;
        }

        public int GetHashCode(LocalModelObjectData obj)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

}
