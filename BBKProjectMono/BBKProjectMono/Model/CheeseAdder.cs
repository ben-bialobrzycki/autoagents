﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class CheeseAdder : IUpdateable
    {
        Timer m_cheesetimer;
        
        WholeModel m_wholdemodel;

        public CheeseAdder(WholeModel wmod)
        {
          
            m_cheesetimer = new Timer();
            m_wholdemodel = wmod;
            m_cheesetimer.Reset(ModelConstants.CHEESETIME);

        }

        public void Update(GameTime gt)
        {
            if (m_cheesetimer.Timeup)
            {
                if (m_wholdemodel.Count(ModelType.Cheese) < ModelConstants.CHEESEMAXAMOUNT)
                {
                    Location loc = ModelHelper.GetRandomLocation(m_wholdemodel.WorldMap);
                    ModelObject mobj = ModelObjectFactory.CreateModelObject(m_wholdemodel, loc, ModelType.Cheese);
                    m_wholdemodel.AddModel(mobj);
                }
                m_cheesetimer.Reset(ModelConstants.CHEESETIME);
            }
        }

        #region IUpdateable Members


        public string GetMessage()
        {
           return "";
        }

        #endregion
    }
}
