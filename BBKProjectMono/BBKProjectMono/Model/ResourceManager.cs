﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class ResourceManager
    {
        WholeModel m_wholemodel;
       
        List<IUpdateable> m_managers;
        public ResourceManager(WholeModel wmod)
        {
            m_wholemodel = wmod;
            m_managers = new List<IUpdateable>();
            m_managers.Add(new CheeseAdder(wmod));
        }

        public void Update(GameTime gt)
        {
            foreach (IUpdateable iup in m_managers)
                iup.Update(gt);
        }

        public string GetMessages()
        {
            StringBuilder builder = new StringBuilder();
            foreach (IUpdateable iup in m_managers)
               builder.Append(iup.GetMessage());
            
            return builder.ToString();
        }


    }
}
