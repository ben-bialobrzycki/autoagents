﻿using System;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class ModelObjectFactory
    {
        public static ModelType[] EntityModels = new ModelType[]{ ModelType.BlueAgent, ModelType.RedAgent, ModelType.Cheese };
        public static ModelType[] BlockingTypes = new ModelType[] { ModelType.BlueAgent, ModelType.RedAgent };
        public static ModelObject CreateModelObject(WholeModel wmod, Vector3 position, ModelType mtype)
        {
            position.Z = GetZValue(mtype);
            switch (mtype)
            {
                case ModelType.BlueAgent: return new BlueAgent(wmod, position); 
                case ModelType.RedAgent: return new RedAgent(wmod, position); 
                case ModelType.Wall: return new ModelWall(wmod, position);
                case ModelType.Cheese: return new ModelCheese(wmod, position);
                case ModelType.Floor: return new ModelFloor(wmod, position);



            }
            throw new Exception("Model Type not implememted" + mtype.ToString());
        }

    

        private static float GetZValue(ModelType mtype)
        {
            if (mtype == ModelType.BlueAgent || mtype == ModelType.RedAgent)          
                return ModelConstants.DEFAULTMOUSEZVALUE;        
            else if (mtype == ModelType.Cheese)
                return ModelConstants.DEFAULTCHEESEZVALUE;
            else if (mtype == ModelType.Floor)
                return ModelConstants.DEFAULTFLOORZVALUE;
            return  ModelConstants.DEFAULTZVALUE;
        }

        public static ModelObject CreateModelObject(WholeModel m_wholdemodel, Location loc, ModelType modelType)
        {
            Vector3 pos = m_wholdemodel.WorldMap.LocationToVector(loc);
            return CreateModelObject(m_wholdemodel,pos,modelType);
        }
    }
}
