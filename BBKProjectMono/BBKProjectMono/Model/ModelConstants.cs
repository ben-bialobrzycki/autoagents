﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class ModelConstants
    {
        public const float DISTANCEFOREQUAL = 0.05f;
        public const float MAPTIMETILLZEROPROB = 3000.0f;
        public const float MAPLOWERTHRESHOLD = 0.002f;
        public const float MOUSEIMMUNITYTIME = 1000.0f; //Time After mouse takes damage before can take it again
        public const int REDMOUSEHEALTH = 100;
        public const int BLUEMOUSEHEALTH = 100;
        public const int BLUECOLLISIONDAMAGE = 10;
        public const int CHEESEINCREASE = 20;
        public static TimeSpan  CHEESETIME = TimeSpan.FromSeconds(3);
        public const float PATHFINDERTIMELIMIT = 500.0f;
        public const int CHEESEMAXAMOUNT = 10;

        public const float REDMAXVELOCITYRUNNING = 0.0012f;
        public const float REDMAXVELOCITYWALKING = 0.0009f;
        public const float BLUEMAXVELOCITYRUNNING = 0.0015f;
        public const float BLUEMAXVELOCITYWALKING = 0.0007f;
        public const float PLAYERROTATIONSPEED = 0.03f;

        public const float MAXTHINKTIME = 500f;
      
        public static bool LIMITSIGHT = true;
        public const float MAXSIGHTDISTANCE = 10.0f;
        public static bool RESPAWN = true;


        //Survey constants
        public const float LOOKTIME = 5000;
        public const float ROTATIONPERIOD = 1000.0f;

        //StateTransitionConstants
        public static TimeSpan STATELIMITER = TimeSpan.FromSeconds(0.3);

    
        //public static float AGENTSIGHTANGLE = MathHelper.ToRadians(40.0f);
        public static float AGENTSIGHTANGLE = MathHelper.ToRadians(60.0f);

        //TargetFinding Constants
        public static TimeSpan DEFAULTPREDATORMEMORY = TimeSpan.FromSeconds(3.5);
        public static TimeSpan DEFAULTPREYMEMORY = TimeSpan.FromSeconds(8);
        public static TimeSpan BLUEPREYMEMORY = TimeSpan.FromSeconds(12);
        public static TimeSpan PRESENTTIMESPAN = TimeSpan.FromSeconds(0.3);
        public static TimeSpan SHORTTIME = TimeSpan.FromSeconds(0.5);
        public static float TARGETEQUALDIST = 0.5f;
        public static float PREDATORTHRESHOLDMAPPERCENTAGE = 0.6f;
        public const float ABSOLOUTEMINIMUMPREDADOTORDISTANCE = 5.0f;


        //Navigation Constants
        public const float CANMOVEDIRECTLYCLEARANCE = 3.0f;
        
        
        //Predator avoidance constants
        public const float MINIMUMSAFEDISTANCE = 0.4f;
        public const float SAFEDISTANCEMODIFIER = 0.25f;
        public const float EXTRASAFEDISTANCE = 0.2f;

        //Safe Location Scoring constants
        public const int SCOREAVERAGEDISTMULTIPLIER = 10;
        public const int SCORECLOSESTDISTMULTIPLIER = 50;
        public const float SCOREHIDDENBONUS = 100f;
        public const float SCORENOTCLOSEPREDATOR = 300;
        public const int SAFECHECKRANGE = 8;

        //Running Away Constants
        public static TimeSpan QUICKCHECKTIME = TimeSpan.FromSeconds(0.2);
        public static TimeSpan UNSAFETIME = TimeSpan.FromSeconds(2);

        //Pathfinding related constants
        public static TimeSpan PATHFINDERRESETTIME = TimeSpan.FromSeconds(0.3);
        public static int PATHFINDERMAXENTITYCALLS = 3;
        public static bool PATHFINDERLIMIT = false;
        public const int MAXPATHFINDERCOUNT = 10;

        //penalty constants
        public const int PREDATORAVOIDRANGE = 5;
        public const int MAXTERRAINPENALTY = 200;

        //Z-Values for Model Placement to make them just touching ground
        public const float DEFAULTZVALUE = 0f;
        public const float DEFAULTFLOORZVALUE = 0.5f;
        public const float DEFAULTMOUSEZVALUE = 0.2f;
        public const float DEFAULTCHEESEZVALUE = 0.3f;

        //Collision Sizes
        public const float MOUSERADIUS = 0.35f;
        public const float CHEESESIZE = 0.5f;
    }
}
