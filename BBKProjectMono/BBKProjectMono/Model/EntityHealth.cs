﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class EntityHealth
    {
       
        private int m_currenthealth;
        Timer m_immunitytimer;
        ModelEntity m_entity;
        bool m_immune;
        public EntityHealth(ModelEntity entity)
        {
            m_entity = entity;
            m_immune = false;
            m_immunitytimer = new Timer();           

        }

        public int Amount
        { get { return m_currenthealth; } }

        public float AmountPercentage
        { get { return (((float)(m_currenthealth)) / (float)MaxHealth); } }
        public bool Immune
        { get { return m_immune; } }
        public void Update()
        {
            if (m_immune)
            {
                if (m_immunitytimer.Timeup)
                    m_immune = false;
            }
        }

        public bool TakeDamage(int damage)
        {
            if (m_immune)
                return false;
            m_currenthealth -= damage;
            m_immunitytimer.Reset(ModelConstants.MOUSEIMMUNITYTIME);
            m_immune = true;
            return true;
        }
        public void SetMaxHealth()
        {
            m_currenthealth = MaxHealth;
        }

        private int MaxHealth
        {
            get
            {
                if (m_entity.GetModelType() == ModelType.RedAgent)
                    return ModelConstants.REDMOUSEHEALTH;
                else if (m_entity.GetModelType() == ModelType.BlueAgent)
                    return ModelConstants.BLUEMOUSEHEALTH;
                throw new Exception("Max Health not defined for" + m_entity.GetModelType().ToString()); 
            }
        }
        internal void Add(int p)
        {
            m_currenthealth += p;
            if (m_currenthealth > MaxHealth)
                SetMaxHealth();
        }
    }
}
