﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BBKModel
{
    public class ModelWall : ModelObject   
    {

        private const float WALLSIZE = 1.0f;

        public ModelWall(IWholeModel wmod, Vector3 pos): base(wmod, pos,ShapeType.Box)
        {
            m_type = ModelType.Wall;
            SetSize(WALLSIZE);
        }

      

        public Vector3 GetSize()
        {
            Vector3 size = new Vector3(1, 1, 1);
            return size;
        }

      

     
    }
}
