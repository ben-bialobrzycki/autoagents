﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    interface IUpdateable
    {
       

        void Update(GameTime gt);

        string GetMessage();
    }
}
