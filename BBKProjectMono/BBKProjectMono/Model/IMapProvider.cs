﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public interface IMapProvider
    {
        LocalMap LocalMap
        {
            get;
        }
        Vector3 Position
        {
            get;
        }

        float PlaneRotation
        {
            get;
        }

        float Radius
        {
            get;
        }

        ModelType Target
        {
            get;
        }

        ModelType Predator
        {
            get;
        }
    }
}
