﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class BlueAgent : MouseAgent
    { 
        public BlueAgent(IWholeModel wmod, Vector3 pos) : base(wmod, pos) 
        {
         
            m_type = ModelType.BlueAgent;
            m_maxspeedrunning = ModelConstants.BLUEMAXVELOCITYRUNNING;
            m_maxspeedwalking = ModelConstants.BLUEMAXVELOCITYWALKING;
            m_health.SetMaxHealth();
            m_preyfinder.UseGhost = false;
             
        }
    
        public override ModelType Target
        {   get   { return ModelType.Cheese;} }

        public override ModelType Predator
        {   get {   return ModelType.RedAgent;  }}

        public override TimeSpan PreyMemory
        {   get {   return ModelConstants.BLUEPREYMEMORY;}  }

        protected override Vector3 GetTarget()
        {
          
            foreach (Target mycheese in m_preyfinder.ClosestTargets)
            {
                if (IsSafePosition(mycheese.Position))
                    return mycheese.Position;
            }
        
            return Vector3.Zero;
        }
             
        public override void HandleCollision(ModelObject CollidingObject, bool initiator)
        {
            base.HandleCollision(CollidingObject,initiator );
            if (CollidingObject.GetModelType() == Predator)
            {
                m_health.TakeDamage(ModelConstants.BLUECOLLISIONDAMAGE);
                SetMode(AgentMode.Avoiding);
            }
            else if (CollidingObject.GetModelType() == ModelType.Cheese)
                m_health.Add(ModelConstants.CHEESEINCREASE);
        }
    }
}
