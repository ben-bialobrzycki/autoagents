﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBKModel
{
    public interface IWholeModel
    {
        int WorldWidth
        { get; }
        int WorldHeight
        { get; }
    }
}
