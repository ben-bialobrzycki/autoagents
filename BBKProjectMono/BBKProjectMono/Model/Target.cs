﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public struct Target
    {
        Vector3 m_lastseenposition ;
        TimeSpan m_totaltimeatlastseen ;
        Guid m_uniqueid ;

        public Target(Vector3 pos, Guid id, TimeSpan time)
        {
            m_lastseenposition = pos;
            m_totaltimeatlastseen = time;
            m_uniqueid = id;
            
        }

        public Target(Vector3 pos, Guid id) : this(pos, id, WallClock.TotalTime) { }

        public bool IsNull()
        {
            return m_lastseenposition == Vector3.Zero;
        }
        public static Target NullTarget
        {
            get { return new Target(Vector3.Zero,Guid.Empty);}
        }
        public Vector3 Position
        { get { return m_lastseenposition; } set { m_lastseenposition = value; } }
        public Guid ID
        { get { return m_uniqueid; } set { m_uniqueid = value; } }
        public TimeSpan TimeAtLastSeen
        { get { return m_totaltimeatlastseen; } set { m_totaltimeatlastseen = value; } }

        public override bool Equals(object obj)
        {
            Target targ = (Target)obj;
            return this.ID == targ.ID;
        }
        public static bool operator ==(Target target1, Target target2)
        {
            return target1.ID == target2.ID;
        }
        public static bool operator !=(Target target1, Target target2)
        {
            return target1.ID != target2.ID;
        }

        internal static List<Vector3> GetPositions(List<Target> predators)
        {
            List<Vector3> positions = new List<Vector3>();
            foreach (Target targ in predators)
            {
                positions.Add(targ.Position);
            }
            return positions;
        }
    }
}
