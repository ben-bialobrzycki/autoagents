﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.IO;

namespace BBKModel
{
    public class DebugHelper
    {
        public static string Print(Location loc)
        {
            string sloc = "(" + loc.X + ", " + loc.Y + ")";
            return sloc;
        }

        public static string Print(Vector3 vec)
        {
            string svec = "(" + vec.X + ", " + vec.Y + ", " + vec.Z + ")";
            return svec;
        }

        public static string PrintString(LocalMap map)
        {
            return Print(map, false);
        }
        public static string Print(LocalMap map)
        {
            return Print(map, true);
        }

        public static string Print(LocalMap map, bool writeconsole)
        {
           return  Print(map, writeconsole, false);
        }
          public static string Print(LocalMap map, bool writeconsole, bool keepformatting)
        {
          
            StringBuilder sbuilder = new StringBuilder();
            StringBuilder sbuilderclean = new StringBuilder(); 
            for (int j = 0; j < map.Height; j++)
            {
                for (int i = 0; i < map.Width; i++)
                {
                    string s = "";
                    string s2 = "";
                    Location curloc = new Location(i, j);
                  
                    if (map.Marked(curloc))
                    {
                        s = "X";
                        s2 = "X";
                    }
                    else if (map.Empty(curloc.X,curloc.Y))
                    {
                        s = " ";
                        s2 = "0";
                    }
                    else
                    {
                       
                        foreach (LocalModelObjectData lsobj  in map.GetLocationSlotObjs(curloc))
                        {
                            s += ModelHelper.ModelTypeToChar(lsobj.ModelType);
                            s2 += ModelHelper.ModelTypeToChar(lsobj.ModelType);
                        }
                    }
                    sbuilder.Append(s + ", ");
                    sbuilderclean.Append(s2);

                }
               
                sbuilder.Append("\n");

            }
            if (writeconsole)
            {
                Console.WriteLine("Printing World");
                Console.WriteLine(sbuilder.ToString());
            }
            if (keepformatting)
                return sbuilder.ToString();
            return sbuilderclean.ToString();
        }

          public static void Print(int width, int height, int[,] intmap)
          {
              Print(width, height, intmap, 4);
          }
        public static void Print(int width, int height, int[,] intmap,int padleft)
        {
            Console.WriteLine("Printing intmap");
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    Console.Write(intmap[j, i].ToString().PadLeft(padleft,'0') + ", ");
                }
                Console.Write("\n");
            }
        }

     
        public static void PrintTerrainPenalties(LocalMap m_map)
        {
            Console.WriteLine("Printing Terrain Penalties");
            for (int i = 0; i < m_map.Height; i++)
            {
                for (int j = 0; j < m_map.Width; j++)
                {
                    Console.Write(m_map.GetTerrainPenalty(new Location(j,i)).ToString().PadLeft(4,'0') + ", ");
                }
                Console.Write("\n");
            }
        }

        public static void AppendToTempFile(LocalMap map,string start,string goal,bool keepformatting)
        {
            string path = @"C:\Temp\BBKProjectOut.txt";
            StreamWriter writer = new StreamWriter(path,true);
            writer.WriteLine("");
            writer.WriteLine("Start {0} Goal {1}", start, goal);
            string s = DebugHelper.Print(map, false,keepformatting);
            writer.Write(s);
            writer.Close();
        }
        public static void WriteToTempFile(string s)
        {
            string path = @"C:\Temp\BBKProjectOut.txt";
            StreamWriter writer = new StreamWriter(path);
            writer.Write(s);
            writer.Close();
        }
    }
}
