﻿using Microsoft.Xna.Framework;

namespace BBKModel
{
    public interface IBBKScenery : IBBKDrawable
    {
      
        Vector3 GetSize();
        BoundingBox GetBoundingBox();
    }
}
