﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBKModel
{
    public class IsType
    {
        ModelType m_type;
        public IsType(ModelType mtype)
        {
            m_type = mtype;
        }
        public Predicate<ModelObject> Match
        {
            get { return IsMatch; }
        }

        public bool IsMatch(ModelObject mobj)
        {
            if (m_type == ModelType.None)
                return true;
            return m_type == mobj.GetModelType();

        }
    }
}
