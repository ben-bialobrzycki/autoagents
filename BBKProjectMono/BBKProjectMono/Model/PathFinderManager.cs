﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class PathFinderManager
    {
        PathFinder m_pathfinder;
        Timer m_timer;
        int m_count;
      

        public PathFinderManager(LocalMap map)
        {
            m_pathfinder = new PathFinder(map);
            m_timer = new Timer();
           
        }

        public void Update(GameTime gt)
        {
         
            if (m_timer.Timeup )
            {
                m_count = 0;
                m_timer.Reset(ModelConstants.PATHFINDERRESETTIME);
            }

        }

        static bool m_uselimiting = true;
        public static bool LimitingOn
        {
            get { return m_uselimiting; }
            set { m_uselimiting = value; }
        }
        public bool Available
        {
            get 
            {
                if (m_uselimiting)
                    return (m_count < ModelConstants.PATHFINDERMAXENTITYCALLS );
                else
                    return true;
            }
        }
         
        public Stack<Vector3> FindPath(Vector3 startvec, Vector3 endvec)
        {
            m_count++;
            if (m_count >= ModelConstants.PATHFINDERMAXENTITYCALLS && m_uselimiting)
                return null;
            return m_pathfinder.FindShortPath(startvec, endvec);
         

        }
    }
}
