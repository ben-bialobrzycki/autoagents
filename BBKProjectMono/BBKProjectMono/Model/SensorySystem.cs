﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    /*This class act as an interface bettwen ModelEntities and the rest of the simulation*/
    public class SensorySystem
    {
        protected static WorldMap m_worldmap;
        private static List<ModelObject> m_modelobjects;

        private List<Location> m_visiblelocs;
        private List<ModelObject> m_possibleblocks;
        private float COLLISIONCHECKANGLE = MathHelper.ToRadians(30);
  
        private IMapProvider m_provider;

        public List<Location> LastUpdated
        { get { return m_visiblelocs; } }
        public SensorySystem(IMapProvider provider)
        {
            m_provider = provider;

            m_visiblelocs = new List<Location>();
            m_possibleblocks = new List<ModelObject>();
        }


        public static void SetWorldMap(WorldMap worldmap)
        {
            m_worldmap = worldmap;

        }

        public static void SetObjectList(List<ModelObject> modelobjects)
        {
            m_modelobjects = modelobjects;
        }

        private void AddSurrounding(List<Location> visiblelocs)
        {
            Location currentloc = m_worldmap.VectortoLocation(m_provider.Position);
            visiblelocs.AddRange(m_worldmap.GetSurroundingLocations(currentloc));
            visiblelocs.Add(currentloc);
        }
        public void UpdateLocalMap()
        {
            GetAllVisible();
            AddSurrounding(m_visiblelocs);
            m_worldmap.UpdateLocalMap(m_provider.LocalMap, m_visiblelocs);

        }

        private void GetAllVisible()
        {
            ClearLists();
            for (int y = 0; y < m_worldmap.Height; y++)
            {
                for (int x = 0; x < m_worldmap.Width; x++)
                {
                    Location checkloc = new Location(x, y);
                    BuildLists(checkloc);
                }
            }
            m_visiblelocs.RemoveAll(LineOfSightBlocked);
          
        }

        private bool LineOfSightBlocked(Location loc)
        {
             Vector3 locpos = m_worldmap.LocationToVector(loc);
             if (RayCollisions(m_provider.Position, locpos, m_possibleblocks))
                 return true;
             return false;
        }

        private void ClearLists()
        {
            m_visiblelocs.Clear();
            m_possibleblocks.Clear();
        }

        private void BuildLists(Location loctosee)
        {
            Vector3 startposition = m_provider.Position;
            Vector3 positiontosee = m_worldmap.LocationToVector(loctosee);

            if (ModelConstants.LIMITSIGHT)
            {
                if (Vector3.Distance(positiontosee, startposition) 
                    > ModelConstants.MAXSIGHTDISTANCE)
                    return;
            }

            float angledifference = 
                ModelHelper.AngleDifference(startposition,positiontosee,
                    m_provider.PlaneRotation);

            if (angledifference < 
                ModelConstants.AGENTSIGHTANGLE + COLLISIONCHECKANGLE)
            {
               List<ModelObject> objs = 
                   m_worldmap.GetContent(loctosee).ModelObjects;
                if (objs != null && objs.Count > 0)
                    m_possibleblocks.AddRange(objs);
                if (angledifference < ModelConstants.AGENTSIGHTANGLE)
                    m_visiblelocs.Add(loctosee);
            }         
        }

     

        public bool RayCollisions(Vector3 start, Vector3 positiontosee, List<ModelObject> objectlist)
        {          
            float dist = Vector3.Distance(start, positiontosee);
            Vector3 lineofSight = positiontosee - start;
            Ray ray = new Ray(start, lineofSight);
            foreach (ModelObject mobj in objectlist)
            {
                if (mobj.GetModelType() != ModelType.Wall)
                    continue;
                if (Vector3.Distance(mobj.Position, start) < dist 
                    && mobj.GetBoundingBox().Intersects(ray) != null)
                    return true;
            }
            return false;         
        }

         public bool RayCollisions(Vector3 start, Vector3 positiontosee)
         {
             return RayCollisions(start, positiontosee, m_modelobjects);
         }
  

        internal void Update()
        {         
            UpdateLocalMap();
        }  

        internal void RegisterCollision(ModelObject CollidingObject)
        {
            Location loc = m_provider.LocalMap.VectortoLocation(CollidingObject.Position);
            m_provider.LocalMap.SetContent(loc, CollidingObject.GetLocationSlotObject()); 
        }

        public bool CanMoveDirectly(Vector3 cheese)
        {
            return CanMoveDirectly(cheese, m_provider.Position);
        }
        public bool CanMoveDirectly(Vector3 cheese,Vector3 start)
        {
            if (RayCollisions(start, cheese))
                return false;

            Vector3[] startpoints = ModelHelper.GetRadialPoints(start, m_provider.Radius + m_provider.Radius / ModelConstants.CANMOVEDIRECTLYCLEARANCE );
            Vector3[] endpoints = ModelHelper.GetRadialPoints(cheese, m_provider.Radius + m_provider.Radius / ModelConstants.CANMOVEDIRECTLYCLEARANCE); 
         
            for (int i = 0; i < startpoints.Length; i++)
            {
                if (RayCollisions(startpoints[i],endpoints[i]))
                    return false;
            }
            return true;

        }



        internal bool AllowedPosition(Vector3 nextposition)
        {
            return m_worldmap.ValidLocation(m_worldmap.VectortoLocation(nextposition));
         
        }

        private bool CollisionOccured(ModelObject mobj, BoundingSphere boundingsphere)
        {
            if (
                (mobj.Shape == ShapeType.Sphere
                      && mobj.GetBoundingSphere().Intersects(boundingsphere))
                ||
                (mobj.Shape == ShapeType.Box &&
                      mobj.GetBoundingBox().Intersects(boundingsphere))
                )
                return true;
            
            return false;
        }

        internal bool AllowedPosition(ModelObject sourcemobj, 
            Vector3 nextposition, BoundingSphere boundingsphere)
        {
            if (!AllowedPosition(nextposition))
                return false;
            Location sourceloc = 
                m_worldmap.VectortoLocation(nextposition);

            List<ModelObject> relobjs = 
                m_worldmap.GetRelevantObjects(sourceloc);
            foreach (ModelObject mobj in relobjs)
            {
                if (mobj != sourcemobj && 
                    CollisionOccured(mobj,boundingsphere))
                {             
                    sourcemobj.HandleCollision(mobj, true);
                    mobj.HandleCollision(sourcemobj, false);
                    sourcemobj.HandleCollisionResult(mobj, true);
                    mobj.HandleCollisionResult(sourcemobj,false);
                    if (ModelHelper.IsBlockingTypeFor
                        (mobj.GetModelType(), sourcemobj.GetModelType()))
                        return false;               
                }
            }
            return true;
        }
    }
}
