﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class RedAgent : MouseAgent
    {
        
      
        public RedAgent(IWholeModel wmod, Vector3 pos) : base(wmod, pos) 
        {
            m_type = ModelType.RedAgent;
            m_maxspeedrunning = ModelConstants.REDMAXVELOCITYRUNNING;
            m_maxspeedwalking = ModelConstants.REDMAXVELOCITYWALKING; 
            m_health.SetMaxHealth();
         
        }

     

        protected override Vector3 GetTarget()
        {
         
           
            return m_preyfinder.CurrentTarget.Position;

        }
       
        public override ModelType Target
        {  get  {   return ModelType.BlueAgent; }}

        protected override void HeadToTargetByWayPoint()
        {
            CheckForPrey();
            base.HeadToTargetByWayPoint();
        }
    }
}
