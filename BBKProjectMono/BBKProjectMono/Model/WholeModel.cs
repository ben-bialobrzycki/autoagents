using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace BBKModel
{
    public class WholeModel : IWholeModel
    {
                 
        protected int m_worldwidth = 50;
        protected int m_worldheight = 50;
        protected WorldMap m_worldmap;
 
    
        protected List<ModelObject> m_modelobjects;
        protected bool m_paused;
        public FrameCounter FrameCounter;

        public ResourceManager  ResourceManager;
        

        protected ModelEntity m_selectedobject;
        public ModelEntity SelectedObject
        { get { return m_selectedobject; } }

      
        public List<ModelObject> ModelObjects
        { get {  return m_modelobjects; } }

     
        public WorldMap WorldMap
        {
            get { return m_worldmap; }
        }
      
     
        public int WorldWidth
        { get { return m_worldwidth; } }

       
        public int WorldHeight
        { get { return m_worldheight; } }

        public bool Paused
        { get { return m_paused; } set { m_paused = value; } }

       
        public WholeModel()
        {
          
            m_modelobjects = new List<ModelObject>();
     
            BuildWorld();
            m_paused = false;
            FrameCounter = new FrameCounter();
            ResourceManager  = new ResourceManager(this);
        }

        public WholeModel(PredefinedMap pmap):this()
        {
            Build(pmap.CharMap, pmap.Width, pmap.Height);
        }
      
        public void AddObject(ModelObject mobj)
        {
            m_modelobjects.Add(mobj);
        }
        

        private void BuildWorld()
        {
           
            PredefinedMap map = PredefinedMap.GetPredefinedMap(PredefinedMap.DefaultMap);
            Build(map.CharMap,map.Width, map.Height);
        }   

        public void Update(GameTime gameTime)
        {
            FrameCounter.Update(gameTime);

            if (m_paused)
                return;
            WallClock.Update(gameTime);
            foreach (ModelObject mobj in m_modelobjects)
            {
                mobj.Update(gameTime);
            }
            
            m_worldmap.UpdateWorldMap(m_modelobjects);
          
            if (ModelConstants.RESPAWN)
                Respawn();
            ClearDead();
     
            ResourceManager.Update(gameTime);         
        }

       
        public int Count(ModelType mtype)
        {
            IsType IsType = new IsType(mtype);
            List<ModelObject> mobjs = m_modelobjects.FindAll(IsType.Match);
            return mobjs.Count;
           
        }

        private void Respawn()
        {
            int count = (m_modelobjects.FindAll(DeadBlue)).Count;
            for (int i = 0; i < count; i++)
            {
                ModelObject mobj = ModelObjectFactory.CreateModelObject(this, ModelHelper.GetRandomLocation(m_worldmap), ModelType.BlueAgent);
                m_modelobjects.Add(mobj);
            }

        }

        private bool DeadBlue(ModelObject mobj)
        {
            if (mobj.GetModelType() == ModelType.BlueAgent && mobj.Dead)
                return true;
            return false;
        }

        private void ClearDead()
        {
            m_modelobjects.RemoveAll(IsDead);
            if (m_selectedobject != null && m_selectedobject.Dead)
                m_selectedobject = null;
           
        }

        private bool IsDead(ModelObject mobj)
        {
            return mobj.Dead;
        }
         
        public void Build(PredefinedMap pmap)
        {
            Build(pmap.CharMap, pmap.Width, pmap.Height);
        }
        public void Build(char[,] charmap ,int width, int height)
        {
            m_worldwidth = width;
            m_worldheight = height;
            m_worldmap = new WorldMap(width, height);        
            m_modelobjects = GetListFromCharArray(charmap, width, height);
         
            ModelObject floor = ModelObjectFactory.CreateModelObject(this,new Vector3(m_worldwidth / 2.0f, m_worldheight / 2.0f,0),ModelType.Floor);
            m_modelobjects.Add(floor);
            m_worldmap.UpdateWorldMap(m_modelobjects);
            SensorySystem.SetObjectList(m_modelobjects);
            SensorySystem.SetWorldMap(m_worldmap);

        }
        public List<ModelObject> GetListFromCharArray(char[,] charmap ,int width, int height)
        {
            List<ModelObject> modelobjects = new List<ModelObject>();
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (charmap[i, j] == ' ')
                        continue;
                    ModelType mtype = ModelHelper.ChartoModelType(charmap[i, j]);
          
                    //Need To add 0.5f to put objects in middle of column or row e.g wall at (0,1) takes up x from 0 to 1.0 y 1.0 to 2.0
                    Vector3 position = m_worldmap.LocationToVector(new Location(j, i));
             
                    ModelObject mobj = ModelObjectFactory.CreateModelObject(this, position, mtype);
                    modelobjects.Add(mobj);
                }

            }
            return modelobjects;
        }

        public ModelObject GetModelForPosition(Vector3 position)
        {
            foreach (ModelObject mobj in m_modelobjects)
            {
                if (Vector3.Distance(mobj.Position, position) < 0.5f)
                    return mobj;
            }
            return null;
        }

        public ModelObject GetModelForLocation(Location location)
        {
            Vector3 position = WorldMap.LocationToVector(location);
            return GetModelForPosition(position);
        }

        public void AddModel(ModelObject mobj)
        {
            m_modelobjects.Add(mobj);
            m_worldmap.UpdateWorldMap(m_modelobjects);
        }
     
        public void SelectNext() 
        {
            bool found = false;
            foreach (ModelObject mobj in m_modelobjects)
            {
                  if (mobj.GetModelType() == ModelType.RedAgent || mobj.GetModelType() == ModelType.BlueAgent)
                  {
                      MouseAgent rmouse = (MouseAgent)mobj;
                      if (m_selectedobject == null) //Just take first one
                      {
                          m_selectedobject = rmouse;
                          return;
                      }

                      if (found) //Take Next one in list
                      {
                           m_selectedobject = rmouse;
                          return;
                      }
                      else if (m_selectedobject == rmouse)
                      {
                          found = true;
                      }
                      
                     
                  }
            }
            m_selectedobject = null;
           
            
        }
    }
}
