﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class ModelFloor : ModelObject
    {
          private const float FLOORWIDTH = 0.01f;

        public ModelFloor(IWholeModel wmod, Vector3 pos): base(wmod, pos,ShapeType.Plane)
        {
            m_type = ModelType.Floor;
            SetSize(new Vector3(wmod.WorldWidth,wmod.WorldHeight,FLOORWIDTH));
        }  

        public Vector3 GetSize()
        {
           
            return m_size;
        }

      
    }
}
