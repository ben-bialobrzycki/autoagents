﻿using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace BBKModel
{
    public static class ModelHelper
    {
        static Random rand = new Random(DateTime.Now.Millisecond);
       
        public static float RotationForMovement(Vector3 vel)
        {
            return RotationForDirection(vel);
          
        }

        public static float RotationForDirection(Vector3 vel)
        {
            float angle = ((float)Math.Atan2(vel.Y, vel.X)) ;//% MathHelper.TwoPi;
            return angle;
        }
        public static bool InSightCone(Vector3 startposition, Vector3 positiontosee, float angle)
        {
            float angleDifference = AngleDifference(startposition,positiontosee,angle);
       
            if (Math.Abs(angleDifference % MathHelper.TwoPi) < ModelConstants.AGENTSIGHTANGLE)
                return true;
            return false;
        }


        public static float AngleDifference(Vector3 startposition, 
            Vector3 positiontosee, float angle)
        {   
            /*Based on discussions on xna creator's club website forum*/
            Vector3 loctoposition = positiontosee - startposition;         
            float axisangle = 
                (float)Math.Atan2(loctoposition.Y, loctoposition.X);       
            float angleDifference = Math.Abs(axisangle - angle);
            if (angleDifference > Math.PI)
            {
                if (axisangle > angle)
                    angleDifference = 
                        Math.Abs(axisangle - (angle + MathHelper.TwoPi));
                else
                    angleDifference = 
                        Math.Abs((axisangle + MathHelper.TwoPi) - angle);
            }
            return angleDifference % MathHelper.TwoPi ;     
        }

        public static bool InSightCone(Vector3 startposition, Vector3 positiontosee, Vector3 sight)
        {
            float aDirection = ModelHelper.RotationForMovement(sight);
            return InSightCone(startposition, positiontosee, aDirection);
        }

        public static Vector2 GetRandomVector2(float max)
        {
            float x = (float)(max * rand.NextDouble());
            float y = (float)(max * rand.NextDouble());
            if (GetRandomBool())
                x = -x;
            if (GetRandomBool())
                y = -y;
            Vector2 vec = new Vector2(x, y);
            return vec;
        }

        public static bool GetRandomBool()
        {
            int x = rand.Next(10);
            return x % 2 == 0;
        }
        public static float GetRandomFloat(float max, float min)
        {
            return min + GetRandomFloat(max - min);
        }

        public static float GetRandomFloat(float max)
        {
            return (float)(max * rand.NextDouble());
        }

        public static Location GetRandomLocation(BaseMap map)
        {
            Location loc = new Location(); ;
            do
            {
                loc.X = rand.Next(map.Width);
                loc.Y = rand.Next(map.Height);

            } while (!map.IsFree(loc));
            return loc;
        }
        static ModelCharPair[] ModelCharMap = 
        { 
            new ModelCharPair('R', ModelType.RedAgent), new ModelCharPair('B', ModelType.BlueAgent), 
            new ModelCharPair('W', ModelType.Wall), new ModelCharPair(' ', ModelType.None), 
            new ModelCharPair('X', ModelType.Marker) ,new ModelCharPair('C',ModelType.Cheese)
        };

        public static char ModelTypeToChar(ModelType mtype)
        {
            foreach (ModelCharPair mpair in ModelCharMap)
            {
                if (mpair.mtype == mtype)
                    return mpair.ch;
            }
            throw new Exception("char not defined for " + mtype.ToString()); 
        }
        public static ModelType ChartoModelType(char ch)
        {
            foreach (ModelCharPair mpair in ModelCharMap)
            {
                if (mpair.ch == ch)
                    return mpair.mtype;
            }
            throw new Exception("Model Type not defined for " + ch); 
        }

        struct ModelCharPair
        {
            public ModelCharPair(char ch, ModelType mty)
            {
                this.ch = ch;
                this.mtype = mty;
            }
            public char ch;
            public ModelType mtype;
        }

        public static bool IsEntityModel(ModelType testmtype)
        {
            foreach (ModelType mtype in ModelObjectFactory.EntityModels)
            {
                if (mtype == testmtype)
                    return true;
            }
            return false;
        }

     

        public static Vector3 CalculateLookAt(Vector3 position, float angle)
        {
            Quaternion q = Quaternion.CreateFromAxisAngle(Vector3.UnitZ, angle);

            Vector3 lookat = position + Vector3.Transform(Vector3.UnitX, q);
            return lookat;
        }

  


        internal static bool IsBlockingTypeFor(ModelType type, ModelType fortype)
        {
            foreach (ModelType btype in BlockingTypesFor(fortype))
            {
                if (btype == type)
                    return true;
            }
            return false;
        }

        public static ModelType[] BlockingTypesFor(ModelType fortype)
        {
          
            switch (fortype)
            {
                case ModelType.BlueAgent : return new ModelType[]{ModelType.BlueAgent,ModelType.RedAgent,ModelType.Wall};
                case ModelType.RedAgent: return new ModelType[] { ModelType.RedAgent, ModelType.BlueAgent ,ModelType.Wall};
            }
            return null;

        }

      
        public static Vector3[] GetRadialPoints(Vector3 position, float Radius)
        {
            Vector3[] points = new Vector3[4];
            points[0] = new Vector3(position.X + Radius, position.Y, position.Z);
            points[1] = new Vector3(position.X - Radius, position.Y, position.Z);
            points[2] = new Vector3(position.X, position.Y + Radius, position.Z);
            points[3] = new Vector3(position.X, position.Y - Radius, position.Z);
            return points;
        }
    }
}
