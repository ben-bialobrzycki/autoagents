﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public abstract class  ModelEntity : ModelObject ,IMapProvider
    {
        protected LocalMap m_map;
        protected SensorySystem m_sensorysystem;
        protected PathFinderManager m_findermanager;
        protected TargetFinder m_preyfinder;
        protected TargetFinder m_predatorfinder;

      

        protected float m_maxspeedwalking;
        protected float m_maxspeedrunning;
        protected MovementMode m_movementmode;

     
        public ModelEntity(IWholeModel wmod,Vector3 pos) : base(wmod, pos, ShapeType.Sphere) 
        {
            m_map = new LocalMap(wmod.WorldWidth, wmod.WorldHeight);
        
            m_sensorysystem = new SensorySystem(this);
            m_findermanager = new PathFinderManager(m_map);
           
            m_preyfinder = new TargetFinder(this,Target,PreyMemory);
            m_predatorfinder = new TargetFinder(this, Predator, PredatorMemory,false);
        }

        public TargetFinder PredatorFinder
        { get { return m_predatorfinder; } }
        public TargetFinder PreyFinder
        { get { return m_preyfinder; } }
        public float CurrentSpeed
        {
            get
            {
                switch (m_movementmode)
                {
                    case MovementMode.Walking: return m_maxspeedwalking; 
                    case MovementMode.Running: return m_maxspeedrunning; 
                    case MovementMode.Stationery: return 0f; 

                }
                return 0;
            }
        }

       public float MaxVelocityWalking
        {
            get
            {
                return m_maxspeedwalking;
            }
        }

       public float MaxVelocityRunning
       {
           get { return m_maxspeedrunning; }
       }
        public LocalMap LocalMap
       { get { return m_map; } }
        public float PlaneRotation
        { get { return m_rotation.Z; } }
        public SensorySystem Sensor
        { get { return m_sensorysystem; } }

    
      
        protected Vector3 m_velocity;
        protected Vector3 m_lookat;
        protected bool m_blockdetected;
        public bool BlockDetected
        { get { return m_blockdetected; } }

        protected Vector3 m_avoidposition;
    
        public Vector3 Velocity
        { get { return m_velocity; } }

        public Location Location
        { get { return m_map.VectortoLocation(m_position); } }

        public Vector3 Lookat
        { get { return m_lookat; } }
       
        protected void SetRotationForForward()
        {
            if (m_velocity.Length() > 0)
                m_rotation.Z = ModelHelper.RotationForMovement(m_velocity);
            
         
        }

        public override void Update(GameTime gt)
        {
            m_findermanager.Update(gt);

            if (m_velocity == Vector3.Zero)
                return;
            Vector3 nextposition = m_position + m_velocity * gt.ElapsedGameTime.Milliseconds;
   
            if (m_sensorysystem.AllowedPosition(this, nextposition, GetBoundingSphere(nextposition)))
            {
                m_position = nextposition;
                m_lookat += m_velocity * gt.ElapsedGameTime.Milliseconds;
            }
     
            m_map.CurrentLocation = m_map.VectortoLocation(m_position);          
        }

        public virtual ModelType Target
        { get { return ModelType.None; } }
        public virtual ModelType Predator
        { get { return ModelType.None; } }
        public virtual TimeSpan PredatorMemory
        { get { return ModelConstants.DEFAULTPREDATORMEMORY; } }
        public virtual TimeSpan PreyMemory
        { get {return ModelConstants.DEFAULTPREYMEMORY; } }

         ModelType IMapProvider.Target
        { get { return Target;}}
         ModelType IMapProvider.Predator
        { get { return Predator; } }
      
    }
}
