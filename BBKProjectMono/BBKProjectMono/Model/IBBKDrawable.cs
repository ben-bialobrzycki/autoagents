﻿using Microsoft.Xna.Framework;

namespace BBKModel
{

    public interface IBBKDrawable
    {

        Vector3 GetPosition();
        Vector3 GetRotation();
        ModelType GetModelType();
        

    }
}
