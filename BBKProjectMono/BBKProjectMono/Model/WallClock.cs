﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    /*Allows use of time stamping and timers without need to pass around a GameTime*/
    public static class WallClock
    {
        private  static TimeSpan m_totalgametime;
        public static TimeSpan TotalTime
        { get { return m_totalgametime; } }
        public static void Update(GameTime gt)
        {
            m_totalgametime += gt.ElapsedGameTime;
        }
    }
}
