﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{
    public class ModelCheese : ModelObject 
    {
        public ModelCheese(IWholeModel wmod, Vector3 pos) : base(wmod,pos,ShapeType.Box) { m_type = ModelType.Cheese; SetSize(ModelConstants.CHEESESIZE); }
        public override void HandleCollision(ModelObject CollidingObject,bool initiator)
        {
           if (CollidingObject.GetModelType() == ModelType.BlueAgent)
                m_dead = true;
        }
    }
}
