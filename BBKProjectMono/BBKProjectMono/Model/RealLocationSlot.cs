﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBKModel
{
    public class RealLocationSlot
    {
        private List<ModelObject> m_modelobjects;

        public RealLocationSlot()
        {
            m_modelobjects = new List<ModelObject>();
        }
        public List<ModelObject> ModelObjects
        {
            get
            {   return m_modelobjects; }
        }

        internal bool SetContent(ModelObject mobj)
        {
            if (m_modelobjects.Contains(mobj))
                return false;
            m_modelobjects.Add(mobj);
            return true;
        }

        internal void Clear()
        {
            m_modelobjects.Clear();
        }
    }
}
