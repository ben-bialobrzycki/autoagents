﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace BBKModel
{

    /*This clas implements the A* algorithm for path finding*/
    public class PathFinder
    {      
        private LocalMap m_map;  
        private Location m_goal;
        private Location m_start;
   
        private bool m_usepenalties = false;
   
   
        public static bool DEBUG = false;
        public int[,] m_fvaluemap; //For debugging
   

        List<Node> m_openlist;
        List<Node> m_closedlist;
        

        public PathFinder(LocalMap map)
        {
            m_map = map;
           
            m_openlist = new List<Node>();
            m_closedlist = new List<Node>();
            Reset();
           
        }

        private void Reset()
        {
            m_openlist.Clear();
            m_closedlist.Clear();
        }
        
    

        public Stack<Vector3> FindShortPath(Vector3 startvec, Vector3 endvec)
        {
            return FindShortPath(startvec, endvec, true);
        }
        public Stack<Vector3> FindShortPath(Vector3 startvec, Vector3 endvec,bool useterrainpenalties)
        {
         
            Reset();
            if (DEBUG)
            {
                m_fvaluemap = new int[m_map.Width, m_map.Height];
            }

            m_start = m_map.VectortoLocation(startvec);
            m_goal = m_map.VectortoLocation(endvec);

       
            m_usepenalties = useterrainpenalties;
            m_map.CurrentLocation = m_start;
            m_map.TargetLocation = m_goal;
            if (m_map.IsBlocked(m_goal))
                return null;
           
         
          
            Node startnode = new Node(m_start.X, m_start.Y);
            m_openlist.Add(startnode);
            bool looking = true;
            Node currentnode = null;
            while (looking)
            {
                currentnode = lowestf();
                if (DEBUG)
                {
                    display_open();
                    display_closed();
                }
                if (currentnode == null)
                    break;
                move_to_closed(currentnode);
                if (currentnode.Location.X == m_goal.X && currentnode.Location.Y == m_goal.Y)
                    looking = false;
                else
                    add_surrounding(currentnode);
            }
    
            Stack<Vector3> vecstack = build_vec_stack(currentnode);
            return vecstack;

        }

       
        private Stack<Location> build_stack(Node node)
        {
            Stack<Location> stack = new Stack<Location>();
            while (node != null)
            {
                Location co = new Location(node.Location.X, node.Location.Y);
                stack.Push(co);
                node = node.parent;
            }
            return stack;
        }

        private Stack<Vector3> build_vec_stack(Node node)
        {
            Stack<Vector3> stack = new Stack<Vector3>();
            while (node != null)
            {
                int x = stack.Count;
                Location co = new Location(node.Location.X, node.Location.Y);
                stack.Push(m_map.LocationToVector(co));
                node = node.parent;
            }
            return stack;
        }
        private void move_to_closed(Node currentnode)
        {
            m_closedlist.Add(currentnode);
            m_openlist.Remove(currentnode);
        }
        private Node lowestf()
        {
            m_openlist.Sort(delegate(Node n1, Node n2) { return n1.f.CompareTo(n2.f); });
            if (m_openlist.Count > 0)
                return m_openlist[0];
            else
                return null;
    
        }
        private void add_surrounding(Node current)
        {
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (!(i == 0 && j == 0))
                    {
                        int nx = current.Location.X + i;
                        int ny = current.Location.Y + j;
                        if (!m_map.IsBlocked(nx, ny) && !in_closed(nx, ny))
                        {
                            if (!m_map.BlockedDiagonal(current.Location, new Location(nx, ny)))
                            {
                                Node node = new Node(nx, ny);
                                node.parent = current;
                                set_values(node);
                                Node check = in_open(nx, ny);
                          
                                if (check == null)
                                {
                                    m_openlist.Add(node);
                                }
                                else if (node.g < check.g)
                                {
                                    check.parent = current;
                                    set_values(check);
                                }
                            }

                        }
                    }

                }

            }
        }

     

 
        private bool in_closed(int x, int y)
        {
            foreach (Node node in m_closedlist)
            {
                if (node.Location.X == x && node.Location.Y == y)
                    return true;
            }
            return false;
        }
        private Node in_open(int x, int y)
        {
            foreach (Node node in m_openlist)
            {
                if (node.Location.X == x && node.Location.Y == y)
                    return node;
            }
            return null;
        }
        private void set_values(Node node)
        {
            int xdist = node.Location.X - node.parent.Location.X;
            int ydist = node.Location.Y - node.parent.Location.Y;

            if (((xdist * xdist) + (ydist * ydist)) == 1)
                node.g = node.parent.g + 10;
            else
                node.g = node.parent.g + 14;
            if (m_usepenalties)
                node.penalty = m_map.GetTerrainPenalty(node.Location);
            node.h = CalculateHValue(node);
            node.f = node.g + node.h + node.penalty;
            if (DEBUG)
                MarkOnDebugMap(node.Location, node.f);
        }

     

        private int CalculateHValue(Node node)
        {
            return (10 * (positive(node.Location.X - m_goal.X) + positive(node.Location.Y - m_goal.Y))); //Original Heuristic 
       
           
        }

        private int GoalDistance(Node node)
        {
            return (10 * (positive(node.Location.X - m_goal.X) + positive(node.Location.Y - m_goal.Y))); //Original Heuristic 
        }

    
        private int positive(int x)
        {
            if (x > 0)
                return x;
            else
                return -x;
        }

  
        public class Node
        {
            public Node parent = null;
            public int f = 0; // total value
            public int g = 0;  //distance to start
            public int h = 0;  // distance to destination
            public int penalty = 0; //Terrain penalty modifier to discourage more dangerous cells
            public Location Location;
            public Node(int x, int y)
            {
                Location.X = x;
                Location.Y = y;
            }
        }

        
#region Debugging functions

        private void MarkOnDebugMap(Location location, int p)
        {
            m_fvaluemap[location.X, location.Y] = p;
        }

        private void display_open()
        {
            Console.WriteLine("Open List");
            show_list(m_openlist);

        }

        private void display_closed()
        {
            Console.WriteLine("Closed List");
            show_list(m_closedlist);

        }
        private void show_list(List<Node> list)
        {
            foreach (Node nod in list)
            {
                Console.WriteLine("Location " + "(" + nod.Location.X + "," + nod.Location.Y + ")"
                  + " f g h = " + nod.f + " " + nod.g + " " + nod.h);
            }
        }
#endregion
    }
}
