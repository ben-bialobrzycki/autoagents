﻿using BBKModel;
using Microsoft.Xna.Framework.Input;

namespace BBKAutoAgents.Controller
{
    public class InputHandler
    {
        WholeView m_view;
        WholeModel m_wholemodel;
        KeyboardState m_oldkeyboardstate;
        KeyboardState m_newkeyboardstate;

        private bool m_exit;
        public bool Exit
        { get { return m_exit; } set { m_exit = value; } }
        private bool m_requestrestart;
        public bool RequestRestart
        { get { return m_requestrestart; } }
 
        private MouseController m_mousecontroller;

        public InputHandler(WholeView theview, WholeModel wmod)
        {
            m_view = theview;
            m_wholemodel = wmod;
 
            m_exit = false;
            m_mousecontroller = new MouseController(this);

        }

        public void ReadInput()
        {
            m_oldkeyboardstate = m_newkeyboardstate;
            m_newkeyboardstate = Keyboard.GetState();
        }
        public void HandleInput()
        {
            ReadInput();
            if (BeenPressed(Keys.D))
                m_view.SwitchMode();

            CheckForRestart();
           
        

            if (BeenPressed(Keys.P))
                m_wholemodel.Paused = !(m_wholemodel.Paused);
            if (BeenPressed(Keys.L))
                PathFinderManager.LimitingOn = !PathFinderManager.LimitingOn;
       
            if (BeenPressed(Keys.C))
                SwitchControlState();

            if (BeenPressed(Keys.Tab))
                m_wholemodel.SelectNext();

            if (BeenPressed(Keys.W))
                m_view.ShowPaths = !m_view.ShowPaths;

            DoCameraControl();
            m_mousecontroller.HandleMouseControl();
          
            
          
           

        }

        private void CheckForRestart()
        {
            if (BeenPressed(Keys.F1))
                SetForRestart(1);

            if (BeenPressed(Keys.F2))
                SetForRestart(2);
            if (BeenPressed(Keys.F3))
                SetForRestart(3);

            if (BeenPressed(Keys.F4))
                SetForRestart(4);
            if (BeenPressed(Keys.F5))
                SetForRestart(5);

            if (BeenPressed(Keys.F6))
                SetForRestart(6);
            if (BeenPressed(Keys.F7))
                SetForRestart(7);

            if (BeenPressed(Keys.F8))
                SetForRestart(8);

            if (BeenPressed(Keys.F9))
                SetForRestart(9); 
        }

        private void SetForRestart(int mapnumber)
        {
            PredefinedMap.DefaultMap = mapnumber;
            m_requestrestart = true;
        }
     

        private void DoCameraControl()
        {
            if (BeenPressed(Keys.NumPad5))
                m_view.SetCameraPosition(CameraPosition.CentreHigh);
            if (BeenPressed(Keys.NumPad2))
                m_view.SetCameraPosition(CameraPosition.MouseCamera);
            if (BeenPressed(Keys.NumPad7))
                m_view.SetCameraPosition(CameraPosition.TopLeft);
            if (BeenPressed(Keys.NumPad9))
                m_view.SetCameraPosition(CameraPosition.TopRight);
            if (BeenPressed(Keys.NumPad1))
                m_view.SetCameraPosition(CameraPosition.BottomLeft);
            if (BeenPressed(Keys.NumPad3))
                m_view.SetCameraPosition(CameraPosition.BottomRight);

            if (m_newkeyboardstate.IsKeyDown(Keys.Add))
                m_view.MoveCurrentCamera(Movement.Forward);
            if (m_newkeyboardstate.IsKeyDown(Keys.Subtract))
                m_view.MoveCurrentCamera(Movement.Backward);
     
        }

        private void SwitchControlState()
        {
            MouseAgent rmouse = (MouseAgent)m_wholemodel.SelectedObject;
            m_mousecontroller.SetControl(rmouse);        
        }


        public bool IsKeyDown(Keys key)
        {
            return m_newkeyboardstate.IsKeyDown(key);
        }

        public bool IsKeyUp(Keys key)
        {
            return m_newkeyboardstate.IsKeyUp(key);
        }
        public bool BeenPressed(Keys key)
        {
            if (m_oldkeyboardstate.IsKeyUp(key) && m_newkeyboardstate.IsKeyDown(key))
                return true;
            else
                return false;
        }

        public bool BeenReleased(Keys key)
        {
            if (m_oldkeyboardstate.IsKeyDown(key) && m_newkeyboardstate.IsKeyUp(key))
                return true;
            else
                return false;
        }
    }
}
