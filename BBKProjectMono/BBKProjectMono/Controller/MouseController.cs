﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BBKModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
namespace BBKAutoAgents.Controller
{
    public class MouseController
    {

        MouseAgent m_playermouse;
        InputHandler m_inputhandler;

        public MouseController(InputHandler inputhandler)
        {
            m_inputhandler = inputhandler;           
            
        }


        public void SetControl(MouseAgent mouse)
        {
            if (m_playermouse != null)
                m_playermouse.PlayerControlled = false;

            if (mouse != null && mouse != m_playermouse)
            {
                mouse.PlayerControlled = true;
                m_playermouse = mouse;
            }
            else
                m_playermouse = null;
      
        }

        public void HandleMouseControl()
        {
            if (m_playermouse == null)
                return;
            if (m_inputhandler.IsKeyDown(Keys.Up))
                m_playermouse.PlayerMove(Movement.Forward);
            else if (m_inputhandler.IsKeyDown(Keys.Down))
                m_playermouse.PlayerMove(Movement.Backward);
            else
                m_playermouse.PlayerMove(Movement.Stop);

            if (m_inputhandler.IsKeyDown(Keys.Left))
                m_playermouse.PlayerMove(Movement.RotateLeft);
            if (m_inputhandler.IsKeyDown(Keys.Right))
                m_playermouse.PlayerMove(Movement.RotateRight);
        }


    }
}
