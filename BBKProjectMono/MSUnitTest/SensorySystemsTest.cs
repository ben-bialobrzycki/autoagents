﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BBKModel;
using Microsoft.Xna.Framework;
using System;

namespace xUnitTests
{
    [TestClass]
    public class SensorySystemsTest
    {
        WholeModel m_mockmodel;
        MouseAgent m_rmouse;
        MockTime m_mocktime;
        MouseAgent m_bmouse;
        MockImap m_imap;
        SensorySystem m_sensor;
        char[,] m_charmap;


       [TestInitialize]
        public void Setup()
        {
         m_charmap = new char[WorldMapTests.WIDTH, WorldMapTests.HEIGHT]
         {
          // 0    1    2    3     4    5     6    7     8     9  
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},//0
           {' ', ' ', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},//1
           {' ', 'W', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},//2
           {'B', 'W', 'B', 'R' , ' ', 'B' , 'W' ,'B' , ' ' , ' '},//3
           {'B', 'W', ' ', ' ' , ' ', ' ' , 'W' ,'W' , ' ' , ' '},//4
           {' ', 'R', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},//5
           {' ', ' ', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},//6
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},//7
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},//8
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},//9
             
         };

            m_mockmodel = new WholeModel();
            m_mocktime = new MockTime();

            Console.WriteLine("Setup Called");
          
         
            m_mockmodel.Build(m_charmap, WorldMapTests.WIDTH, WorldMapTests.HEIGHT);
            SensorySystem.SetWorldMap(m_mockmodel.WorldMap);
            SensorySystem.SetObjectList(m_mockmodel.ModelObjects);
            m_imap = new MockImap(m_mockmodel.WorldMap.CreateLocalMap(), new Vector3(3.5f, 3.5f, 0.5f));
            ModelObject mobj = m_mockmodel.GetModelForPosition(new Vector3(3.5f, 3.5f, 0));
            m_rmouse = (MouseAgent)mobj;
            m_rmouse.SetDirection(new Vector3(1, 0, 0));

            m_bmouse = (MouseAgent)m_mockmodel.GetModelForPosition(new Vector3(7.5f, 3.5f, 0));
           
           
            m_sensor = new SensorySystem(m_rmouse);
        }
        
        [TestMethod]
        public void UpdateLocalMapTest()
        {

            LocalMap localmap = m_rmouse.LocalMap;
            DebugHelper.Print(localmap);
           
           
            m_sensor.UpdateLocalMap();
            string s = DebugHelper.Print(localmap);
          //  DebugHelper.WriteToTempFile(s);
            string CORRECT = "0000000000000000W000000000W00000BR0BW000000000W000000000W0000000000000000000000000000000000000000000";
             
            Assert.AreEqual(s, CORRECT);
        }

      
        [TestMethod]
        public void CheckRayCollsionTest()
        {
           
            float dist = 4;
            m_sensor = m_rmouse.Sensor;
            Ray ray = new Ray(m_rmouse.Position, Vector3.UnitX);
            Vector3 end1 = m_rmouse.Position;
            end1.X += 4.0f;
            bool rsult = m_sensor.RayCollisions(m_rmouse.Position, end1);
            Assert.IsTrue(rsult, "A1");
            Vector3 end2 = m_rmouse.Position;
            end1.X += 2.0f;
            rsult = m_sensor.RayCollisions(m_rmouse.Position, end2);
            Assert.IsFalse(rsult, "A2");
            Ray ray2 = new Ray(m_rmouse.Position, new Vector3(4, 1, 0));
       
        }

        [TestMethod]
        public void UpdateLocalMapOnCornerTest()
        {
            Location rloc = new Location(1,5);
            MouseAgent redmouse = (MouseAgent)m_mockmodel.GetModelForLocation(rloc);
            Vector3 oldpos = redmouse.Position;
            redmouse.Position = new Vector3(oldpos.X,oldpos.Y - 0.25f,oldpos.Z);
            Vector3 direction = new Vector3(-0.5f,-0.25f,0);
            redmouse.SetDirection(direction);
            SensorySystem sensor = new SensorySystem(redmouse);
            sensor.UpdateLocalMap();
            LocalMap map = redmouse.LocalMap;
            DebugHelper.Print(redmouse.LocalMap);
            Location loc = new Location(1,4);
            Assert.IsFalse(map.IsFree(loc));

        }
        [TestMethod]
        public void UpdateLocalMapBasicTest()
        {
            Location rloc = new Location(1, 5);
            MouseAgent redmouse = (MouseAgent)m_mockmodel.GetModelForLocation(rloc);
           
            Vector3 direction = new Vector3(0f, -1f, 0);
            redmouse.SetDirection(direction);
            m_sensor = redmouse.Sensor;
            m_sensor.UpdateLocalMap();
            LocalMap map = redmouse.LocalMap;
            DebugHelper.Print(map);
            Location loc = new Location(1, 4);
            Assert.IsFalse(map.IsFree(loc));
        }

        [TestMethod]
        public void CanMoveDirectlyTest()
        {
            MouseAgent mred = (RedAgent)m_mockmodel.GetModelForLocation(new Location(1, 5));
            SensorySystem system = mred.Sensor;

            UIHelper helper = new UIHelper();
            
            Vector3 okpos = new Vector3(4.0f,5.5f,mred.Position.Z);
            ShowLines(mred.Position, okpos);
            Console.WriteLine(mred.Position);
            Assert.IsTrue(system.CanMoveDirectly(okpos),"A1");
            Vector3 badpos1 = new Vector3(7.0f, 3.0f, mred.Position.Z);
            Assert.IsFalse(system.CanMoveDirectly(badpos1),"A2");
            Vector3 badpos2 = new Vector3(2.0f, 3.0f, mred.Position.Z);
            Assert.IsFalse(system.CanMoveDirectly(badpos2),"A3");
        }

        [TestMethod]
        public void CanMoveDirectlybasicTest()
        {
            SensorySystem system = new SensorySystem(m_imap);
            float z = ModelConstants.DEFAULTMOUSEZVALUE;
            Vector3 start = new Vector3(4.5f, 4.5f, z);
            Vector3 endblocked = new Vector3(8.5f,4.5f,z);
            Vector3 endok = new Vector3(5.5f, 6.5f, z);
            Vector3 endok2 = new Vector3(1.5f, 8.5f, z);
            Vector3 endok3 = new Vector3(6.5f, 9.5f, z);
         
            m_imap.MyPosition = start;
            Assert.IsFalse(system.CanMoveDirectly(endblocked),endblocked.ToString());
            Assert.IsTrue(system.CanMoveDirectly(endok),endok.ToString());
            Assert.IsTrue(system.CanMoveDirectly(endok2),endok2.ToString());
            Assert.IsTrue(system.CanMoveDirectly(endok3),endok3.ToString());

        }

        [TestMethod]
        public void ShowScene()
        {
            UIHelper uihelper = new UIHelper();
            uihelper.Setup();
            foreach (ModelObject mobj in m_mockmodel.ModelObjects)
            {
                if (mobj.GetModelType() == ModelType.Wall)
                {
                    uihelper.AddSquare(mobj.Position, mobj.Radius);
                }
            }

          

            Vector3 start = new Vector3(4.5f, 4.5f, 0);

       
            Vector3 endblocked = new Vector3(8.5f, 4.5f, 0);
            Vector3 endok = new Vector3(5.5f, 6.5f, 0);
            Vector3 endok2 = new Vector3(1.5f, 8.5f, 0);
            Vector3 endok3 = new Vector3(6.5f, 8.5f, 0);
           
            uihelper.SetLineThickness(0.01f);
      
            uihelper.TearDown();
        }

        [TestMethod]
        public void CanMoveTest()
        {
            Vector3 start = new Vector3(4.5f, 4.5f, 0);
            Vector3 endblocked = new Vector3(8.5f, 4.5f, 0);
            Vector3 endok = new Vector3(5.5f, 6.5f, 0);
            Vector3 endok3 = new Vector3(6.5f, 8.5f, 0);
            ShowLines(start, endok3);
        }

        private void ShowLines(Vector3 start, Vector3 end)
        {
              UIHelper uihelper = new UIHelper();
            uihelper.Setup();
             uihelper.SetLineThickness(0.01f);
             Vector3[] startrad = ModelHelper.GetRadialPoints(start, ModelConstants.MOUSERADIUS);
            Vector3[] endrad = ModelHelper.GetRadialPoints(end, ModelConstants.MOUSERADIUS);
            foreach (ModelObject mobj in m_mockmodel.ModelObjects)
            {
                if (mobj.GetModelType() == ModelType.Wall)
                {
                    uihelper.AddSquare(mobj.Position, mobj.Radius);
                }
            }
              for (int i = 0; i < startrad.Length; i++)
            {
                uihelper.AddLine(startrad[i], endrad[i]);
            }
               uihelper.TearDown();
        }
        [TestMethod]
        public void CanMoveDirectlyHardTest()
        {
            SetMap2();
           
            MouseAgent mred = (RedAgent)m_mockmodel.GetModelForLocation(new Location(1, 5));
            SensorySystem system = mred.Sensor;
            Vector3 origposition = mred.Position;
            Vector3 cheese = new Vector3(2.5f, 3.5f, origposition.Z);
            for (float x = -0.2f; x < 0.5f; x += 0.1f)
            {
                mred.Position = new Vector3(origposition.X + x,origposition.Y,origposition.Z);
                bool canmove = system.CanMoveDirectly(cheese);
                float dist = Vector3.Distance(mred.Position, cheese);
                float timereq = dist / mred.MaxVelocityWalking;
                int steps = (int)(timereq / m_mocktime.DEFAULTGAMEFRAME.TotalMilliseconds);
                mred.SetDestination(cheese);
                bool collided = false; ;
                for (int i = 0; i < steps; i++)
                {
                    mred.DoBaseUpdate(m_mocktime.NextFrame());
                    if (mred.BlockDetected)
                        collided = true;
                }
                if (canmove == collided)
                {
                    mred.Position = new Vector3(origposition.X + x, origposition.Y, origposition.Z);
                    bool canmove2 = system.CanMoveDirectly(cheese);
                    Assert.IsTrue(canmove2 != collided);

                }
                Assert.IsTrue(canmove != collided);
            }
          
        }

        [TestMethod]
        public void UpdateByVisibleLocationOptimisedTest()
        {
            LocalMap map = m_rmouse.LocalMap;
            map.clearmap(MapUpdateMode.ClearAll);
            DebugHelper.Print(map);
            string oldway = @"0000000000000000W000000000W00000BR0BW000000000W000000000W0000000000000000000000000000000000000000000";
            map.clearmap(MapUpdateMode.ClearAll);

            DebugHelper.Print(map);
            m_sensor.UpdateLocalMap();
            string newway = DebugHelper.Print(map);
            Assert.AreEqual(oldway, newway);
            
        }

        [TestMethod]
        public void UpdateByVisibleLocationSpeedTest()
        {
            int count = 1000;
            DateTime startold = DateTime.Now;
       

            DateTime endold = DateTime.Now;

            DateTime startnew = DateTime.Now;
            for (int i = 0; i < count; i++)
            {
                m_sensor.UpdateLocalMap();
            }
            DateTime endnew = DateTime.Now;

            Console.WriteLine("Old way time for {0} updates = {1} New way time {2}", count, (endold - startold), (endnew - startnew));

        }

        [TestMethod]
        public void UpdateByVisibleLocationSpeedTest2()
        {
            SetMap3();
            ModelConstants.LIMITSIGHT = false;
            MouseAgent rmouse = (MouseAgent)m_mockmodel.GetModelForLocation(new Location(4, 2));
            rmouse.SetDirection(new Vector3(1,1,0));
            m_sensor = new SensorySystem(rmouse);
            m_sensor.UpdateLocalMap();
            DebugHelper.Print(rmouse.LocalMap);
            int count = 1000;
            DateTime startold = DateTime.Now;
      

            DateTime endold = DateTime.Now;

            DateTime startnew = DateTime.Now;
            for (int i = 0; i < count; i++)
            {
                m_sensor.UpdateLocalMap();
            }
            DateTime endnew = DateTime.Now;

            Console.WriteLine("Old way time for {0} updates = {1} New way time {2}", count, (endold - startold), (endnew - startnew));

        }
        public void SetMap2()
        {
         m_charmap = new char[WorldMapTests.WIDTH, WorldMapTests.HEIGHT]
         {
          
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', 'R', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
             
         };
         m_mockmodel = new WholeModel();
         m_mockmodel.Build(m_charmap, 10, 10);
           
        }

        public void SetMap4()
        {
            m_charmap = new char[WorldMapTests.WIDTH, WorldMapTests.HEIGHT]
         {
          
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', 'R' , ' ' ,' ' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
             
         };
            m_mockmodel = new WholeModel();
            m_mockmodel.Build(m_charmap, 10, 10);

        }

        public void SetMap3()
        {
            m_charmap = new char[24, 24]{
         //  0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19  20  21  22  23 
           {'W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W'}, //00
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'}, //01 
           {'W',' ',' ',' ','B',' ',' ',' ',' ',' ',' ',' ',' ','C',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'}, //02
           {'W',' ',' ',' ',' ',' ',' ',' ','C',' ',' ',' ',' ',' ',' ','W','W',' ',' ',' ',' ',' ',' ','W'}, //03
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W','W',' ',' ',' ',' ',' ',' ','W'}, //04
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W','W',' ',' ',' ',' ',' ',' ','W'}, //05
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'}, //06
           {'W',' ',' ',' ','W','W',' ','W','W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'}, //07
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'}, //08
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'}, //09
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','B',' ',' ',' ','W'}, //10
           {'W',' ',' ',' ',' ',' ',' ',' ',' ','W','W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'}, //11
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'}, //12
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'}, //13
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W','W',' ',' ',' ',' ',' ',' ','W'}, //14
           {'W','W','W',' ','W','W','W','W','W','W','W','W',' ',' ',' ','W','W',' ',' ',' ',' ',' ',' ','W'}, //15
           {'W','W','W',' ','W','W','W','W','W','W','W','W',' ',' ',' ','W','W','W',' ',' ','W','W',' ','W'}, //16
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'}, //17
           {'W',' ',' ',' ','B',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W','W',' ',' ',' ',' ',' ',' ','W'}, //18
           {'W','W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'}, //19
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'}, //20
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' ','W'}, //21
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' ','W'}, //22
           {'W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W'}, //23
             
         };

            m_mockmodel = new WholeModel();
            m_mockmodel.Build(m_charmap, 24, 24);
        }
     
     [TestCleanup]
        public void teardown()
        {
            m_mockmodel = null;
            m_charmap = null;

        }


      
         
    }
}
