﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BBKModel;
using Microsoft.Xna.Framework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace xUnitTests
{
    [TestClass]
    public class LocationTest
    {
        [TestMethod]
        public void AdjacentTest()
        {
            Location loc1 = new Location(1, 1);
            Location loc2 = new Location(5, 2);
            Location loc3 = new Location(2, 2);
            Location loc4 = new Location(1, 2);

            Assert.IsTrue(loc1.Adjacent(loc3));
            Assert.IsTrue(loc1.Adjacent(loc4));
            Assert.IsFalse(loc1.Adjacent(loc2));

        }
    }
}
