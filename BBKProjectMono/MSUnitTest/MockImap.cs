﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BBKModel;
using Microsoft.Xna.Framework;

namespace xUnitTests
{
    public class MockImap : IMapProvider
    {

        public LocalMap MyLocalMap;
        public Vector3 MyPosition;
        public float MyRotation = 0f;
        public float MyRadius = ModelConstants.MOUSERADIUS;
        public ModelType MyPredator = ModelType.None;
        public ModelType MyPrey = ModelType.None;

        public MockImap(LocalMap map, Vector3 position)
        {
            MyLocalMap = map;
            MyPosition = position;
        }



        #region IMapProvider Members

        LocalMap IMapProvider.LocalMap
        {
            get { return MyLocalMap; }
        }

        Vector3 IMapProvider.Position
        {
            get { return MyPosition; }
        }

        float IMapProvider.PlaneRotation
        {
            get { return MyRotation; }
        }

        float IMapProvider.Radius
        {
            get { return MyRadius; }
        }

        ModelType IMapProvider.Predator
        {
            get { return ModelType.None; }
        }
        ModelType IMapProvider.Target
        { get { return ModelType.None; } }


        #endregion

   
    }
}
