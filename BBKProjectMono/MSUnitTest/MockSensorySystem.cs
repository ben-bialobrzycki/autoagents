﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BBKModel;

namespace xUnitTests
{
    public class MockSensorySystem : SensorySystem 
    {
        public MockSensorySystem(ModelEntity ment):base(ment)
        {}

        public WorldMap WorldMap { get { return m_worldmap; } }
    }
}
