﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BBKModel;
using Microsoft.Xna.Framework;

namespace xUnitTests
{
    [TestClass]
    public class TargetFinderTest
    {
        public const int HEIGHT = 10;
        public const int WIDTH = 10;
        WorldMap m_worldmap;
        LocalMap m_localmap;
        WholeModel m_wholemodel;
        TargetFinder m_targetfinder;
        Location rmouseloc;
        Location bmouseloc1;
        Location bmouseloc2;
        BlueAgent bmouse;
        Vector3 rpos ;
        MockTime m_mocktime;

        char[,] m_charmap;


       [TestInitialize]
        public void Setup()
        {
            m_localmap = new LocalMap(WIDTH, HEIGHT);

            m_charmap = new char[WIDTH, HEIGHT]
         {
          
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', 'W' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', 'W' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', 'W' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', 'R' , ' ', ' ' , 'B' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
             
         };
            m_mocktime = new MockTime();
            WallClock.Update(m_mocktime.NextFrame());
            rmouseloc = new Location(3,5);
            bmouseloc1 = new Location(6,5);
            bmouseloc2 = bmouseloc1;
            bmouseloc2.Y -= 2;
            m_wholemodel = new WholeModel();
            m_wholemodel.Build(m_charmap, WIDTH, HEIGHT);
            m_worldmap = m_wholemodel.WorldMap;
            bmouse =  m_wholemodel.GetModelForLocation(bmouseloc1) as BlueAgent;
            DoUpdate();
            m_targetfinder = new TargetFinder(new MockImap(m_localmap,Vector3.Zero),ModelType.BlueAgent,TimeSpan.MaxValue);
            rpos = m_worldmap.LocationToVector(rmouseloc);
          


        }

        private void DoUpdate()
        {
            m_worldmap.UpdateWorldMap(m_wholemodel.ModelObjects);
            List<Location> visiblelocs = new List<Location>();
            for (int x = rmouseloc.X; x <= rmouseloc.X + 2; x++)
            {
                for (int y = rmouseloc.Y - 2; y < HEIGHT; y++)
                {
                    visiblelocs.Add(new Location(x, y));
                }
            }
             for (int x = rmouseloc.X; x < WIDTH; x++)
            {
                for (int y = rmouseloc.Y; y < HEIGHT; y++)
                {
                    visiblelocs.Add(new Location(x, y));
                }
            }
            m_worldmap.UpdateLocalMap(m_localmap, visiblelocs);
            WallClock.Update(m_mocktime.NextFrame());
        }

        [TestMethod]
        public void setupcheckworld()
        {
            List<ModelObject> mobjs = m_worldmap.GetAll(ModelType.BlueAgent);
            Assert.IsTrue(mobjs.Count == 1);
            Location bloc = m_worldmap.VectortoLocation(mobjs[0].Position);

            Assert.IsTrue(bloc == bmouseloc1);
        }

     
        [TestMethod]
        public void FindTargetWorks()
        {
            m_targetfinder.UseGhost = true;
            m_targetfinder.Update(m_localmap.GetAllLocations());
            Vector3 targpos = m_targetfinder.CurrentTarget.Position;
            Location targloc = m_worldmap.VectortoLocation(targpos);
            Assert.AreEqual(bmouseloc1, targloc);
        }
        [TestMethod]
        public void FindHiddenTargetWorks()
        {
            m_targetfinder.UseGhost = true;
            m_targetfinder.Update(m_localmap.GetAllLocations());
           
            bmouse.Position = m_worldmap.LocationToVector(bmouseloc2);
            DoUpdate();
            Vector3 pos = m_worldmap.LocationToVector(rmouseloc);
            m_targetfinder.Update(m_localmap.GetAllLocations());
            Vector3 targpos = m_targetfinder.CurrentTarget.Position; 
            Location targloc = m_worldmap.VectortoLocation(targpos);
            m_localmap.MarkPath(targloc);
            DebugHelper.Print(m_localmap);
            Assert.AreEqual(bmouseloc1, targloc);
        }

        [TestMethod]
        public void RemoveTargetWorks()
        {
            m_targetfinder.Update(m_localmap.GetAllLocations());
            Guid id = m_targetfinder.CurrentTarget.ID;
            m_targetfinder.RemoveTarget(id);
            Assert.IsTrue(m_targetfinder.Targets.Count == 0);

        }
    }
}
