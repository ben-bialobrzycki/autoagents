﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BBKModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Microsoft.Xna.Framework.Storage;

namespace xUnitTests
{
    [TestClass]
    public class WholeModelTests
    {
        WholeModel m_mockmodel;
        MouseAgent m_rmouse;
        MockTime m_mocktime;
        MouseAgent m_bmouse;

       [TestInitialize]
        public void Setup()
        {
             m_mockmodel = new WholeModel();
             m_mocktime = new MockTime();
          

            char[,] charmap = new char[WorldMapTests.WIDTH ,WorldMapTests.HEIGHT]
         {
          
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},
           {'B', 'W', 'B', 'R' , ' ', 'B' , 'W' ,'B' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', ' ' , 'W' ,'W' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
             
         };
            m_mockmodel.Build(charmap, WorldMapTests.WIDTH, WorldMapTests.HEIGHT);
            ModelObject mobj = m_mockmodel.GetModelForPosition(new Vector3(3.5f, 3.5f, 0));
            m_rmouse = (MouseAgent)mobj;
            m_rmouse.SetDirection(new Vector3(1, 0,0));
            
            m_bmouse = (MouseAgent)m_mockmodel.GetModelForPosition(new Vector3(7.5f,3.5f,0));
        }

     

        [TestMethod]
        public void LocalMapsAreBuiltUpTest()
        {
            DebugHelper.Print(m_rmouse.LocalMap);
            m_rmouse.SetDirection(new Vector3(1,0,0));
            m_rmouse.Update(m_mocktime.NextFrame());
            DebugHelper.Print(m_rmouse.LocalMap);
            m_rmouse.SetDirection(new Vector3(-1, 0,0));
            m_rmouse.Update(m_mocktime.NextFrame());
            DebugHelper.Print(m_rmouse.LocalMap);
            

        }

        [TestMethod]
        public void WorldMapUpdatesTest()
        {
            for (int i = 0; i < 5; i++)
            {
                DebugHelper.Print(m_mockmodel.WorldMap.CreateLocalMap());
                for (int j = 0 ;j < 20;j++)
                    m_mockmodel.Update(m_mocktime.NextFrame());
            }
        }

        [TestMethod]
        public void LocalMapModelObjectUpdateTest()
        {
            for (int i = 0; i < 5; i++)
            {
                DebugHelper.Print(m_rmouse.LocalMap);
              //  for (int j = 0; j < 20; j++)
                m_rmouse.Update(m_mocktime.NextFrame());
            }

        }

      

        [TestMethod]
        public void MobjMapsArePersonal()
        {
            foreach (ModelObject mobj1 in m_mockmodel.ModelObjects)
            {
                if (mobj1.GetType() == typeof(MouseAgent))
                {
                    MouseAgent rmouse1 = (MouseAgent)mobj1;
                    int count = 0;
                    foreach (ModelObject mobj2 in m_mockmodel.ModelObjects)
                    {
                        if (mobj2.GetType() == typeof(MouseAgent))
                        {
                            MouseAgent rmouse2 = (MouseAgent)mobj2;
                            if (rmouse1.LocalMap.Equals(rmouse2.LocalMap))
                                count++;
                        }

                    }
                    Assert.AreEqual(1, count);
                }
            }

        }
     
        [TestMethod]
        public void UpdateMouseRetainsOtherEntityPos()
        {
            m_rmouse.Update(m_mocktime.NextFrame());
            List<LocalModelObjectData> lsobjs = m_rmouse.LocalMap.GetLocationSlotObjs(new Location(5, 3));
            DebugHelper.Print(m_rmouse.LocalMap);
            DebugHelper.Print(m_mockmodel.WorldMap.CreateLocalMap());
            Assert.IsTrue(lsobjs.Count != 0);
          
            Assert.IsTrue(lsobjs.Contains(new LocalModelObjectData(ModelType.BlueAgent),new SlotModelTypeComparer()));
            for (int i = 0; i < 5; i++)
            {
                GameTime gt = m_mocktime.NextFrame();
                m_rmouse.Update(gt);
                lsobjs = m_rmouse.LocalMap.GetLocationSlotObjs(new Location(5, 3));
                Assert.IsTrue(lsobjs.Contains(new LocalModelObjectData(ModelType.BlueAgent),new SlotModelTypeComparer()));
            }
        }

        [TestMethod]
        public void MockTimerTest()
        {
            GameTime gt = m_mocktime.NextFrame();
            TimeSpan ts = gt.ElapsedGameTime;
            Assert.AreEqual(10, ts.TotalMilliseconds);
        }
      
        private int currentlyselected()
        {
            int i = 0;
            foreach (IBBKDrawable idraw in m_mockmodel.ModelObjects)
            {
                if (idraw.GetType() == typeof(MouseAgent))
                {
                    MouseAgent rmouse = (MouseAgent)idraw;
                    if (rmouse == m_mockmodel.SelectedObject)
                        return i;
                }
                i++;
            }
            return i;
        }

     [TestCleanup]
        public void TearDown()
        {

        }
    }
}
