﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BBKModel;
using Microsoft.Xna.Framework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace xUnitTests
{
    [TestClass]
    public class LocationSlotTest  
    {
        protected WholeModel m_wholemodel;

        [TestInitialize]
        public virtual void SetUp()
        {
            m_wholemodel = new WholeModel();
        }

        [TestMethod]
        public void ClearSlotTest()
        {
            LocalLocationSlot slot = new LocalLocationSlot();
            MouseAgent rmouse = new RedAgent(m_wholemodel, Vector3.Zero);
            slot.SetContent(new LocalModelObjectData(ModelType.RedAgent));
            slot.ClearSlot(MapUpdateMode.ClearEntitiesOnly);
            Assert.IsTrue(slot.Empty);
          
        }
    }
}
