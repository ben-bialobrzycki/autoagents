﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BBKModel;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace xUnitTests
{
   // [TestClass]
    public class MouseAgentTest
    {
        WholeModel m_mockmodel;
        MouseAgent m_rmouse;
        MockTime m_mocktime;

       [TestInitialize]
        public void Setup()
        {
                      char[,] charmap = new char[WorldMapTests.WIDTH, WorldMapTests.HEIGHT]
         {
          
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},
           {' ', 'W', ' ', ' ' , ' ', ' ' , 'W' ,'W' , ' ' , ' '},
           {' ', 'R', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
             
         };
                      setupformap(charmap, WorldMapTests.WIDTH, WorldMapTests.HEIGHT);
        }

        private void setupformap(char[,] charmap,int width,int height)
        {
              m_mockmodel = new WholeModel();
            m_mocktime = new MockTime();



            m_mockmodel.Build(charmap, width , height);
            ModelObject mobj = m_mockmodel.GetModelForPosition(new Vector3(1.5f, 5.5f, 0));
            if (mobj != null)
            {
                m_rmouse = (MouseAgent)mobj;
                m_rmouse.SetDirection(new Vector3(1, 0, 0));
            }

            

            SensorySystem.SetWorldMap(m_mockmodel.WorldMap);
            SensorySystem.SetObjectList(m_mockmodel.ModelObjects);
            WallClock.Update(m_mocktime.NextFrame());
        }

        [TestMethod]
        public void ConstructorTest()
        {
            BlueAgent rblue = new BlueAgent(m_mockmodel,new Vector3(1,1,0));
        }

        [TestMethod]
        public void TargetTest()
        {
            RedAgent red = new RedAgent(m_mockmodel,new Vector3(1,1,0));
            Assert.IsTrue(red.Target == ModelType.BlueAgent);
            BlueAgent blue = new BlueAgent(m_mockmodel, new Vector3(2, 1, 0));
            Assert.IsTrue(blue.Target == ModelType.Cheese);
        }

        [TestMethod]
        public void RunAwayTest2()
        {
            char[,] charmap = new char[16, 16]
         {
          
             
           {'W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','R',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ','W'},
           {'W',' ','R',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ','W','W',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ','B',' ','R',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W'},
             
         };
            setupformap(charmap, 16, 16);
            List<Location> locs = m_mockmodel.WorldMap.GetAllLocations();
            
            BlueAgent blue = (BlueAgent)m_mockmodel.GetModelForLocation(new Location(2, 13));
            m_mockmodel.WorldMap.UpdateLocalMap(blue.LocalMap, locs);
            blue.UpdateTargetFinders(locs);
            blue.UpdateSystems();
            blue.SetMode(AgentMode.RunningAway);
            Console.WriteLine("Destination = {0} UltimateDestination = {1}", blue.CurrentDestination, blue.UltimateDestination);
            DebugHelper.Print(blue.LocalMap);// blue.WayPoints
            blue.LocalMap.MarkPath(blue.WayPoints);
            DebugHelper.Print(blue.LocalMap);// blue.WayPoints
            
        }

        [TestMethod]
        public void GetSafeLocationTest()
        {
          
            char[,] charmap = new char[16, 16]
         {
          
             
           {'W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ','R',' ',' ',' ',' ',' ',' ',' ','R',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ','W',' ','B',' ',' ',' ','W',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ','W',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ','W','W','W',' ',' ','W','W',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W'},
             
         };
            setupformap(charmap, 16, 16);
          
            ModelObject mobj = m_mockmodel.GetModelForLocation(new Location(5, 9));
            BlueAgent rblue = (BlueAgent)mobj;
            m_mockmodel.WorldMap.UpdateLocalMap(rblue.LocalMap,rblue.LocalMap.GetAllLocations());
            rblue.UpdateTargetFinders(rblue.LocalMap.GetAllLocations());
            Location safeloc = rblue.GetSafeLocation();
            Console.WriteLine("Safe Loc is {0}", safeloc);
            rblue.SetMode(AgentMode.RunningAway);
            LocalMap lmap = m_mockmodel.WorldMap.CreateLocalMap();
            lmap.MarkPath(rblue.WayPoints);
            lmap.MarkPath(lmap.VectortoLocation(rblue.CurrentDestination));
            DebugHelper.Print(lmap);
            



        }

        [TestMethod]
        public void GetSafeLocationTest3()
        {

              char[,] charmap = new char[20, 20]{
         //  0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19  20  21  22  23      
           {'W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W'},//0
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},//1
           {'W',' ',' ',' ',' ',' ','B',' ',' ',' ','B',' ',' ',' ',' ',' ',' ',' ',' ','W'},//2
           {'W','W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ','W'},//3
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ','W'},//4
           {'W',' ',' ','W',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ','W'},//5
           {'W',' ',' ','W',' ',' ',' ',' ',' ',' ',' ','W','W','W',' ',' ',' ',' ',' ','W'},//6
           {'W',' ',' ','W',' ',' ',' ',' ','W',' ',' ',' ',' ','W','W','W',' ',' ',' ','W'},//7
           {'W',' ',' ','W','W','W','R',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ','W'},//8
           {'W',' ',' ','W',' ',' ',' ',' ',' ',' ',' ',' ',' ','W','R',' ',' ',' ',' ','W'},//9
           {'W',' ',' ','W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},//10
           {'W',' ',' ',' ',' ',' ',' ',' ',' ','W','W',' ',' ',' ',' ',' ',' ','W',' ','W'},//11
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ','W','W',' ',' ',' ','W',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ','W',' ','W'},
           {'W',' ','W','W','W',' ','W',' ',' ',' ',' ',' ','W',' ','W',' ',' ','W',' ','W'},
           {'W',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W'},
             
         };
            setupformap(charmap, 20, 20);

            ModelObject mobj = m_mockmodel.GetModelForLocation(new Location(6, 2));
            BlueAgent rblue = (BlueAgent)mobj;
            List<Location> locs = GetLocations(4, 8, 0, 9);
            m_mockmodel.WorldMap.UpdateLocalMap(rblue.LocalMap, locs);
            rblue.UpdateTargetFinders(locs);
            Location safeloc = rblue.GetSafeLocation();
            Console.WriteLine("Safe Loc is {0}", safeloc);
            rblue.SetMode(AgentMode.RunningAway);
            LocalMap lmap = m_mockmodel.WorldMap.CreateLocalMap();
            lmap.MarkPath(rblue.WayPoints);
            lmap.MarkPath(lmap.VectortoLocation(rblue.CurrentDestination));
            DebugHelper.Print(lmap);

        }

        [TestMethod]
        public void GetSafeLocationTest4()
        {

            char[,] charmap = new char[20, 20]{
         //  0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19  20  21  22  23       4.5 10.5
           {'W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W'},//0
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},//1
           {'W',' ',' ',' ',' ',' ','B',' ',' ',' ','B',' ',' ',' ',' ',' ',' ',' ',' ','W'},//2
           {'W','W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ','W'},//3
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ','W'},//4
           {'W',' ',' ','W',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ','W'},//5
           {'W',' ',' ','W',' ',' ',' ',' ',' ',' ',' ','W','W','W',' ',' ',' ',' ',' ','W'},//6
           {'W',' ',' ','W',' ',' ',' ',' ','W',' ',' ',' ',' ','W','W','W',' ',' ',' ','W'},//7
           {'W',' ',' ','W','W','W','R',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ','W'},//8
           {'W',' ',' ','W',' ',' ',' ',' ',' ',' ',' ',' ',' ','W','R',' ',' ',' ',' ','W'},//9
           {'W',' ',' ','W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},//10
           {'W',' ',' ',' ',' ',' ',' ',' ',' ','W','W',' ',' ',' ',' ',' ',' ','W',' ','W'},//11
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ','W','W',' ',' ',' ','W',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ','W',' ','W'},
           {'W',' ','W','W','W',' ','W',' ',' ',' ',' ',' ','W',' ','W',' ',' ','W',' ','W'},
           {'W',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','W'},
           {'W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W','W'},
             
         };
            setupformap(charmap, 20, 20);

            ModelObject mobj = m_mockmodel.GetModelForLocation(new Location(6, 2));
            BlueAgent rblue = (BlueAgent)mobj;
           // List<Location> locs = GetLocations(4, 8, 0, 9);
            List<Location> locs = m_mockmodel.WorldMap.GetAllLocations();
            m_mockmodel.WorldMap.UpdateLocalMap(rblue.LocalMap, locs);
            rblue.UpdateSystems();
            rblue.UpdateTargetFinders(locs);
            Assert.IsTrue(rblue.PredatorFinder.Targets.Count > 0);

            List<LocationScore> locscores = new List<LocationScore>();
            foreach (Location loc in locs)
            {
                Vector3 locpos = m_mockmodel.WorldMap.LocationToVector(loc);
                if (Vector3.Distance(rblue.Position,locpos) < rblue.SafeDistance)
                    locscores.Add(new LocationScore(loc,rblue.CalculateSafeScore(loc)));

            }

            int[,] inmap = GetIntMap(locscores,rblue.LocalMap.Width,rblue.LocalMap.Height);
            DebugHelper.Print(rblue.LocalMap.Width,rblue.LocalMap.Height, inmap);
            Location safeloc = rblue.GetSafeLocation();
            Console.WriteLine("Safe Loc is {0}", safeloc);

        }

     
        public int[,] GetIntMap(List<LocationScore> locscores, int width, int height)
        {
            int[,] intmap = new int[width,height];
           
            foreach (LocationScore lscore in locscores)
            {
                intmap[lscore.Location.X,lscore.Location.Y] = (int)(lscore.Score * 10); 
            }
            return intmap;
             
        }
        private List<Location> GetLocations(int startx, int endx, int starty, int endy)
        {
            List<Location> locs = new List<Location>();
            for (int x = startx ; x <= endx;x++)
            {
                for (int y = starty; y <= endy; y++)
                {
                    Location loc = new Location(x, y);
                    locs.Add(loc);
                }
            }
            return locs;

        }
    
        [TestMethod]
        public void CopyWaypointsTest()
        {
            m_mockmodel.Update(m_mocktime.NextFrame());

            m_rmouse.SetMode(AgentMode.Wandering);
            if (m_rmouse.WayPoints != null)
            {
                Assert.IsTrue(m_rmouse.WayPoints.Count > 0);
                Vector3[] vecarray = new Vector3[m_rmouse.WayPoints.Count];
                m_rmouse.WayPoints.CopyTo(vecarray,0);
                Assert.IsTrue(m_rmouse.WayPoints.Count > 0);
            }

        }
    }

    public struct LocationScore
    {
        public Location Location;
        public float Score;
        public LocationScore(Location loc, float score) { Location = loc; Score = score; }
    }
}
