﻿using System;
using System.Collections.Generic;

using BBKModel;
using Microsoft.Xna.Framework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace xUnitTests
{
    [TestClass]
    public class WorldMapTests
    {
        public const int HEIGHT = 10;
        public const int WIDTH = 10;
        WorldMap m_worldmap;
        LocalMap m_localmap;
        WholeModel m_wholemodel;

        char[,] m_charmap;


       [TestInitialize]
        public void Setup()
        {
           
           
            m_charmap  = new char[WIDTH,HEIGHT]
         {
          //  0   1    2    3     4    5     6    7     8     9
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '}, //0
           {' ', ' ', ' ', ' ' , ' ', 'W' , ' ' ,' ' , ' ' , ' '}, //1
           {' ', ' ', ' ', ' ' , 'C', 'W' , ' ' ,' ' , ' ' , ' '}, //2 
           {' ', 'R', ' ', ' ' , 'B', 'W' , 'W' ,' ' , ' ' , ' '}, //3
           {' ', ' ', ' ', ' ' , ' ', 'W' , ' ' ,' ' , ' ' , ' '}, //4
           {' ', ' ', ' ', ' ' , ' ', 'W' , 'B' ,' ' , ' ' , ' '}, //5 
           {' ', ' ', ' ', ' ' , ' ', 'W' , ' ' ,' ' , ' ' , ' '}, //6
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '}, //7
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '}, //8
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '}, //9
             
         };
            m_wholemodel = new WholeModel();
            m_wholemodel.Build(m_charmap,WIDTH,HEIGHT);
            m_worldmap = m_wholemodel.WorldMap;
            m_localmap = m_worldmap.CreateLocalMap();
            

        }
        [TestMethod]
        public void WorldMapBuiltbyModel1()
        {
            Assert.AreEqual(1,CountModel(m_wholemodel.ModelObjects,ModelType.RedAgent),"A");
            Assert.AreEqual(2, CountModel(m_wholemodel.ModelObjects, ModelType.BlueAgent), "B");
            Assert.AreEqual(7, CountModel(m_wholemodel.ModelObjects, ModelType.Wall), "C");
            Assert.AreEqual(1, CountModel(m_wholemodel.ModelObjects, ModelType.Cheese), "D");
       
        }
    

        [TestMethod]
        public void EligibleTypeTest()
        {
           Assert.IsFalse(m_worldmap.EligibleType(ModelType.Floor),"Floor Not Allowed in Map");
           Assert.IsTrue(m_worldmap.EligibleType(ModelType.Cheese), "Cheese  Allowed in Map");
        }
        [TestMethod]
        public void GetRelevantObjectTest()
        {
            Location loc = new Location(4, 3);
            List<ModelObject> withentities = m_worldmap.GetRelevantObjects(loc);
            Assert.AreEqual(5, withentities.Count,"A");
            Assert.AreEqual(1,CountModel(withentities,ModelType.BlueAgent),"B");
            Assert.AreEqual(1, CountModel(withentities, ModelType.Cheese), "B2");
            Assert.AreEqual(3, CountModel(withentities, ModelType.Wall),"C");

    
           
            
        }

        private int CountModel(List<ModelObject> mobjs,ModelType mtype)
        {
            int Count = 0;
            foreach (ModelObject mobj in mobjs)
            {
                if (mobj.GetModelType() == mtype )
                   Count++;
               
            }
            return Count;
        }

   

        [TestMethod]
       public void ClearMapTest()
       {
           DebugHelper.Print(m_worldmap.CreateLocalMap());
           m_worldmap.clearmap(MapUpdateMode.ClearEntitiesOnly);
           DebugHelper.Print(m_worldmap.CreateLocalMap());
           
       }
     

        private void PrintUpdateTime(LocalMap map)
        {
            Console.WriteLine("World \n");
            
            for (int i = 0; i < map.Height; i++)
            {
                for (int j = 0; j < map.Width; j++)
                {
                    TimeSpan ts = map.TimeSinceLastUpdated(new Location(j,i));
                    Console.Write(", " + ts.TotalMilliseconds.ToString("N3"));
                }
                Console.Write("\n");

            }
        }
      

   
        [TestMethod]
        public void  IsBlockedTest()
        {
              char[,] charmap  = new char[WIDTH,HEIGHT]
         {
          // 0    1    2    3     4    5     6    7     8     9
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},//0
           {' ', ' ', ' ', ' ' , ' ', 'W' , ' ' ,' ' , ' ' , ' '},//1
           {' ', ' ', ' ', ' ' , 'C', 'W' , ' ' ,' ' , ' ' , ' '},//2
           {' ', 'R', ' ', ' ' , 'B', 'W' , 'W' ,' ' , ' ' , ' '},//3
           {' ', ' ', ' ', ' ' , 'B', 'W' , ' ' ,' ' , ' ' , ' '},//4
           {' ', ' ', ' ', ' ' , ' ', 'W' , 'B' ,' ' , ' ' , ' '},//5
           {' ', ' ', ' ', ' ' , ' ', 'W' , ' ' ,' ' , ' ' , ' '},//6
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},//7
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},//8
           {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},//9
             
         };
            Location startloc = new Location(4,4);
            Location destloc = new Location(4,3);
              m_wholemodel = new WholeModel();
            m_wholemodel.Build(charmap,WIDTH, HEIGHT);
            LocalMap map = m_wholemodel.WorldMap.CreateLocalMap();
            map.CurrentLocation = startloc;

            Assert.IsTrue(map.IsBlocked(destloc));
        }

        [TestMethod]
        public void SetPenaltiesTest()
        {
            Location loc = new Location(2, 4);
            List<Location> loclist = new List<Location>();
            loclist.Add(loc);
            DebugHelper.PrintTerrainPenalties(m_localmap);
            m_localmap.SetPenalties(loclist, 3);
            DebugHelper.PrintTerrainPenalties(m_localmap);
        }

        [TestMethod]
        public void IsDiagonalTest()
        {
            Location current = new Location(6, 2);
            Location dest1 = new Location(7,3);
            Location dest2 = new Location(6,3);

            Assert.IsTrue(m_localmap.IsDiagonal(current, dest1));
            Assert.IsFalse(m_localmap.IsDiagonal(current, dest2));
        }
        [TestMethod]
        public void BlockedDiagonalTest()
        {
            Location current = new Location(6, 2);
            Location dest1 = new Location(7, 3);
            Location dest3 = new Location(7, 1);
            Assert.IsTrue(m_localmap.BlockedDiagonal(current, dest1),"A1");
            Assert.IsFalse(m_localmap.BlockedDiagonal(current, dest3),"A2");
        }


        [TestMethod]
        public void GetSurroundingLocationsTest()
        {
            Location loc = new Location(6,2);
            m_worldmap.CurrentLocation = loc;
            Assert.AreEqual(8,m_worldmap.GetSurroundingLocations(loc).Count);
            Assert.AreEqual(4,m_worldmap.GetSurroundingLocations(loc, false).Count);

        }

        [TestMethod]
        public void GetDistanceSortedListTest()
        {
            m_worldmap.CurrentLocation = new Location(3, 3);
            List<Location> sortedlist = m_worldmap.GetDistanceSortedList();
            int pos = 0;
            float lastdist = 0;
            foreach (Location loc in sortedlist)
            {
                float dist = Vector3.Distance(m_worldmap.LocationToVector(m_worldmap.CurrentLocation),m_worldmap.LocationToVector(loc));
                Console.WriteLine("List Position {0} Location {1} Distance {2}", pos, loc, dist);
                if (pos > 0)
                    Assert.IsTrue(lastdist <= dist);
                pos++;
                lastdist = dist;
            }
        }
     [TestCleanup]
        public void TearDown()
        {
            m_worldmap = null;
            m_localmap = null;
        }

    }
}
