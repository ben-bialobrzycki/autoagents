﻿using System.Drawing;
using System.Windows.Forms;

namespace xUnitTests
{
    struct LinePoint
    {
        public Point A;
        public Point B;
        public LinePoint(Point A, Point B)
        {
            this.A = A;
            this.B = B;
        }
    }

    
    public partial class CanvasForm : Form
    {
      
        public System.Windows.Forms.Panel pnlCanvas;
        public CanvasForm()
        {
            InitializeComponent();
    
          
            pnlCanvas.Refresh();
        }

   
        private void pnlCanvas_Paint(object sender, PaintEventArgs e)
        {
          
        }
    }
}
