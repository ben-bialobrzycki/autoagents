﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BBKModel;
using Microsoft.Xna.Framework;

namespace xUnitTests
{
    [TestClass]
    public class TimerTest
    {

        [TestMethod]
        public void TimeOutWorks()
        {
            Timer timer = new Timer();
            timer.Reset(TimeSpan.FromSeconds(0.5));
            MockTime mime = new MockTime();
            TimeSpan time = TimeSpan.Zero;
            while (time < TimeSpan.FromSeconds(0.6))
            {
                Console.WriteLine(timer.TimeLeft);
                GameTime gt = mime.NextFrame();
                WallClock.Update(gt);
                time += gt.ElapsedGameTime;
            }
            Console.WriteLine(timer.TimeLeft);
            Assert.IsTrue(timer.Timeup);
            
        }
    }
}
