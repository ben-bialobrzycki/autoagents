﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using BBKModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace xUnitTests
{
    [TestClass]
    public class ModelHelperTests 
    {
        public static float SIGHTANGLE = MathHelper.ToRadians(30);
        [TestMethod]
        public void InSightConeTest()
        {
            Vector3 locvector = new Vector3(3,3,0);
            Vector3 sightvec = new Vector3(1,0,0);
            Vector3 postocheck = new Vector3(5,3,0);


            float AngleLimit = SIGHTANGLE;


            bool result = ModelHelper.InSightCone(locvector, postocheck, sightvec);
            Assert.IsTrue(result,"assert1");
            sightvec = new Vector3(0, 1, 0);
            result = ModelHelper.InSightCone(locvector, postocheck, sightvec);
            Assert.IsFalse(result,"assert2");
            sightvec = Vector3.UnitX;
            postocheck = new Vector3(12, 2, 0);
            result = ModelHelper.InSightCone(locvector, postocheck, sightvec);
            Assert.IsTrue(result,"assert3");

            float y = (float)(Math.Sin(MathHelper.ToRadians(AngleLimit - 10)) * sightvec.Length());
            float x = (float)(Math.Cos(MathHelper.ToRadians(AngleLimit - 10)) * sightvec.Length());
            Vector3 check = locvector + sightvec;
            Vector3 check1 = check;
            check1.X += x;
            check1.Y += y;
            Assert.IsTrue(ModelHelper.InSightCone(locvector, check1, sightvec),"assert4");
            Vector3 check2 = check;
            check2.X += x;
            check2.Y -= y;
            Assert.IsTrue(ModelHelper.InSightCone(locvector, check2, sightvec), "assert5");

            y = (float)(Math.Sin(MathHelper.ToRadians(AngleLimit - 5)) * sightvec.Length());
            x = (float)(Math.Cos(MathHelper.ToRadians(AngleLimit - 5)) * sightvec.Length());


            sightvec = new Vector3(5, 1, 0); /*has angle 11.3 deg with x axis */

            y = (float)(Math.Sin(MathHelper.ToRadians(15)) * sightvec.Length());
            x = (float)(Math.Cos(MathHelper.ToRadians(15)) * sightvec.Length());
            Vector3 lowerlimitbelow = locvector +  new Vector3(x, -y, 0);
            Assert.IsTrue(ModelHelper.InSightCone(locvector, lowerlimitbelow, sightvec), "assert6");
            Vector3 lowerlimitup = locvector + new Vector3(x, y, 0);
            Assert.IsTrue(ModelHelper.InSightCone(locvector, lowerlimitup, sightvec), "assert7");

        }

        private void ShowVisible(Vector3 start, float angle)
        {
            int size = 10;
            int[,] intmap = new int[size, size];
            for (int x = 0; x < size; x++)
            {
                for (int y = 0; y < size; y++)
                {
                    if (ModelHelper.InSightCone(start, new Vector3(x, y, 0), angle))
                        intmap[x, y] = 1;
                    else if ((int)start.X == x && (int)start.Y == y)
                        intmap[x, y] = -1;
                    else
                        intmap[x, y] = 0;
                    
                }
            }
            DebugHelper.Print(size, size, intmap,0);
        }
        [TestMethod]
        public void InSightConeNegX()
        {
            Vector3 start = new Vector3(5,5,0);
            Vector3 toseebelow = new Vector3(2,6,0);
            Vector3 toseeabove = new Vector3(2, 4, 0);
            Vector3 tonotseebelow = new Vector3(9, 6, 0);
            Vector3 tonotseeabove = new Vector3(9, 4, 0);
            Vector3 dirn = new Vector3(-1,0.05f,0);
        
            float angle = ModelHelper.RotationForDirection(dirn);
            Console.WriteLine("Angle {0}", angle);
            ShowVisible(start, angle);

           
            bool resultbelow = ModelHelper.InSightCone(start, toseebelow, angle);
            bool resultabove = ModelHelper.InSightCone(start, toseeabove, angle);
            Assert.IsTrue(resultbelow, "Below");
            Assert.IsTrue(resultabove, "Above");
            Assert.IsFalse(ModelHelper.InSightCone(start,tonotseebelow,angle));
            Assert.IsFalse(ModelHelper.InSightCone(start, tonotseeabove, angle));


        }

        [TestMethod]
        public void InSightConePosX()
        {
            Vector3 start = new Vector3(5, 5, 0);
            Vector3 toseebelow = new Vector3(9, 6, 0);
            Vector3 toseeabove = new Vector3(9, 4, 0);
            Vector3 tonotseebelow = new Vector3(2, 6, 0);
            Vector3 tonotseeabove = new Vector3(2, 4, 0);

            for (float mod = 0.1f; mod > -0.3f; mod -= 0.1f)
            {
                Vector3 dirn = new Vector3(1, mod, 0);
                float angle = ModelHelper.RotationForDirection(dirn);

                Console.WriteLine("Angle {0} Direction {1}", angle, dirn);
                ShowVisible(start, angle);
                bool resultbelow = ModelHelper.InSightCone(start, toseebelow, angle);
                bool resultabove = ModelHelper.InSightCone(start, toseeabove, angle);
                Assert.IsTrue(resultbelow, "Below");
                Assert.IsTrue(resultabove, "Above");
                Assert.IsFalse(ModelHelper.InSightCone(start, tonotseebelow, angle));
                Assert.IsFalse(ModelHelper.InSightCone(start, tonotseeabove, angle));
            }
        }

        [TestMethod]
        public void InSightConePosY()
        {
            Vector3 start = new Vector3(5, 5, 0);
            Vector3 toseebelow = new Vector3(6, 9, 0);
            Vector3 toseeabove = new Vector3(4, 9, 0);
            Vector3 dirn = new Vector3(0, 1, 0);
            float angle = ModelHelper.RotationForDirection(dirn);

            ShowVisible(start, angle);
            bool resultbelow = ModelHelper.InSightCone(start, toseebelow, angle);
            bool resultabove = ModelHelper.InSightCone(start, toseeabove, angle);
            Assert.IsTrue(resultbelow, "Below");
            Assert.IsTrue(resultabove, "Above");
            Vector3 tonotseebelow = new Vector3(6, 1, 0);
            Vector3 tonotseeabove = new Vector3(4, 1, 0);
            Assert.IsFalse(ModelHelper.InSightCone(start, tonotseebelow, angle));
            Assert.IsFalse(ModelHelper.InSightCone(start, tonotseeabove, angle));

        }

        [TestMethod]
        public void InSightConeNegY()
        {
            Vector3 start = new Vector3(5, 5, 0);
            Vector3 toseebelow = new Vector3(6, 1, 0);
            Vector3 toseeabove = new Vector3(4, 1, 0);
            Vector3 dirn = new Vector3(0, -1, 0);
          
            float angle = ModelHelper.RotationForDirection(dirn);
            ShowVisible(start, angle);
            bool resultbelow = ModelHelper.InSightCone(start, toseebelow, angle);
            bool resultabove = ModelHelper.InSightCone(start, toseeabove, angle);
            Assert.IsTrue(resultbelow, "Below");
            Assert.IsTrue(resultabove, "Above");
            Vector3 tonotseebelow = new Vector3(6, 9, 0);
            Vector3 tonotseeabove = new Vector3(4, 9, 0);
            Assert.IsFalse(ModelHelper.InSightCone(start, tonotseebelow, angle));
            Assert.IsFalse(ModelHelper.InSightCone(start, tonotseeabove, angle));

        }

        [TestMethod]
        public void CalculateLookAtTest()
        {
            List<Vector3> lookat = new List<Vector3>();
            List<float> Angles = new List<float>();

            Vector3 position = new Vector3(1, 3, 0);

            Angles.Add(MathHelper.ToRadians(0));
            lookat.Add(new Vector3(2, 3, 0));
           
            Angles.Add(MathHelper.ToRadians(90));
            lookat.Add(new Vector3(1, 4, 0));
            
            Angles.Add(MathHelper.ToRadians(180));
            lookat.Add(new Vector3(0, 3, 0));

            for (int i = 0; i < Angles.Count;i++)
            {
                Vector3 calclookat = ModelHelper.CalculateLookAt(position,Angles[i]);
                Assert.AreEqual(lookat[i], calclookat,"Failed at angle " + Angles[i].ToString());
            }


        }

        [TestMethod]
        public void MathTest()
        {
            Vector3 loctoposition = new Vector3(3,4,0);
            /*From XNA Forum*/

            for (int i = 0; i < 20;i++ )
            {
                float axisangle = Math.Abs((float)Math.Atan2(loctoposition.Y, loctoposition.X));
                float axisdegrees = MathHelper.ToDegrees(axisangle);
                Console.WriteLine("Vector {0} angle {1} rad angle {2} degrees", loctoposition, axisangle,axisdegrees);
                loctoposition.X -= 0.3f;
            }
       

        }

        [TestMethod]
        public void RotationAngleTests()
        {
            for (int i = 0; i < 10; i++)
            {
                Vector3 direction = new Vector3(ModelHelper.GetRandomVector2(2.0f), 0);
                float oldway = MathHelper.ToDegrees( ModelHelper.RotationForMovement(direction));
                float newway = MathHelper.ToDegrees(ModelHelper.RotationForDirection(direction));
                Console.WriteLine("Vector {0} Correct Angle {1} New Angle {2}", direction, oldway, newway);

            }
        }

        [TestMethod]
        public void SetRotationForForwardTest()
        {

            Vector3 velocity = new Vector3(2, 0f, 0f);
            float roty = ModelHelper.RotationForMovement(velocity);
            Assert.AreEqual(roty, 0);
            velocity = new Vector3(1f, 1f, 0f);
            roty = ModelHelper.RotationForMovement(velocity);
            Assert.AreEqual(roty, MathHelper.PiOver4);
            velocity = new Vector3(1f, 1f, 0f);
            roty = ModelHelper.RotationForMovement(velocity);
            Assert.AreEqual(roty, MathHelper.PiOver4);


        }
      

    }
}
