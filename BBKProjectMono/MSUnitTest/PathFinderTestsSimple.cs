﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using BBKModel;

namespace xUnitTests
{
    [TestClass]
    public class PathFinderTestSimple
    {
        LocalMap m_map;
        PathFinder m_pathfinder;
        Stack<Vector3> m_result;
        WholeModel m_wholemodel;
       [TestInitialize]
        public void SetUp()
        {
            char[,] charmap = new char[WorldMapTests.WIDTH, WorldMapTests.HEIGHT]
             {
              
               {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
               {' ', ' ', ' ', ' ' , 'W', ' ' , 'W' ,' ' , ' ' , ' '},
               {' ', 'W', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},
               {' ', 'W', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},
               {' ', 'W', ' ', ' ' , ' ', ' ' , 'W' ,'W' , ' ' , ' '},
               {' ', ' ', ' ', ' ' , ' ', ' ' , 'W' ,' ' , ' ' , ' '},
               {' ', ' ', 'W', 'W' , 'W', ' ' , 'W' ,' ' , ' ' , ' '},
               {' ', ' ', ' ', ' ' , 'W', ' ' , ' ' ,' ' , ' ' , ' '},
               {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
               {' ', ' ', ' ', ' ' , ' ', ' ' , ' ' ,' ' , ' ' , ' '},
                 
             };
            m_wholemodel = new WholeModel();
            m_wholemodel.Build(charmap, WorldMapTests.WIDTH, WorldMapTests.HEIGHT);
            m_map = m_wholemodel.WorldMap.CreateLocalMap();
            m_pathfinder = new PathFinder(m_map);


        }

        [TestMethod]
        public void FindShortPathTest()
        {
            DebugHelper.Print(m_map, true);
            Location startloc = new Location(3, 8);
            Location endloc = new Location(7, 2);
    
            Vector3 startvec = m_map.LocationToVector(startloc);
            Vector3 endvec = m_map.LocationToVector(endloc);
            m_result = m_pathfinder.FindShortPath(startvec, endvec);
          
            Vector3[] array = m_result.ToArray();
            string correct = @"00000000000000W0W0000W0000WX000W0000W0X00W0000WWX0000000W0X000WWW0WX000000W0XX00000XXX00000000000000";
            m_map.MarkPath(m_result);
            string result = DebugHelper.Print(m_map, false, false);
        
            DebugHelper.Print(m_map);
            Assert.AreEqual(result, correct);
            Assert.AreEqual(10, array.Length);
        
        }

        [TestMethod]
        public void AnotherPathTest()
        {


            PredefinedMap pmap = PredefinedMap.GetPredefinedMap(1);
            WholeModel mwmodel = new WholeModel();
            mwmodel.Build(pmap.CharMap, pmap.Width, pmap.Height);
            m_map = mwmodel.WorldMap.CreateLocalMap();
            PathFinder finder = new PathFinder(m_map);
            Vector3 end = new Vector3(1.5f, 1.5f, 0);
            Vector3 start = new Vector3(1.5f, 7.5f, 0);
            m_result = finder.FindShortPath(start, end);
            m_map.MarkPath(m_result);
            DebugHelper.Print(m_map);
            Vector3[] waypoints = finder.FindShortPath(start, end).ToArray();
            foreach (Vector3 point in waypoints)
            {
                foreach (ModelObject mobj in mwmodel.ModelObjects)
                {
                    if (mobj.GetModelType() == ModelType.Wall)
                    {
                        float dist = Vector3.Distance(point, mobj.Position);
                        Assert.IsTrue(dist > 0.25f, "Bad Point " + point);
                    }
                }
            }
        }

        [TestMethod]
        public void NoCutCorners()
        {
            PredefinedMap pmap = PredefinedMap.GetPredefinedMap(7);
            m_wholemodel.Build(pmap.CharMap, pmap.Width, pmap.Height);
            m_map = m_wholemodel.WorldMap.CreateLocalMap();
            Location start = new Location(9, 10);
            Location end = new Location(8, 11);
            PathFinder pfinder = new PathFinder(m_map);
            m_result = pfinder.FindShortPath(m_map.LocationToVector(start), m_map.LocationToVector(end));
            // Assert.IsTrue(m_result.Count > 2);
            m_map.MarkPath(m_result);
            DebugHelper.Print(m_map);

        }
        [TestMethod]
        public void NoCutCorners2()
        {

            PredefinedMap pmap = PredefinedMap.GetPredefinedMap(7);
            m_wholemodel.Build(pmap);
            m_map = m_wholemodel.WorldMap.CreateLocalMap();
            Location start = new Location(9, 10);
            Location end = new Location(8, 11);
            PathFinder pfinder = new PathFinder(m_map);
            Vector3 startvec = new Vector3(6.439505f, 17.26317f, 0.2f);
            Vector3 endvec = m_map.LocationToVector(new Location(1, 1));
            m_result = pfinder.FindShortPath(startvec, endvec);
            // Assert.IsTrue(m_result.Count > 2);
            m_map.MarkPath(m_result);
            DebugHelper.Print(m_map);
        }

      


    }


}
