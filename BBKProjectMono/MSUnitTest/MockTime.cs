﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace xUnitTests
{
    public class MockTime
    {
        TimeSpan m_totalgt = new TimeSpan(0);
        TimeSpan m_totalrt = new TimeSpan(0);
        TimeSpan m_gameframe ;
        TimeSpan m_realframe ;
        public TimeSpan DEFAULTGAMEFRAME = new TimeSpan(100000);
        public TimeSpan DEFAULTREALFRAME = new TimeSpan(100000);

      
        public MockTime()
        {

        }


        private GameTime IncrementFrames()
        {
            m_totalgt += m_gameframe;
            m_totalrt += m_realframe;
            //  GameTime gt = new GameTime(m_totalrt, m_realframe, m_totalgt, m_gameframe);
            GameTime gt = new GameTime( m_totalgt, m_gameframe);
            return gt;
        }
        public GameTime NextFrame()
        {
            m_gameframe = DEFAULTGAMEFRAME;
            m_realframe = DEFAULTREALFRAME;
            return IncrementFrames();
        }

        public GameTime NextFrame(int millisec)
        {
            m_gameframe = new TimeSpan(millisec * 10000);
            m_realframe = new TimeSpan(millisec * 10000);
            return IncrementFrames();
        }
    }
}
