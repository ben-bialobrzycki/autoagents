﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using BBKModel;

namespace xUnitTests
{
    public class UIHelper
    {
        TimeSpan m_sleeptime = TimeSpan.FromSeconds(2);
        CanvasForm m_form;
        Pen m_bluepen;
        int m_bluethickness;
        float blackdefault = 0.1f;
        Pen m_blackpen;
        List<LinePoint> m_lines;
        List<System.Drawing.Rectangle> m_rectangles;
        Random m_rand;
        public float Scale = 60.0f;
        public void SetLineThickness(float width)
        {
            m_bluethickness = (int)(width * Scale);
        }

        public UIHelper()
        {
           m_rand = new Random(DateTime.Now.Millisecond);
           m_form = new CanvasForm();
           m_form.pnlCanvas.Paint += new System.Windows.Forms.PaintEventHandler(pnlCanvas_Paint);
           m_bluepen = new Pen(System.Drawing.Color.Blue, 10.0f);
           m_blackpen = new Pen(System.Drawing.Color.Black, 15.0f);
           m_lines = new List<LinePoint>();
           m_rectangles = new List<System.Drawing.Rectangle>();
           SetLineThickness(ModelConstants.MOUSERADIUS * 2);
          // dummy();
        }

        public void Setup()
        {
            Application.DoEvents();
            m_form.Focus();
            m_form.Show();

            m_form.Focus();
        }
        void pnlCanvas_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            m_bluepen.Width = m_bluethickness;
            m_blackpen.Width = (int)(Scale * blackdefault);
            foreach (LinePoint line in m_lines)
                e.Graphics.DrawLine(m_bluepen, line.A, line.B);
            foreach (System.Drawing.Rectangle rec in m_rectangles)
                e.Graphics.DrawRectangle(m_blackpen, rec);
        }

        public void AddLine(Vector3 Avec, Vector3 Bvec)
        {
            AddLine(VectorToPoint(Avec), VectorToPoint(Bvec));
        }

        public void AddSquare(Vector3 Centre, float length)
        {
            AddSquare(VectorToPoint(Centre), (int)(length * Scale));
        }
        private System.Drawing.Point VectorToPoint(Vector3 vec)
        {
            return new System.Drawing.Point((int)(vec.X * Scale),(int)(vec.Y * Scale)) ;
        }
        public void AddLine(System.Drawing.Point A, System.Drawing.Point B)
        {
            m_lines.Add(new LinePoint(A, B));
        }
        public void AddSquare(System.Drawing.Point centre, int length)
        {
            System.Drawing.Point topleft = new System.Drawing.Point((centre.X - length / 2), centre.Y - length / 2);
            m_rectangles.Add(new System.Drawing.Rectangle(topleft, new Size(length, length)));
        }
        private void dummy()
        {
            int max = 100;
          
            for (int i = 0; i < 10; i++)
            {
                AddSquare(RandomPoint(max), m_rand.Next(max));
            }
        }

        private System.Drawing.Point RandomPoint(int max)
        {
            return new System.Drawing.Point(m_rand.Next(max));
        }
        public void ShowForm()
        {
          
          
        }

        public void TearDown()
        {
            m_form.Refresh();
            m_form.Show();
            Thread.Sleep(m_sleeptime);
            m_form.Close();
        }
    }
}
