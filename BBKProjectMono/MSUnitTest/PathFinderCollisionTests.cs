﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using BBKModel;

namespace xUnitTests
{
    [TestClass]
    public class PathFinderCollisionTests
    {
        LocalMap m_map;
        PathFinder m_pathfindergroup;
        Stack<Vector3> m_result;
        WholeModel m_wholemodel;

       [TestInitialize]
        public void SetUp()
        {
            PathFinder.DEBUG = true;
            m_wholemodel = new WholeModel();

            PredefinedMap pmap = Map1();
            m_wholemodel.Build(pmap.CharMap, pmap.Width, pmap.Height);
            m_map = m_wholemodel.WorldMap.CreateLocalMap();
            m_pathfindergroup = new PathFinder(m_map);


        }

        private PredefinedMap Map1()
        {
            const int MAP1W = 16;
            const int MAP1H = 16;
            PredefinedMap pmap = new PredefinedMap(MAP1W, MAP1H);
            pmap.CharMap = new char[MAP1W, MAP1H]{
          
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ','W',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ','C',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ','W',' ','W',' ',' ','W',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ','B',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ','B',' ',' ',' ','W','W','W','W',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ','B','B',' ','C',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ','W','W','W','W',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
             
         };
            return pmap;
        }

        [TestMethod]
        public void MoveAroundeachOtherTest()
        {
            Location start = new Location(2,10);
            Vector3 startvec = m_map.LocationToVector(start);
            Location othermouse = new Location(2, 9);

            Location end = new Location(2,6);
            Vector3 endvec = m_map.LocationToVector(end);
            m_result = m_pathfindergroup.FindShortPath(startvec, endvec);
            Vector3[] vecarray = m_result.ToArray();
            m_map.MarkPath(m_result);
            DebugHelper.Print(m_map);
            Assert.IsFalse(Collides(vecarray,m_map.LocationToVector(othermouse)));
            
            

        }
        private bool Collides(Vector3[] array, Vector3 vec)
        {
            foreach (Vector3 arrayvec in array)
            {
                if (Vector3.Distance(arrayvec, vec) < 0.35f)
                    return true;
            }
            return false;
        }
    }
}
