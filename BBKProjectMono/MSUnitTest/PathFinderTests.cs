﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using BBKModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace xUnitTests
{
    [TestClass]
    public class PathFinderTests
    {
        LocalMap m_map;
        PathFinder m_pathfindergroup;
        Stack<Vector3> m_result;
        WholeModel m_wholemodel;
       
  
       [TestInitialize]
        public void SetUp()
        {
           Setupformap(Map1());


        }

        private void Setupformap(PredefinedMap pmap)
        {
            PathFinder.DEBUG = false;
            m_wholemodel = new WholeModel();

            m_wholemodel.Build(pmap.CharMap, pmap.Width, pmap.Height);
            m_map = m_wholemodel.WorldMap.CreateLocalMap();
            m_pathfindergroup = new PathFinder(m_map);
        }

        private PredefinedMap Map1()
        {
            const int MAP1W = 16;
            const int MAP1H = 16;
            PredefinedMap pmap = new PredefinedMap(MAP1W, MAP1H);
            pmap.CharMap = new char[MAP1W, MAP1H]{
          
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ','W',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ','B',' ','W',' ',' ',' ',' ',' ',' ',' '},
           {' ','B',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ','W',' ','W',' ',' ','W',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ','W','W',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ','R',' ','R',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
             
         };
            return pmap;
        }

        private PredefinedMap Map2()
        {
            const int MAP1W = 16;
            const int MAP1H = 16;
            PredefinedMap pmap = new PredefinedMap(MAP1W, MAP1H);
            pmap.CharMap = new char[MAP1W, MAP1H]{
          
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ','W',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' '},
           {' ','B',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ','W',' ','W',' ',' ','W',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ','W',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ','R',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ','W','W',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ','B',' ',' ','R',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
           {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '},
             
         };
            return pmap;
        }

    

        [TestMethod]
        public void UseTerrainPenaltiesTest()
        {
            Setupformap(Map2());

            DebugHelper.Print(m_map, true);
            Location startloc1 = new Location(2, 13);
            
            Location endloc = new Location(11, 2);

            Vector3 startvec1 = m_map.LocationToVector(startloc1);
            List<Location> badlocs = m_map.GetAllLocation(ModelType.RedAgent);
            Assert.IsTrue(badlocs.Count == 2);
            m_map.SetPenalties(badlocs,3);
            DebugHelper.PrintTerrainPenalties(m_map);
            Vector3 endvec = m_map.LocationToVector(endloc);

            PathFinder.DEBUG = true;
            m_result = m_pathfindergroup.FindShortPath(startvec1, endvec, true);
            m_map.MarkPath(m_result);
            DebugHelper.Print(m_map);
            DebugHelper.Print(m_map.Width, m_map.Height, m_pathfindergroup.m_fvaluemap);
        }
        [TestMethod]
        public void UseTerrainPenaltiesTestWithOldPathFinder()
        {
            Setupformap(Map2());

            DebugHelper.Print(m_map, true);
            Location startloc1 = new Location(2, 5);

            Location endloc = new Location(11, 2);

            Vector3 startvec1 = m_map.LocationToVector(startloc1);
            List<Location> badlocs = m_map.GetAllLocation(ModelType.RedAgent);
            Assert.IsTrue(badlocs.Count == 2);
            m_map.SetPenalties(badlocs, 3);
            DebugHelper.PrintTerrainPenalties(m_map);
            Vector3 endvec = m_map.LocationToVector(endloc);
              m_result = new PathFinder(m_map).FindShortPath(startvec1, endvec);
            m_map.MarkPath(m_result);
            DebugHelper.Print(m_map);
        }
    
    }
}
