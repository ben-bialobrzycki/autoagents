﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace xUnitTests
{
    [TestClass]
    public class MockTimeTests
    {
        [TestMethod]
        public void MockTimeTest()
        {
            MockTime mtime = new MockTime();
            GameTime gt = mtime.NextFrame(20);
            int ms = gt.ElapsedGameTime.Milliseconds;
            Assert.AreEqual(20, ms);

        }
    }
}
