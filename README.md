# Autonomous Agents#

Final Year Msc Computer Science Project From Around 2009 - Simulating Behaviour of Autonomous Agents In Games
This project was mostly a chance to explore various AI Techniques, Path Finding, State Machines for Behavioural states, Modelling partial knowledge of environment, Modelling sensory input as well as some 3D graphics.

This Repo is purely for demonstration purposes

Demo Video of the application running here: [https://www.youtube.com/watch?v=C86UKXlWFEw](https://www.youtube.com/watch?v=C86UKXlWFEw)

### How do I get set up? ###

* Originally Built with XNA, but now migrated to Monogame, monogame packages available through Nuget. 
* Pre Built Binary for windows is available on Downloads page - BBKAutoAgentsRelease.zip - Unzip and Run Release/BBKProjectMono.exe

###Controls

'D' To Toggle Debug Info
'TAB' Switches Currently selected Agent
'F1 to F8' - switch between pre-defined maps
'W' - Toggle Showing Waypoint Paths

Numpad 7,9,1,3, 5 Switches Camera Between Various Presets 2 Is the Camera View from the currently selected agent

'C' - Allows manual control of the selected agent via Arrow Keys