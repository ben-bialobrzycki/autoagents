﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace DiagramProject
//{
//    public class Class1
//    {

//private void BuildLists(Location loctosee)
//{
//    Vector3 startposition = m_provider.Position;
//    Vector3 positiontosee = m_worldmap.MapLocationToVector(loctosee);

//    if (ModelConstants.LIMITSIGHT)
//    {
//        if (Vector3.Distance(positiontosee, startposition)
//            > ModelConstants.MAXSIGHTDISTANCE)
//            return;
//    }

//    Vector3 loctoposition = positiontosee - startposition;
//    float angleDifference =
//        Math.Abs((float)Math.Atan2(loctoposition.Y, loctoposition.X)
//            - m_provider.PlaneRotation); /*From XNA Forum*/

//    if (angleDifference < RoboMouse.SIGHTANGLE + COLLISIONCHECKANGLE)
//    {
//        List<ModelObject> objs = m_worldmap.GetContent(loctosee).ModelObjects;
//        if (objs != null && objs.Count > 0)
//            m_possibleblocks.AddRange(objs);
//        if (angleDifference < RoboMouse.SIGHTANGLE)
//            m_visiblelocs.Add(loctosee);
//    }
//}


//public bool RayCollisions(Vector3 start, Vector3 positiontosee, List<ModelObject> objectlist)
//{

//    float dist = Vector3.Distance(start, positiontosee);
//    Vector3 lineofSight = positiontosee - start;
//    Ray ray = new Ray(start, lineofSight);
//    foreach (ModelObject mobj in objectlist)
//    {
//        if (mobj.GetModelType() != ModelType.Wall)
//            continue;
//        if (Vector3.Distance(mobj.Position, start) < dist
//            && mobj.GetBoundingBox().Intersects(ray) != null)
//            return true;
//    }
//    return false;
//}

//private void DoAutonomousBehaviour(GameTime gt)
//{
//    m_timer.Update(gt);
//    switch (m_mode)
//    {
//        case MouseMode.Wandering: Wander(); break;
//        case MouseMode.RunningAway: RunAway(); break;
//        case MouseMode.HeadingToTargetDirect: HeadToTarget(); break;
//        case MouseMode.HeadingToTargetByWayPoint: HeadToTargetByWayPoint(); break;
//        case MouseMode.Surveying: Survey(); break;
//        case MouseMode.Avoiding: Avoid(); break;
//        case MouseMode.Thinking: Think(); break;
//        default: Wander(); break;
//    }

//    if (m_mode != MouseMode.Surveying && m_mode != MouseMode.Thinking)
//        SetDestination(m_currentdestination);
//    m_blockdetected = false;
//}
 
//}
//protected void Wander()
//{
//    if (CheckForPredators())
//        return;

//    m_preyfound = CheckForPrey();

//    if (!m_preyfound && !m_usingwaypoints)        
//        SetRandomDestination();
    
//    NavigateByWayPoints();
//    if (m_timer.Timeup && !m_preyfound)           
//        StartSurvey();      
//}

    
//public void Update(GameTime gameTime)
//{
//    FrameCounter.Update(gameTime);

//    if (m_paused)
//        return;
//    WallClock.Update(gameTime);
//    foreach (ModelObject mobj in m_modelobjects)
//    {
//        mobj.Update(gameTime);
//    }
    
//    m_worldmap.UpdateWorldMap(m_modelobjects);
  
//    if (ModelConstants.RESPAWN)
//        Respawn();
//    ClearDead();

//    ResourceManager.Update(gameTime);         
//}
//}

//public void Update(List<Location> visiblelocs)
//{
//    if (m_targettype == ModelType.None)
//        return;
//    CopyOldTargetList();
//    ClearOldTargets();
//    AddTargets(RetrieveVisibleTargetList(visiblelocs));
//    Target target = ClosestCurrentlyVisibleTarget();
//    if (target.IsNull() )
//    {
//        target = MostRecentTarget();
//        if (!target.IsNull() && m_useghost)
//            target = FindGhostTarget(target);
//    }     
//    m_currenttarget = target;
//    CheckIfTargetsChanged();
//}

//public List<Target> RetrieveVisibleTargetList(List<Location> visiblelocs)
//{
//   List<Target> targetlist = new List<Target>();
//   foreach (Location loc in visiblelocs)
//    {
//        Target target = GetMapTarget(loc);
//        if (target.IsNull())
//            continue;
//        targetlist.Add(target);
//    }
//    return targetlist;
//}

//public Target GetMapTarget(Location tloc)
//{
//    if (m_provider.LocalMap.TimeSinceLastUpdated(tloc) > m_timeout)
//        return Target.NullTarget;
//    if (!m_provider.LocalMap.Contains(tloc, m_targettype))
//        return Target.NullTarget;
//    return m_provider.LocalMap.GetTarget(tloc, m_targettype);
//}

//public struct Target
//{
//    Vector3 m_lastseenposition ;
//    TimeSpan m_totaltimeatlastseen ;
//    Guid m_uniqueid ;

//public void Update(List<Location> visiblelocs)
//{
//    if (m_targettype == ModelType.None)
//        return;
//    CopyOldTargetList();
//    ClearOldTargets();
//    AddTargets(RetrieveVisibleTargetList(visiblelocs));
//    Target target = ClosestCurrentlyVisibleTarget();
//    if (target.IsNull() )
//    {
//        target = MostRecentTarget();
//        if (!target.IsNull() && IsNoLongerThere(target)
//                && m_useghost)
//            target = FindGhostTarget(target);
//    }     
//    m_currenttarget = target;
//    CheckIfTargetsChanged();
//}

//protected void NavigateByWayPoints()
//{
//    if (m_waypoints == null && !SetWayPoints())
//    {
//        SetWayPoints(); //Debug line
//        return;
//    }
   
//    if (Arrived())
//    {
//        if (m_waypoints.Count > 0)
//            m_currentdestination = m_waypoints.Pop();
//        else
//            SetMode(MouseMode.Wandering);
//    }
//    else if (m_blockdetected && !m_map.Contains(m_map.VectortoLocation(m_currentdestination), Target))
//    {
//        if (!SetWayPoints())
//            SetMode(MouseMode.Wandering);
//    }
//}
//protected void NavigateByWayPoints()
//{
//    if (NeedToRestartNavigation())
//    {
//        SetMode(MouseMode.Wandering);
//        m_usingwaypoints = false;
//        return;
//    }

//    if (Arrived() && m_waypoints.Count > 0 )
//        m_currentdestination = m_waypoints.Pop();        

//}

//protected bool NeedToRestartNavigation()
//{
//    if (m_waypoints == null && !SetWayPoints())
//        return true;

//    if (Arrived() && m_waypoints.Count == 0)
//        return true;
//    if (m_blockdetected && !SetWayPoints() )            
//        return true;

//    return false;
//}

//public void SetDirection()
//{
//    Vector3 dir = m_currentdestination - m_position;
//    SetDirection(dir);
//}

//public void SetDirection(Vector3 vel3)
//{         
//    if (vel3 != Vector3.Zero)
//    {
//        m_velocity = CurrentSpeed * Vector3.Normalize(vel3);
//        SetRotationForForward();
//    }
//    else
//        m_velocity = Vector3.Zero;

//}

//public override void Update(GameTime gt)
//{
//    m_findermanager.Update(gt);

//    if (m_velocity == Vector3.Zero)
//        return;

//    Vector3 nextposition = m_position + 
//        (m_velocity * gt.ElapsedGameTime.Milliseconds);

//    if (m_sensorysystem.AllowedPosition
//        (this, nextposition, GetBoundingSphere(nextposition)))
//    {
//        m_position = nextposition;
//        m_lookat += m_velocity * gt.ElapsedGameTime.Milliseconds;
//    }

//    m_map.CurrentLocation = m_map.VectortoLocation(m_position);          
//}

//internal bool AllowedPosition(ModelObject sourcemobj, 
//    Vector3 nextposition, BoundingSphere boundingsphere)
//{
//    if (!AllowedPosition(nextposition))
//        return false;
//    Location sourceloc =
//        m_worldmap.VectortoLocation(nextposition);

//    List<ModelObject> relobjs = 
//        m_worldmap.GetRelevantObjects(sourceloc);
//    foreach (ModelObject mobj in relobjs)
//    {
//        if (mobj != sourcemobj && CollisionOccured(mobj,boundingsphere))
//        {             
//            sourcemobj.HandleCollision(mobj, true);
//            mobj.HandleCollision(sourcemobj, false);
//            sourcemobj.HandleCollisionResult(mobj, true);
//            mobj.HandleCollisionResult(sourcemobj,false);
//            if (ModelHelper.IsBlockingTypeFor
//                (mobj.GetModelType(), sourcemobj.GetModelType()))
//                return false;               
//        }
//    }
//    return true;
//}

//protected bool CheckForPredators()
//{
//    if (Predator == ModelType.None)
//        return false; ;
//    if (!Safe())
//    {
//        SetMode(AgentMode.RunningAway); 
//        return true;
//    }
//    return false;
//}
